const moduloAmbiente = {
    currentEnvironment: 'DESARROLLO',
    environmentLocal: 'LOCAL',
    environmentDesarrollo: 'DESARROLLO',
    environmentQA: 'QA',
    environmentProduccion: 'PRODUCCION',
    local: {
        ip: 'http://localhost:8080',
        authenticate:   'http://gateway-api-lafabril-infra40-des.lafabril-e56b6e75a8f828933d9930ffee795882-0000.us-south.containers.appdomain.cloud/app/v1/api/authenticate',
        urlSeguridad:   'http://gateway-api-lafabril-infra40-des.lafabril-e56b6e75a8f828933d9930ffee795882-0000.us-south.containers.appdomain.cloud/app/v1/api/services/',
        urlTvc:         'http://tvc-route-lafabril-infra40-des.lafabril-e56b6e75a8f828933d9930ffee795882-0000.us-south.containers.appdomain.cloud/tvc/v1/api/fe/',
        urlTvc: '/tvc/v1/api/fe/'
    },
    desarrollo: {
        ip: '',
        authenticate:   'http://seguridad-route-lafabril-infra40-des.lafabril-e56b6e75a8f828933d9930ffee795882-0000.us-south.containers.appdomain.cloud/qrks/v1/seguridad/api/authenticate',
        urlSeguridad:   'http://seguridad-route-lafabril-infra40-des.lafabril-e56b6e75a8f828933d9930ffee795882-0000.us-south.containers.appdomain.cloud/qrks/v1/seguridad/api/services/',
        urlTvc:         'http://industria-route-lafabril-infra40-des.lafabril-e56b6e75a8f828933d9930ffee795882-0000.us-south.containers.appdomain.cloud/qrks/v1/industria/api/services/',
        urlKibana:      'http://kibana-route-lafabril-infra40-des.lafabril-e56b6e75a8f828933d9930ffee795882-0000.us-south.containers.appdomain.cloud'

        // authenticate:   'http://gateway-api-lafabril-infra40-des.lafabril-e56b6e75a8f828933d9930ffee795882-0000.us-south.containers.appdomain.cloud/app/v1/api/authenticate',
        // urlSeguridad:   'http://gateway-api-lafabril-infra40-des.lafabril-e56b6e75a8f828933d9930ffee795882-0000.us-south.containers.appdomain.cloud/app/v1/api/services/',
        // urlTvc:         'http://tvc-route-lafabril-infra40-des.lafabril-e56b6e75a8f828933d9930ffee795882-0000.us-south.containers.appdomain.cloud/tvc/v1/api/fe/',
        // urlKibana:      'http://kibana-route-lafabril-infra40-des.lafabril-e56b6e75a8f828933d9930ffee795882-0000.us-south.containers.appdomain.cloud'
        // authenticate: 'http://gateway-dev:8080/app/v1/api/authenticate',
        // urlSeguridad: 'http://gateway-dev:8080/app/v1/api/services/',
        // urlTvc: 'http://tvc-dev:8080/tvc/v1/api/fe/',
    },
    qa: {
        ip: 'http://172.25.4.19:8080',
        authenticate: '/app/v1/api/authenticate',
        urlSeguridad: '/app/v1/api/services/'
    },
    produccion: {
        ip: '',
        authenticate:   'https://gwapi.nube.lafabril.com.ec/app/v1/api/authenticate',
        urlSeguridad:   'https://gwapi.nube.lafabril.com.ec/app/v1/api/services/',
        urlTvc:         'https://tvcapi.nube.lafabril.com.ec/tvc/v1/api/fe/',
        urlKibana:      'https://mp.nube.lafabril.com.ec'
        // authenticate:   'http://gateway-route-lafabril-infra40-prd.lafabril-e56b6e75a8f828933d9930ffee795882-0000.us-south.containers.appdomain.cloud/app/v1/api/authenticate',
        // urlSeguridad:   'http://gateway-route-lafabril-infra40-prd.lafabril-e56b6e75a8f828933d9930ffee795882-0000.us-south.containers.appdomain.cloud/app/v1/api/services/',
        // urlTvc:         'http://tvc-route-lafabril-infra40-prd.lafabril-e56b6e75a8f828933d9930ffee795882-0000.us-south.containers.appdomain.cloud/tvc/v1/api/fe/',
    }
}
export { moduloAmbiente }