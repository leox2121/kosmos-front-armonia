import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from './shared-module/seguridad/services/auth.guard';
import { HomeComponent } from './views/home/home.component';

const routes: Routes = [
  { path: 'home', component: HomeComponent, canActivate: [AuthGuard]},
  { path: '', component: HomeComponent, canActivate: [AuthGuard]},
  { path: '**', redirectTo: 'login' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true })],
  exports: [RouterModule]
})
export class AppRoutingModule { }

/*
 { path: 'logistica/aprobar-vehiculo', component: AprobarVehiculoComponent, canActivate: [AuthGuard]},
  { path: 'logistica/agregar-vehiculo/:origen', component: AgregarVehiculoComponent, canActivate: [AuthGuard]},
  { path: 'logistica/agregar-vehiculo-detalle/:origen', component: AgregarVehiculoDetalleComponent, canActivate: [AuthGuard]},
  { path: 'logistica/almacen/:origen', component: GaritaComponent, canActivate: [AuthGuard]},
  { path: 'logistica/sellos/:origen', component: SellosComponent, canActivate: [AuthGuard]},
  { path: 'logistica/reemplazar-vehiculo', component: ReemplazarVehiculoComponent, canActivate: [AuthGuard]},
  { path: 'logistica/reporte', component: ReporteComponent, canActivate: [AuthGuard]},
*/