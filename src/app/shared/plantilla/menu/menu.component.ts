import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { AuthService } from 'src/app/shared-module/seguridad/services/auth.service';
// import { Menu } from 'src/app/interface/menu';
// import { AuthService } from 'src/app/auth/auth.service';
// import { menuModulo, menuSeguridad } from 'src/assets/diccionarios/menuModulo';
// import { MantenerUsuarioService } from '../../shared-module/admin-seguridad/services/mantener-menu.service';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {

  isLoggedIn$: Observable<boolean>;

  menus: any = [];
  navbarOpen = true;
  propiedades: object = { width: '250px', marginLeft: '250px' };
  variable: string;
  datosUsuario: { apellido: string, email: string, login: string, login2: string, nombre: string };
  nombreUsuario: string;

  constructor(
    // private servicio: MantenerUsuarioService, 
    private authService: AuthService
  ) { }

  ngOnInit() {
    this.variable = '25px';
    if (localStorage.getItem('accessToken') !== '' && localStorage.getItem('accessToken') !== null && localStorage.getItem('userName') !== '') {
      this.isLoggedIn$ = this.authService.isLoggedIn;
      if (localStorage.getItem('mostrarMenu') === 'true') {
        this.menus = JSON.parse(localStorage.getItem('menu'));
        this.datosUsuario = JSON.parse(localStorage.getItem('datosUsuarioLogin'));
        this.nombreUsuario = `${this.datosUsuario.nombre} ${this.datosUsuario.apellido}`;
        //this.menus.push(menuModulo);    
      }
    }
  }

  toggleNavbar() {
    this.navbarOpen = !this.navbarOpen;
  }

  openNav() {
    this.variable = '25px';
    this.navbarOpen = true;
  }

  closeNav() {
    this.variable = '25px';
    this.navbarOpen = false;
  }

  onLogout() {
    this.isLoggedIn$ = this.authService.isLoggedIn;
    this.authService.logout();
  }

}