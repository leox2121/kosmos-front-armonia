import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HeaderComponent } from './plantilla/header/header.component';
import { MenuComponent } from './plantilla/menu/menu.component';
import { FooterComponent } from './plantilla/footer/footer.component';
import { LfsaInputComponent } from './components/lfsa-input/lfsa-input.component';
import { LfsaErrorMsgComponent } from './components/lfsa-error-msg/lfsa-error-msg.component';
import { LfsaBotonEditarGridComponent } from './components/lfsa-boton-editar-grid/lfsa-boton-editar-grid.component';
import { LfsaCardComponent } from './components/lfsa-card/lfsa-card.component';
import { LfsaComboBoxComponent } from './components/lfsa-combo-box/lfsa-combo-box.component';
import { LfsaModalComponent } from './components/lfsa-modal/lfsa-modal.component';
import { LfsaBotonesFormularioComponent } from './components/lfsa-botones-formulario/lfsa-botones-formulario.component';
import { LfsaCardV2Component } from './components/lfsa-card-v2/lfsa-card-v2.component';
import { LfsaLoadingV2Component } from './components/lfsa-loading-v2/lfsa-loading-v2.component';
import { LfsaLoadingComponent } from './components/lfsa-loading/lfsa-loading.component';

@NgModule({
  declarations: [
    HeaderComponent,
    MenuComponent,
    FooterComponent,
    LfsaInputComponent,
    LfsaErrorMsgComponent,
    LfsaBotonEditarGridComponent,
    LfsaCardComponent,
    LfsaComboBoxComponent,
    LfsaModalComponent,
    LfsaBotonesFormularioComponent,
    LfsaCardV2Component,
    LfsaLoadingV2Component,
    LfsaLoadingComponent
  ],
  exports: [
    HeaderComponent,
    MenuComponent,
    FooterComponent,
    LfsaInputComponent,
    LfsaErrorMsgComponent,
    LfsaBotonEditarGridComponent,
    LfsaCardComponent,
    LfsaComboBoxComponent,
    LfsaModalComponent,
    LfsaBotonesFormularioComponent,
    LfsaCardV2Component,
    LfsaLoadingV2Component,
    LfsaLoadingComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule
  ]
})

export class SharedModule {}