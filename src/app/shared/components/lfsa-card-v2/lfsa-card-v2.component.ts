import { Component, Input, OnInit, TemplateRef } from '@angular/core';

@Component({
  selector: 'app-lfsa-card-v2',
  templateUrl: './lfsa-card-v2.component.html',
  styleUrls: ['./lfsa-card-v2.component.css']
})
export class LfsaCardV2Component implements OnInit {

  @Input() innTemplate: TemplateRef<any>;
  @Input() innTemplateButton: TemplateRef<any>;
  @Input() titulo: string = 'Sin titulo';
  @Input() tituloCard: string = 'Sin titulo';
  @Input() modulo: string = 'Sin titulo';
  classefecto:string = "main-container animated fadeInRight";
  
  constructor() { }

  ngOnInit(): void {
  }

}
