import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LfsaCardV2Component } from './lfsa-card-v2.component';

describe('LfsaCardV2Component', () => {
  let component: LfsaCardV2Component;
  let fixture: ComponentFixture<LfsaCardV2Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LfsaCardV2Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LfsaCardV2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
