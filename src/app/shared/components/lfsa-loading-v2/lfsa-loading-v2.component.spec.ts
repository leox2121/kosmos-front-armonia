import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LfsaLoadingV2Component } from './lfsa-loading-v2.component';

describe('LfsaLoadingV2Component', () => {
  let component: LfsaLoadingV2Component;
  let fixture: ComponentFixture<LfsaLoadingV2Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LfsaLoadingV2Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LfsaLoadingV2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
