import { Component, EventEmitter, Input, OnInit, Output, TemplateRef } from '@angular/core';

@Component({
  selector: 'app-lfsa-card',
  templateUrl: './lfsa-card.component.html',
  styleUrls: ['./lfsa-card.component.css']
})
export class LfsaCardComponent implements OnInit {

  @Input() innTemplate: TemplateRef<any>;
  @Input() titulo = 'Sin titulo';
  @Input() options: boolean;
  @Input() optionsExport: boolean;
  @Input() footer: boolean;
  @Input() banderaColapse = false;
  @Input() colapseName = 'defecto';
  @Output() devolverAccion = new EventEmitter<any>();
  @Input() innFooterTemplate: TemplateRef<any>;
  @Input() efecto: boolean = true;
  simbolo = '-';
  classefecto:string = "main-container animated fadeInRight";
  constructor() { }

  ngOnInit() {
    if(this.efecto == false){
      this.classefecto = "main-container";
    }
    /*console.log('on innit');
    console.log(this.innTemplate);*/
  }

  devolverAccionOption(option) {
    this.devolverAccion.emit(option);
  }

  devolverAccionOptionExport(option) {
    this.devolverAccion.emit(option);
  }

  cambiar(){
    // debugger
      if(this.simbolo === '-') {
            this.simbolo = '+';
      }else {
            this.simbolo = '-';
      }
}

}
