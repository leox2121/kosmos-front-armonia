import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LfsaCardComponent } from './lfsa-card.component';

describe('LfsaCardComponent', () => {
  let component: LfsaCardComponent;
  let fixture: ComponentFixture<LfsaCardComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LfsaCardComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LfsaCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
