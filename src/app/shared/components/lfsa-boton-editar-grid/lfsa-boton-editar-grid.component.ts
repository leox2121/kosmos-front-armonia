import { Component, OnInit } from '@angular/core';
import { ICellRendererAngularComp } from 'ag-grid-angular';

@Component({
  selector: 'app-lfsa-boton-editar-grid',
  templateUrl: './lfsa-boton-editar-grid.component.html',
  styleUrls: ['./lfsa-boton-editar-grid.component.css']
})
export class LfsaBotonEditarGridComponent implements  ICellRendererAngularComp {

  params;
  label: string;
  colorBoton: string; 
  iconEdit: string = 'fal fa-edit';
  iconDelete: string = 'fal fa-trash-alt';
  iconVer: string = 'fa fa-eye';
  iconSelect: string = 'fas fa-clipboard-check';
  iconGraphi: string = 'fas fa-chart-pie';

  agInit(params): void {
    this.params = params;
    this.label = this.params.label || null;
    this.colorBoton = this.params.colorBoton || null;
  }

  refresh(params?: any): boolean {
    return true;
  }

  onClick($event) {
    if (this.params.onClick instanceof Function) {
      // put anything into params u want pass into parents component
      const params = {
        event: $event,
        rowData: this.params.node.data
        // ...something
      };
      this.params.onClick(params);

    }
  }

}
