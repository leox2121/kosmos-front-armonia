import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LfsaBotonEditarGridComponent } from './lfsa-boton-editar-grid.component';

describe('LfsaBotonEditarGridComponent', () => {
  let component: LfsaBotonEditarGridComponent;
  let fixture: ComponentFixture<LfsaBotonEditarGridComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LfsaBotonEditarGridComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LfsaBotonEditarGridComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
