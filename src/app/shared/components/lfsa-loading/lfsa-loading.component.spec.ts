import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LfsaLoadingComponent } from './lfsa-loading.component';

describe('LfsaLoadingComponent', () => {
  let component: LfsaLoadingComponent;
  let fixture: ComponentFixture<LfsaLoadingComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LfsaLoadingComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LfsaLoadingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
