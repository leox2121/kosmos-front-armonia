import { Component, Input, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-lfsa-error-msg',
  templateUrl: './lfsa-error-msg.component.html',
  styleUrls: ['./lfsa-error-msg.component.css']
})
export class LfsaErrorMsgComponent implements OnInit {

  @Input() control: FormControl;
  @Input() label: string;
  
  constructor() { }

  ngOnInit(): void {
  }

}
