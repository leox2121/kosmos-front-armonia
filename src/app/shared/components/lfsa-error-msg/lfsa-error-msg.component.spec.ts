import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LfsaErrorMsgComponent } from './lfsa-error-msg.component';

describe('LfsaErrorMsgComponent', () => {
  let component: LfsaErrorMsgComponent;
  let fixture: ComponentFixture<LfsaErrorMsgComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LfsaErrorMsgComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LfsaErrorMsgComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
