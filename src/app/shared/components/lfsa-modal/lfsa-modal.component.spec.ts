import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LfsaModalComponent } from './lfsa-modal.component';

describe('LfsaModalComponent', () => {
  let component: LfsaModalComponent;
  let fixture: ComponentFixture<LfsaModalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LfsaModalComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LfsaModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
