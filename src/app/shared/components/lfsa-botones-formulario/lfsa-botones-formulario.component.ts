import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { buttons } from '../../../../assets/data/diccionario';

@Component({
  selector: 'app-lfsa-botones-formulario',
  templateUrl: './lfsa-botones-formulario.component.html',
  styleUrls: ['./lfsa-botones-formulario.component.css']
})
export class LfsaBotonesFormularioComponent implements OnInit {

  DATA_BTNS;
  @Input() formularioValido: boolean;
  @Input() textBoton: string = 'Guardar';//mostrarBotonVerde
  @Input() mostrarBotonVerde = true;
  @Input() loading = false;
  @Output() devolverAccion = new EventEmitter<any>();
  
  constructor() { }

  ngOnInit() {
     this.DATA_BTNS = buttons;
  }

  regresar() {
    this.devolverAccion.emit('regresar');
  }

  guardar() {
    this.devolverAccion.emit('guardar');
  }

}
