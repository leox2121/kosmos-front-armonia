import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LfsaBotonesFormularioComponent } from './lfsa-botones-formulario.component';

describe('LfsaBotonesFormularioComponent', () => {
  let component: LfsaBotonesFormularioComponent;
  let fixture: ComponentFixture<LfsaBotonesFormularioComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LfsaBotonesFormularioComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LfsaBotonesFormularioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
