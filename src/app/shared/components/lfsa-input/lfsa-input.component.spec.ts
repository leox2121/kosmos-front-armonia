import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LfsaInputComponent } from './lfsa-input.component';

describe('LfsaInputComponent', () => {
  let component: LfsaInputComponent;
  let fixture: ComponentFixture<LfsaInputComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LfsaInputComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LfsaInputComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
