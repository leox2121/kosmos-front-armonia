import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LfsaComboBoxComponent } from './lfsa-combo-box.component';

describe('LfsaComboBoxComponent', () => {
  let component: LfsaComboBoxComponent;
  let fixture: ComponentFixture<LfsaComboBoxComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LfsaComboBoxComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LfsaComboBoxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
