import { Component, forwardRef, Input, OnInit } from '@angular/core';
import { ControlValueAccessor, FormControl, NG_VALUE_ACCESSOR } from '@angular/forms';

const INPUT_FIELD_VALUE_ACCESSOR: any = {
  provide: NG_VALUE_ACCESSOR,
  useExisting: forwardRef(() => LfsaComboBoxComponent),
  multi: true
};

@Component({
  selector: 'app-lfsa-combo-box',
  templateUrl: './lfsa-combo-box.component.html',
  styleUrls: ['./lfsa-combo-box.component.css'],
  providers: [INPUT_FIELD_VALUE_ACCESSOR]
})
export class LfsaComboBoxComponent implements ControlValueAccessor {

  @Input() comboBox: any[];
  @Input() label: string;
  @Input() isReadOnly = false;
  @Input() id: string;
  @Input() opcion: string;
  @Input() idOpcion: string;
  @Input() habilitar: boolean;
  @Input() control: FormControl;
  @Input() claseLabel = '';
  
  @Input() mostrarImg = false;
  @Input() rutaImagen: string = '';


  constructor() {}
  private innerValue: any;

  get value() {
    return this.innerValue;
  }

  set value(v: any) {
    if (v !== this.innerValue) {
      this.innerValue = v;
      this.onChangeCb(v);
    }
  }

  onChangeCb: (_: any) => void = () => {};
  onTouchedCb: (_: any) => void = () => {};

  writeValue(v: any): void {
    this.value = v;
  }

  registerOnChange(fn: any): void {
    this.onChangeCb = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouchedCb = fn;
  }

  setDisabledState?(isDisabled: boolean): void {
    this.isReadOnly = isDisabled;
  }

}
