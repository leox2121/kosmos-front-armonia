import { Injectable } from '@angular/core';
import Swal from 'sweetalert2';
import { CommonFormService } from './common-form.service';
import { HttpHeaders } from '@angular/common/http';
import { estructuraRespuesta } from '../../../assets/data/diccionario';
import { moduloAmbiente } from '../../../assets/ambiente/ambienteModulo';

@Injectable({
  providedIn: 'root'
})
export class UtilsService {

  mdAmbiente: any = {};
  innEstructuraRespuesta: any;

  ambiente: string;

  authenticate: string;
  urlTvc: string;
  urlSeguridad: string;
  urlKibana: string;

  constructor(private commonFormService: CommonFormService) {
    this.innEstructuraRespuesta = estructuraRespuesta.respuesta;
    this.mdAmbiente = moduloAmbiente;
    this.ambiente = this.mdAmbiente.currentEnvironment;
    switch (this.ambiente) {
      case this.mdAmbiente.environmentLocal:
        this.authenticate = this.mdAmbiente.local.ip + this.mdAmbiente.local.authenticate;
        this.urlSeguridad = this.mdAmbiente.local.ip + this.mdAmbiente.local.urlSeguridad;
        this.urlTvc = this.mdAmbiente.local.ip + this.mdAmbiente.local.urlTvc;
        break;
      case this.mdAmbiente.environmentDesarrollo:
        this.authenticate = this.mdAmbiente.desarrollo.ip + this.mdAmbiente.desarrollo.authenticate;
        this.urlSeguridad = this.mdAmbiente.desarrollo.ip + this.mdAmbiente.desarrollo.urlSeguridad;
        this.urlTvc = this.mdAmbiente.desarrollo.ip + this.mdAmbiente.desarrollo.urlTvc;
        this.urlKibana = this.mdAmbiente.desarrollo.ip + this.mdAmbiente.desarrollo.urlKibana;
        break;
      case this.mdAmbiente.environmentQA:
        this.authenticate = this.mdAmbiente.qa.ip + this.mdAmbiente.qa.authenticate;
        this.urlSeguridad = this.mdAmbiente.qa.ip + this.mdAmbiente.qa.urlSeguridad;
        this.urlTvc = this.mdAmbiente.qa.ip + this.mdAmbiente.qa.urlTvc;
        break;
      case this.mdAmbiente.environmentProduccion:
        this.authenticate = this.mdAmbiente.produccion.ip + this.mdAmbiente.produccion.authenticate;
        this.urlSeguridad = this.mdAmbiente.produccion.ip + this.mdAmbiente.produccion.urlSeguridad;
        this.urlTvc = this.mdAmbiente.produccion.ip + this.mdAmbiente.produccion.urlTvc;
        this.urlKibana = this.mdAmbiente.produccion.ip + this.mdAmbiente.produccion.urlKibana;
        break;
      default:
        break;
    }
  }

  getAuthenticate() {
    const url = `${this.authenticate}`;
    return url;
  }

  getAmbiente() {
    return this.ambiente;
  }

  getQuery(query: string) {
    const url = `${this.urlSeguridad}${query}`;
    return url;
  }

  getSeguridad(query: string) {
    const url = `${this.urlSeguridad}${query}`;
    return url;
  }

  getHeaders(): HttpHeaders {
    const headers = new HttpHeaders({
      'Authorization': localStorage.getItem('accessToken')
    });
    return headers;
  }

  getQueryTvc(query: string) {
    const url = `${this.urlTvc}${query}`;
    return url;
  }

  getUrlKibana() {
    const url = `${this.urlKibana}`;
    return url;
  }

  formatofecha(parametro: String) {
    let temp = parametro.split('-');
    return temp[2] + '/' + temp[1] + '/' + temp[0];
  }

  formatStadardFecha(value: Date) {
    const mesTemp = value.getMonth() + 1;
    const mes = ('0' + mesTemp.toString()).slice(-2);
    let dia = ('0' + value.getDate().toString()).slice(-2);
    const fechaStringActual = value.getFullYear() + '-' + mes + '-' + dia
    return fechaStringActual;
  }

  calcularHorasLogout() {
    const horaLimit = 2;
    let fechaHoraLogin: any = localStorage.getItem('timeLogin');
    if (fechaHoraLogin === '' || fechaHoraLogin === undefined || fechaHoraLogin === null) {
      localStorage.clear();
      window.location.href = window.location.origin;
    }
    fechaHoraLogin = new Date(fechaHoraLogin);
    let fechaHoraActual = new Date();
    let tiempoMN = (fechaHoraActual.valueOf() - fechaHoraLogin.valueOf()) / 1000 / 60;
    let horas = tiempoMN / 60;
    if (horas > horaLimit) {
      localStorage.clear();
      window.location.href = window.location.origin;
    }
  }

  roundCantDollar(tag, params) {
    if (params.data[tag] == '' || params.data[tag] == null || params.data[tag] == undefined) {
      params.data[tag] = 0;
    }
    let dato = Number.parseFloat(params.data[tag]).toFixed(2);
    return '$ ' + dato;
  }

  roundCantDollarInput(valor) {
    if (valor == '' || valor == null || valor == undefined) {
      valor = 0;
    }
    let dato = Number.parseFloat(valor).toFixed(2);
    return '$ ' + dato;
  }

  formatearPorcentaje(tag, params) {
    let dato = Number.parseFloat(params.data[tag]).toFixed(2);
    return '% ' + dato;
  }

  getCurrentDate() {
    let date = new Date();
    let mnth = ("0" + (date.getMonth() + 1)).slice(-2);
    let day = ("0" + date.getDate()).slice(-2);
    return [day, mnth, date.getFullYear()].join("/");
  }

  getCurrentDateGuion() {
    let date = new Date();
    let mnth = ("0" + (date.getMonth() + 1)).slice(-2);
    let day = ("0" + date.getDate()).slice(-2);
    return [date.getFullYear(), mnth, day].join("-");
  }

  getFechaInput(str: string) {
    str = (str.length > 10) ? str : str + 'T00:00:00';
    let date = new Date(str);
    let mnth = ("0" + (date.getMonth() + 1)).slice(-2);
    let day = ("0" + date.getDate()).slice(-2);
    return [day, mnth, date.getFullYear()].join("/");
  }

  getFechaHoraInput(str) {
    let date = new Date(str);
    let mnth = ("0" + (date.getMonth() + 1)).slice(-2);
    let day = ("0" + date.getDate()).slice(-2);
    let h = ("0" + (date.getHours())).slice(-2);
    let m = ("0" + date.getMinutes()).slice(-2);
    let s = ("0" + date.getSeconds()).slice(-2);
    let fechaHora = [day, mnth, date.getFullYear()].join("/");
    return fechaHora + ' ' + h + ':' + m + ':' + s;
  }

  getHoraInput(str) {
    let date = new Date(str);
    let h = ("0" + (date.getHours())).slice(-2);
    let m = ("0" + date.getMinutes()).slice(-2);
    let s = ("0" + date.getSeconds()).slice(-2);
    return [h, m, s].join(":");
  }

  // zoomOut() {
  //   setTimeout(() => {
  //     document.body.style.zoom = "80%"
  //     return false;
  //   }, 900);
  // }

  /*PROCESO DE VDI FRECUENCIA*/
  renderizarFechaFrecuencia(fecha: string, res: any[]) {
    let out: string = "La fecha es:< " + fecha + "> Equivale:  ";
    for (let i = 0; i < res.length; i++) {
      const el = res[i];
      out = out + "(" + el.idFrecuencia + ")=" + el.dscaFrecuencia + "     ";
    }
    out = out + "";
    return out;
  }

  /*DESCARGAR FILE INPUNT BASE64*/
  descargarArchivo(fileName: string, fileBase64: string, type?: string) {
    let name_file = fileName;
    let archivo = fileBase64;
    const dataURI = "data:aplication/plain;base64," + (archivo);
    var a = document.createElement("a");
    a.href = dataURI;
    a.setAttribute("download", name_file);
    a.click();
  }

  /*VERIFICA QUE SI CUALQUIER TIPO DE DATO ESTA VACIO */
  isEmpty(arg) {
    return (
      arg == null || // Check for null or undefined
      arg.length === 0 || // Check for empty String (Bonus check for empty Array)
      (typeof arg === 'object' && Object.keys(arg).length === 0) // Check for empty Object or Array
    );
  }

  exportCSV(cabecera: any[], cuerpo: any[], name: string) {
    //EJEMPLO DE INVOCACION NO DESCOMENTAR
    // let cabecera: any[] = [['example']];
    // const data: any[] = [{'clave':'valor'}];
    // let name: string = "data.csv";
    // this.exportCSV(cabecera, data, name);
    cuerpo.forEach((e) => { cabecera.push(Object.values(e)); });
    let listStr = cabecera.map((e) => e.join(";")).join("\n");
    let str = this.deleteDiacriticos(listStr.toString());
    const blob = new Blob([str], { type: "data:text/csv;charset=utf-8," });
    const blobURL = window.URL.createObjectURL(blob);
    const anchor = document.createElement("a");
    anchor.download = name;
    anchor.href = blobURL;
    anchor.dataset.downloadurl = ["text/csv", anchor.download, anchor.href,].join(":");
    anchor.click();
    setTimeout(() => { URL.revokeObjectURL(blobURL); }, 100);
  }

  deleteDiacriticos(texto) {
    return texto.normalize("NFD").replace(/[\u0300-\u036f]/g, "");
  }

  getDataUser() {
    return JSON.parse(localStorage.getItem('dataUsuario'));
  }

  async servicioItemSeleccionado(textoMostrar) {
    const { value: itemValue } = await Swal.fire({
      title: 'Esta seguro que desea seleccionar este producto ' + textoMostrar.toUpperCase(),
      showCancelButton: true,
      confirmButtonText: 'Confirmar'
    });
    if (itemValue) {
      // console.log('presiono ok-->' + textoMostrar);
      const Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 900
      });

      Toast.fire({
        icon: 'success',
        title: textoMostrar
      })
    }
  }

  async showAlertAdvertenciaEliminar(textoMostrar) {
    const { value: itemValue } = await Swal.fire({
      title: 'Esta seguro que desea Elminar este producto ' + textoMostrar.toUpperCase(),
      showCancelButton: true,
      showConfirmButton: true,
      confirmButtonText: 'Confirmar'
    })
    if (itemValue) {
      const Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 500
      });
      Swal.fire({
        icon: 'success',
        title: 'Registro Eliminado',
        showConfirmButton: false,
        timer: 1000
      })
      return true;
    }
    localStorage.setItem('toasIdEliminar', '0');
    return false;
  }

  mensajeOKVisto(innMensaje, botonConfirmar?: boolean, tiempo = 2000) {
    Swal.fire({
      position: 'top',
      icon: 'success',
      title: innMensaje,
      showConfirmButton: botonConfirmar,
      timer: tiempo
    });
  }

  mensajeErrorVisto(innMensaje, botonConfirmar?: boolean, tiempo = 1000) {
    Swal.fire({
      position: 'top',
      icon: 'error',
      title: innMensaje,
      showConfirmButton: botonConfirmar,
      timer: tiempo
    });
  }

  mensajeOKToast(innMensaje) {
    const Toast = Swal.mixin({
      toast: true,
      position: 'top-end',
      showConfirmButton: false,
      timer: 1000
    });

    Toast.fire({
      icon: 'success',
      title: innMensaje
    });
  }

  mensajeErroToast(innMensaje, timer?: number) {
    const Toast = Swal.mixin({
      toast: true,
      position: 'top-end',
      showConfirmButton: false,
      timer: (timer == undefined ? 3000 : timer)
    });

    Toast.fire({
      icon: 'error',
      title: innMensaje
    });
  }

  setearListasLocalStore(nombre: string, lista: any[]) {
    if (lista.length > 0) {
      localStorage.setItem(nombre, JSON.stringify(lista));
    } else {
      localStorage.setItem(nombre, '');
    }
  }

  recuperarListasLocalStore(nombre: string): any[] {
    if (localStorage.getItem(nombre) !== null) {
      if (localStorage.getItem(nombre).length > 0) {
        return JSON.parse(localStorage.getItem(nombre));
      } else { return []; }
    } else {
      return [];
    }

  }

  setearParametroLocalStore(param: string, value: any) {
    localStorage.setItem(param, value);
  }

  getParametroLocalStore(param: string) {
    return localStorage.getItem(param);
  }

  // ALERTAS UTILES CON CONFIRMACION QUE DEVUELVEN TRUE O FALSE
  // cb como parametro es un callback para evaluar la respuesta desde el componente padre
  confirmar(params, cb) {
    Swal.fire({
      title: `${params.title}`,
      text: `${params.text}`,
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si, estoy seguro'
    }).then((result) => {
      if (result.value) {
        cb(result.value);
        Swal.fire(
          `${params.action}`,
          `Haz realizado la accion ${params.action} correctamente`,
          'success'
        );
        // this.commonFormService.limpiarYregrear(context);
      }
    });
  }

//   confirmarAcyncronico(context, params, cb, cbb?) {
//     Swal.queue([{
//       title: `${params.title}`,
//       text: `${params.text}`,
//       icon: 'warning',
//       showCancelButton: true,
//       confirmButtonColor: '#3085d6',
//       cancelButtonColor: '#d33',
//       confirmButtonText: 'Si, estoy seguro',
//       showLoaderOnConfirm: true,
//       preConfirm: () => {
//         cb().subscribe(res => {
//           Swal.fire(
//             `${params.action}`,
//             `Haz realizado la accion ${params.action} correctamente`,
//             'success'
//           );
//           this.commonFormService.limpiarYregrear(context);
//           if (cbb) {
//             cbb();
//           }
//           // console.log(res);
//           return res;
//         }, (e) => {
//           console.log(e.error);
//           let msg = '';
//           let status = '';

//           const err = e.error
//           msg = err.message;
//           status = e.statusText;

//           if (e.status === 0) {
//             msg = 'No hay conexión a internet';
//           }
//           if (e.status === 501) {
//             //  msg = e.error.message;
//             //  status = e.error.status;
//             msg = e.error;
//             status = e.statusText;
//           }
//           if (e.status === 500) {
//             //msg = e.error;
//             msg = e.error.message;
//             status = e.statusText;
//           }
//           // AQUI MANEJAR LOS OTROS POSIBLES ERRORES PARA CAMBIAR EL MENSAJE
//           Swal.fire({
//             icon: 'warning',
//             title: status,
//             text: msg,
//           });
//         });
//       }
//     }]);
//   }


  // confirmarAcyncronicoSinFormularioGrid(params, cb, cbb?) {
  //   Swal.queue([{
  //     title: `${params.title}`,
  //     text: `${params.text || ''}`,
  //     icon: 'warning',
  //     showCancelButton: true,
  //     confirmButtonColor: '#3085d6',
  //     cancelButtonColor: '#d33',
  //     confirmButtonText: 'Si, estoy seguro',
  //     showLoaderOnConfirm: true,
  //     preConfirm: () => {
  //       cb().subscribe(res => {
  //         Swal.fire(
  //           `${params.action}`,
  //           `Haz realizado la accion ${params.action} correctamente`,
  //           'success'
  //         );
  //         // this.commonFormService.limpiarYregrear(context);
  //         if (cbb) {
  //           cbb();
  //         }
  //         return res;
  //       }, (e) => {
  //         console.log(e.error);
  //         let msg = '';
  //         let status = '';
  //         if (e.status === 0) {
  //           msg = 'No hay conexión a internet';
  //         }
  //         if (e.error.key === '501') {
  //           msg = e.error.message;
  //           status = e.error.status;
  //         }
  //         // AQUI MANEJAR LOS OTROS POSIBLES ERRORES PARA CAMBIAR EL MENSAJE
  //         Swal.fire({
  //           icon: 'warning',
  //           title: status,
  //           text: msg,
  //         });
  //       });
  //     }
  //   }]);
  // }

  confirmarSincronoPromesaColor(params) {
    let color: any;
    return new Promise((resolve, reject) => {
      Swal.fire({
        title: `${params.title}`,
        text: `${params.text || ''}`,
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        cancelButtonText: 'Cancelar',
        confirmButtonText: 'Sí, estoy seguro',
        showLoaderOnConfirm: true,
      }).then(msn => {
        debugger
        if (msn.value) {
          params.funcion.subscribe((resp) => {
            switch (resp.colorClasificacion) {
              case 'Verde':
                color = 'success';
                break;
              case 'Amarillo':
                color = 'warning';
                break;
              case 'Naranja':
                color = 'error';
                break;
              case 'Rojo':
                color = 'error';
                break;
              default:
                break;
            }
            Swal.fire(
              `${resp.recomendacion}`,
              `Puntaje: ${resp.puntaje} | Codigo: ${resp.codigo}`,
              color
            );
            resolve(resp);
          }, (e) => {
            console.log(e.error);
            let msg = '';
            let status = '';

            const err = e.error
            msg = err.message;
            status = e.statusText;

            if (e.status === 0) {
              msg = 'No hay conexión a internet';
            }
            if (e.status === 501) {
              //  msg = e.error.message;
              //  status = e.error.status;
              msg = e.error;
              status = e.statusText;
            }
            if (e.status === 500) {
              //msg = e.error;
              msg = e.error.message;
              status = e.statusText;
            }
            // AQUI MANEJAR LOS OTROS POSIBLES ERRORES PARA CAMBIAR EL MENSAJE
            Swal.fire({
              icon: 'warning',
              title: status,
              text: msg,
            });
          });
        } else {
          console.log('cancelo');
        }
      });
    });
  }

  async confirmarSincronoPromesa(params: {title?: string, text?: string, loading?: any, funcion?: any, context?: any, action?: any }) {
    return new Promise((resolve, reject) => {
      //const parametros: any = params;
      Swal.fire({
        title: `${params.title}`,
        text: `${params.text || ''}`,
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Si, estoy seguro',
        showLoaderOnConfirm: true,
      }).then(msn => {
        console.log(msn);
        if (msn.value) {
          if (params.loading !== undefined) {
            params.loading.loading = true;
          }
          params.funcion.subscribe((resp) => {
            console.log(resp);
            Swal.fire(
              `${params.action}`,
              `Haz realizado la accion ${params.action} correctamente`,
              'success'
            );
            if (params.context !== undefined) {
              this.commonFormService.limpiarYregrear(params.context);
            }
            resolve(resp);
          }, (e) => {
            if(e.status === 204) {
              Swal.fire({
                icon: 'warning',
                title: 'Error',
                text: 'No se encontro una respuesta.',
              });
            }
            console.log(e);
            let msg = '';
            let status = '';
            if (e.status === 0) {
              msg = 'No hay conexión a internet';
            }
            // si entra por este if es para eliminar pedidos completos
            //if (e.error.key === '423' || e.error.key === '424' || e.error.key === '426' || e.error.key === '401') {
            if (e.error.key === '495' || e.error.key === '498' || e.error.key === '499' || e.error.key === '496') {
              reject(e);
            } else {
              // msg = e.error.message;
              msg = e.error.mensaje;
              status = e.error.status;
              Swal.fire({
                icon: 'warning',
                title: status,
                text: msg,
              });
              if (params.loading !== undefined) {
                params.loading.loading = false;
              }
            }
            reject(e);
          });
        } else {
          console.log('cancelo');
        }
      });
    });
  }

  confirmarSincronoPromesaAction(params, cb) {
    Swal.fire({
      title: `${params.title}`,
      text: `${params.text || ''}`,
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      cancelButtonText: 'Cancelar',
      confirmButtonText: 'Si, estoy seguro',
      showLoaderOnConfirm: true,
    }).then(msn => {
      if (msn.value) {
        cb();
        Swal.fire(
          `${params.action}`,
          `Se ha enviado al correo de recuperacion del usuario ${params.ruc} correctamente`,
          //`Haz realizado la accion ${params.action} correctamente`,
          // `Por favor guarde el codigo de solicitud..!!`,
          'success'
        );
      } else {
        console.log('cancelo');
      }
    });
  }

//   confirmarAcyncronicoTVC(context, params, cb, cbb?) {
//     Swal.queue([{
//       title: `${params.title}`,
//       text: `${params.text}`,
//       type: 'warning',
//       showCancelButton: true,
//       confirmButtonColor: '#3085d6',
//       cancelButtonColor: '#d33',
//       cancelButtonText: 'Cancelar',
//       confirmButtonText: 'Si, estoy seguro',
//       showLoaderOnConfirm: true,
//       preConfirm: () => {
//         cb().subscribe(res => {
//           Swal.fire(
//             `${params.action}`,
//             `Haz realizado la accion ${params.action} correctamente`,
//             'success'
//           );
//           //this.commonFormService.limpiarYregrear(context);
//           if (cbb) {
//             cbb();
//           }
//           //console.log(res);
//           return res;
//         }, (e) => {
//           console.log(e.error);
//           let msg = '';
//           let status = '';
//           if (e.status === 0) {
//             msg = 'No hay coneccion a internet';
//           }
//           if (e.status === 501) {
//             //  msg = e.error.message;
//             //  status = e.error.status;
//             msg = e.error;
//             status = e.statusText;
//           }
//           if (e.status === 500) {
//             msg = e.error;
//             status = e.statusText;
//           }
//           // AQUI MANEJAR LOS OTROS POSIBLES ERRORES PARA CAMBIAR EL MENSAJE
//           Swal.fire({
//             icon: 'warning',
//             title: status,
//             text: msg,
//           });
//         });
//       }
//     }]);
//   }

  proundDecimal(param, longi) {
    return Number.parseFloat(param).toFixed(longi);
  }

  confirmarSincronoPromesaNormal(params) {
    return new Promise((resolve, reject) => {
      Swal.fire({
        title: `${params.title}`,
        text: `${params.text || ''}`,
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        cancelButtonText: 'Cancelar',
        confirmButtonText: 'Sí, estoy seguro',
        showLoaderOnConfirm: true,
      }).then(msn => {
        if (msn.value) {
          let mensaje = `Haz realizado la acción ${params.action} correctamente`;
          if (params.msn != undefined) {
            mensaje = params.msn;
          }
          if (params.loading) {
            params.context.loading = true;
          }
          params.funcion.subscribe((resp) => {
            Swal.fire(
              `${params.action}`,
              mensaje,
              'success'
            );
            resolve(resp);
            if (params.loading) {
              params.context.loading = false;
            }
          }, (e) => {
            // debugger
            console.log(e.error);
            let msg = '';
            let status = '';
            if (e.status === 0) {
              msg = 'No hay conexión a internet';
            }
            if (e.error.key === '495' || e.error.key === '498' || e.error.key === '499' || e.error.key === '496') {
              reject(e);
            } else {
              msg = e.error.message;
              status = e.error.status;
              Swal.fire({
                icon: 'warning',
                title: status,
                text: msg,
              });
            }
            reject(e);
            if (params.loading) {
              params.context.loading = false;
            }
          });
        } else {
          console.log('cancelo servicio Utils');
        }
      });
    });
  }

  confirmarSincronoPromesaSinActions(params) {
    return new Promise((resolve, reject) => {
      Swal.fire({
        title: `${params.title}`,
        text: `${params.text || ''}`,
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        cancelButtonText: 'Cancelar',
        confirmButtonText: 'Sí, estoy seguro',
        showLoaderOnConfirm: true,
      }).then(msn => {
        if (msn.value) {
          let mensaje = `Haz realizado la accion ${params.action} correctamente`;
          if (params.msn !== undefined) {
            mensaje = params.msn;
          }
          resolve('ok');
        } else {
          console.log('cancelo servicio Utils');
        }
      });
    });
  }

  confirmarSincronoPromesaDetalle(params) {
    return new Promise((resolve, reject) => {
      Swal.fire({
        title: `${params.title}`,
        input: 'text',
        text: `${params.text || ''}`,
        icon: 'info',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        cancelButtonText: 'Cancelar',
        confirmButtonText: 'Editar',
        inputValidator: detalle => {
          // Si el valor es válido, debes regresar undefined. Si no, una cadena
          if (!detalle) {
            return "Ingrese la informacion solicitada";
          } else {
            return undefined;
          }
        }
      }).then(result => {
        if (result.value) {
          let mensaje = '';
          if (params.msn !== undefined) {
            mensaje = params.msn;
          }
          params.funcion.subscribe((resp) => {
            Swal.fire(
              `${params.action}`,
              result.value,
              'success'
            );
            resolve(result.value);
          });
        } else {
          if (params.modal) {
            params.context.abrir.click();
          }
        }
      });
    });
  }

  getNumberCurrentWeek() {
    var d: any = new Date();  //Creamos un nuevo Date con la fecha de "this".
    d.setHours(0, 0, 0, 0);   //Nos aseguramos de limpiar la hora.
    d.setDate(d.getDate() + 4 - (d.getDay() || 7)); // Recorremos los días para asegurarnos de estar "dentro de la semana"
    var c: any = new Date(d.getFullYear(), 0, 1);
    return Math.ceil((((d - c) / 8.64e7) + 1) / 7);
  }

  Prop(obj, is, value?) {
    if (typeof is == 'string')
      is = is.split('.');
    if (is.length == 1 && value !== undefined)
      return obj[is[0]] = value;
    else if (is.length == 0)
      return obj;
    else {
      var prop = is.shift();
      //Forge a path of nested objects if there is a value to set
      if (value !== undefined && obj[prop] == undefined) obj[prop] = {}
      return this.Prop(obj[prop], is, value);
    }
  }

  renderTemplateParam(str, obj) {
    return str.replace(/\$\{(.+?)\}/g, (match, p1) => { return this.Prop(obj, p1) })
  }

  confirmarSincronoPromesaCantidadProducto(params) {
    return new Promise((resolve, reject) => {
      Swal.fire({
        title: `${params.title}`,
        input: 'number',
        text: `${params.text || ''}`,
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        cancelButtonText: 'Cancelar',
        confirmButtonText: 'Agregar',
        showLoaderOnConfirm: true,
      }).then(msn => {
        if (msn.value) {
          let mensaje = `Haz realizado la acción ${params.action} correctamente`;
          if (params.msn != undefined) {
            mensaje = params.msn;
          }
          params.funcion.subscribe((resp) => {
            Swal.fire(
              `${params.action}`,
              mensaje,
              'success'
            );
            resolve(msn.value);
            if (params.modal) {
              params.context.abrir.click();
            }
          }, (e) => {

            console.log(e.error);
            let msg = '';
            let status = '';
            if (e.status === 0) {
              msg = 'No hay conexión a internet';
            }
            if (e.error.key === '495' || e.error.key === '498' || e.error.key === '499' || e.error.key === '496') {
              reject(e);
            } else {
              msg = e.error.message;
              status = e.error.status;
              Swal.fire({
                icon: 'warning',
                title: status,
                text: msg,
              });
            }
            reject(e);
            if (params.modal) {
              params.context.abrir.click();
            }
          });
        } else {
          console.log('cancelo servicio Utils');
          if (params.modal) {
            params.context.abrir.click();
          }
        }
      });
    });
  }

  demoIngresarCantidad() {
    Swal.fire({
      title: 'Ingrese Cantidad',
      input: 'number',
      inputAttributes: {
        autocapitalize: 'off'
      },
      showCancelButton: true,
      confirmButtonText: 'ok',
      showLoaderOnConfirm: true,
      preConfirm: (login) => {
        console.log(login);
      }
    }).then((result: any) => {
      if (result.value) {
        this.mensajeOKVisto(result.value, false, 1000);
        return result.value;
      }
    });
  }

}
