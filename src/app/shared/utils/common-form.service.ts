import { Injectable } from '@angular/core';
import { buttons } from '../../../assets/data/diccionario';


@Injectable({
  providedIn: 'root'
})
export class CommonFormService {

  constructor() { }

  abrirFormularioParaGuardar(context) {
    context.textBoton = buttons.guardar;
    context.editableForm = false;
    context.mostrarFormulario = true;
    context.formulario.reset();
  }

  abrirFormularioParaEditar(context, rowData) {
    context.editableForm = true;
    context.mostrarFormulario = true;
    context.textBoton = buttons.editar;
    context.formulario.setValue(rowData);
  }

  limpiarYregrear(context) {
    context.formulario.reset();
    context.mostrarFormulario = false;
  }

  onGridReadyUtil(ctx, params, service) {
    ctx.gridApi = params.api;
    ctx.gridColumnApi = params.columnApi;
    service.getAll().subscribe((res) => {
      ctx.rowData = res.respuesta;
    });
  }  
}
