import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map, timeout } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { UtilsService } from './utils.service';

@Injectable({
  providedIn: 'root'
})
export class CommonService {

  url: any;
  diccionario: any;
  constructor(
    private http: HttpClient,
    private util: UtilsService
  ) { }

  /*PETICIONES SIN HEADER*/
  peticionGetSinHeader(innUrl: any) {
    return this.http.get(innUrl)
      .pipe(
        map(res => res[this.util.innEstructuraRespuesta])
      );
  }

  peticionPostSinHeader(innUrl: string, innObjeto) {
    return this.http.post<any>(innUrl, innObjeto)
      .pipe(
        map(res => res[this.util.innEstructuraRespuesta]
        )
      );
  }

  peticionPutSinHeader(innUrl: string, innObjeto) {
    return this.http.put<any>(innUrl, innObjeto)
      .pipe(
        map(res => res[this.util.innEstructuraRespuesta]
        )
      );
  }

  peticionDeleteSinHeader(innUrl: string, innObjeto?) {
    return this.http.delete<any>(innUrl, innObjeto)
      .pipe(
        map(res => res[this.util.innEstructuraRespuesta]
        )
      );
  }

  /*PETICIONES CON HEADER*/
  peticionGetConHeader(innUrl: any) {
    const headers = this.util.getHeaders();
    return this.http.get(innUrl, { headers })
      .pipe(
        map(res => res[this.util.innEstructuraRespuesta])
      );
  }

  peticionPostConHeader(innUrl: string, innObjeto) {
    const headers = this.util.getHeaders();
    return this.http.post<any>(innUrl, innObjeto, { headers })
      .pipe(
        map(res => res[this.util.innEstructuraRespuesta]
        )
      );
  }

  peticionPutConHeader(innUrl: string, innObjeto) {
    const headers = this.util.getHeaders();
    return this.http.put<any>(innUrl, innObjeto, { headers })
      .pipe(
        map(res => res[this.util.innEstructuraRespuesta]
        )
      );
  }

  peticionDeleteConHeader(innUrl: string) {
    const headers = this.util.getHeaders();
    return this.http.delete<any>(innUrl, { headers })
      .pipe(
        map(res => res[this.util.innEstructuraRespuesta]
        )
      );
  }

  /* PETICIONES CON TIME OUT */
  peticionGetConHeaderTimeOut(innUrl: any, timeOut: number) {
    const headers = this.util.getHeaders();
    return this.http.get(innUrl, { headers })
      .pipe(
        timeout(timeOut),
        map(res => res[this.util.innEstructuraRespuesta])
      );
  }

  peticionPostConHeaderTimeOut(innUrl: string, innObjeto, timeOut: number) {
    const headers = this.util.getHeaders();
    return this.http.post<any>(innUrl, innObjeto, { headers })
      .pipe(
        timeout(timeOut),
        map(res => res[this.util.innEstructuraRespuesta]
        )
      );
  }
}