import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'kosmos-front-armonia';

  usuarioLogeado = true;
  mostrarPlantilla = false;
  
  constructor() {
    const bandera = localStorage.getItem('mostrarMenu');
    if (bandera === 'true' ) {
        this.mostrarPlantilla = true;
    } else {
        this.mostrarPlantilla = false;
    }
  }
}
