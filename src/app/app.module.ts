import { DatePipe } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { LOCALE_ID, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';
//MODULOS
import { SeguridadModule } from './shared-module/seguridad/seguridad.module';
import { AuthGuard } from './shared-module/seguridad/services/auth.guard';
import { AuthService } from './shared-module/seguridad/services/auth.service';
import { SharedModule } from './shared/shared.module';
//LIBRERIAS
import { AgGridModule } from 'ag-grid-angular';
//COMPONENTES
import { HomeComponent } from './views/home/home.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TvcModule } from './aplicaciones-modulos/tvc/tvc.module';
import { LfsaBotonEditarGridComponent } from './shared/components/lfsa-boton-editar-grid/lfsa-boton-editar-grid.component';
import { AdminSeguridadModule } from './shared-module/admin-seguridad/admin-seguridad.module';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    TvcModule,
    AgGridModule.withComponents([LfsaBotonEditarGridComponent]),
    HttpClientModule,
    SharedModule,
    SeguridadModule,
    AdminSeguridadModule,
    AppRoutingModule
  ],
  // providers: [],
  providers: [{ provide: LOCALE_ID, useValue: 'es-Ar' }, AuthService, AuthGuard, DatePipe],
  bootstrap: [AppComponent]
})
export class AppModule { }
