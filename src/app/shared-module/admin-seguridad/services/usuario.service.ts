import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { UsuarioSeguridadModel } from '../models/usuarioSeguridad.model';
import { moduloSeguridad } from '../diccionarios/seguridadModulo.js';
import { map } from 'rxjs/operators';
import { UtilsService } from 'src/app/shared/utils/utils.service';

@Injectable({
  providedIn: 'root'
})
export class UsuarioService {

  headers: HttpHeaders;
  mdSeguridad: any;
  url: any;
  urlLdap: any;
  
  constructor(private http: HttpClient, private _util: UtilsService) {
    this.mdSeguridad = moduloSeguridad;
    this.url = _util.getSeguridad(this.mdSeguridad.url.seguridad.usuario);
    this.urlLdap = _util.getSeguridad(this.mdSeguridad.url.seguridad.buscarLdap);
  }

  getAll() {
    const headers = this._util.getHeaders();
    return this.http.get<UsuarioSeguridadModel>(this.url, { headers });
  }

  guardarUsuario(inUsuario: UsuarioSeguridadModel) {
    const headers = this._util.getHeaders();
    return this.http.post<UsuarioSeguridadModel[]>(this.url, inUsuario, { headers });
  }

  actualizarUsuarioId(inUsuario: UsuarioSeguridadModel) {
    const headers = this._util.getHeaders();
    return this.http.put<UsuarioSeguridadModel>(this.url, inUsuario, { headers });
  }

  eliminarUsuarioId(inId) {
    const headers = this._util.getHeaders();
    return this.http.delete(this.url + '/' + inId, { headers });
  }
  buscarUsuarioLdap(inId) {
    const headers = this._util.getHeaders();
    return this.http.get(this.urlLdap + inId, { headers }).pipe(
      map(res => res['respuesta'])
    );
  }//
}
