import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { SesionSeguridadModel } from '../models/sesionSeguridad.model';
import { moduloSeguridad } from '../diccionarios/seguridadModulo.js';
import { UtilsService } from 'src/app/shared/utils/utils.service';
import { CommonService } from 'src/app/shared/utils/common.service';


@Injectable({
  providedIn: 'root'
})
export class SesionService {
  mdSeguridad: any;
  url: string;
  urlAplicacion: string;

  constructor(
    private http: HttpClient,
    private _util: UtilsService,
    private _commom: CommonService
  ) {
    this.mdSeguridad = moduloSeguridad;
    this.url = _util.getSeguridad(this.mdSeguridad.url.seguridad.sesion);
    this.urlAplicacion = _util.getSeguridad(this.mdSeguridad.url.seguridad.aplication);
  }

  getAll() {
    // const headers = this._util.getHeaders();
    // return this.http.get<SesionSeguridadModel>(this.url, { headers });
    return this._commom.peticionGetConHeader(this.url);
  }

  getAplicacion() {
    const headers = this._util.getHeaders();
    return this.http.get<any>(this.urlAplicacion, { headers });
  }


  guardarSesion(inSesion: SesionSeguridadModel) {
    const headers = this._util.getHeaders();
    return this.http.post<SesionSeguridadModel>(this.url, inSesion, { headers });
  }

  actualizarSesionId(inSesion: SesionSeguridadModel) {
    const headers = this._util.getHeaders();
    return this.http.put<SesionSeguridadModel>(this.url, inSesion, { headers });
  }

  eliminarSesionId(inId) {
    const headers = this._util.getHeaders();
    return this.http.delete(this.url + '/' + inId, { headers });
  }
}
