import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { MantenerMenu } from '../models/mantenerMenu';
import { moduloSeguridad } from '../diccionarios/seguridadModulo.js';
import { UtilsService } from 'src/app/shared/utils/utils.service';
import { CommonService } from 'src/app/shared/utils/common.service';

@Injectable({
  providedIn: 'root'
})
export class MantenerUsuarioService {

  headers: HttpHeaders;
  mdSeguridad: any;
  url: string;

  constructor(
    private http: HttpClient,
    private _util: UtilsService,
    private _commom: CommonService
  ) {
    this.mdSeguridad = moduloSeguridad;
    this.url = this._util.getSeguridad(this.mdSeguridad.url.seguridad.menu);
  }

  getAll() {
    // const headers = this._util.getHeaders();
    // return this.http.get(this.url, { headers })
    //   .pipe(
    //     map(res => res['respuesta'])
    //   );
    return this._commom.peticionGetConHeader(this.url);
  }

  guardarMenu(objetoMenu: MantenerMenu) {
    // const headers = this._util.getHeaders();
    // return this.http.post<MantenerMenu[]>(this.url, objetoMenu, { headers })
    //   .pipe(
    //     map(res => res['respuesta'])
    //   );
    return this._commom.peticionPostConHeader(this.url, objetoMenu);
  }

  actualizarMenu(objetoMenu: MantenerMenu) {
    // const headers = this._util.getHeaders();
    // return this.http.put<MantenerMenu>(this.url, objetoMenu, { headers })
    //   .pipe(
    //     map(res => res['respuesta'])
    //   );
    return this._commom.peticionPutConHeader(this.url, objetoMenu);
  }

  eliminarId(inId) {
    // const headers = this._util.getHeaders(); //this._util.getQuery(this.url)
    // return this.http.delete<MantenerMenu>(this.url + '/' + inId, { headers });
    return this._commom.peticionDeleteConHeader(this.url + '/' + inId);
  }

  getMenu() {
    // const headers = this._util.getHeaders();
    // return this.http.get<any[]>(this.url + '/nw/' + localStorage.getItem('userName'), { headers });
    return this._commom.peticionGetConHeader(this.url + '/nw/' + localStorage.getItem('userName'));
  }
}
