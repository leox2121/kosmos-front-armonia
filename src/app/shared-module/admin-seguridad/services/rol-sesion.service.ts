import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { moduloSeguridad } from '../diccionarios/seguridadModulo';
import { UtilsService } from 'src/app/shared/utils/utils.service';
import { CommonService } from 'src/app/shared/utils/common.service';


@Injectable({
  providedIn: 'root'
})
export class RolSesionService {

  headers: HttpHeaders;
  mdSeguridad: any;
  url: string;

  constructor(
    private http: HttpClient,
    private _util: UtilsService,
    private _commom: CommonService
  ) {
    this.mdSeguridad = moduloSeguridad;
    this.url = this._util.getSeguridad(this.mdSeguridad.url.seguridad.rol_sesion);
  }

  getAll() {
    // const headers = this._util.getHeaders();
    // return this.http.get(this.url, { headers });
    return this._commom.peticionGetConHeader(this.url);
  }

  getSesion(sesion) {
    // const headers = this._util.getHeaders();
    // return this.http.get(this.url + '/' + sesion, { headers });
    return this._commom.peticionGetConHeader(this.url + '/' + sesion);
  }

  guardarRolSesion(inRolSesion) {
    // const headers = this._util.getHeaders();
    // return this.http.post(this.url, inRolSesion, { headers });
    return this._commom.peticionPostConHeader(this.url, inRolSesion);
  }

  actualizarRolSesionId(id, parametro) { }

  eliminarRolSesionId(rolId, sesionId) {
    // const headers = this._util.getHeaders();
    // return this.http.delete(this.url + '/' + rolId + '/' + sesionId, { headers });
    return this._commom.peticionDeleteConHeader(this.url + '/' + rolId + '/' + sesionId);
  }
}
