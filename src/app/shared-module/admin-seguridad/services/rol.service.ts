import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { RolSeguridadModel } from '../models/rolSeguridad.model';
import { moduloSeguridad } from '../diccionarios/seguridadModulo.js';
import { map } from 'rxjs/operators';
import { UtilsService } from 'src/app/shared/utils/utils.service';
import { CommonService } from 'src/app/shared/utils/common.service';

@Injectable({
  providedIn: 'root'
})
export class RolService {

  mdSeguridad: any;
  url: string;
  urlSesiones: string;
  urlSesion: string;

  constructor(
    private http: HttpClient,
    private _util: UtilsService,
    private _commom: CommonService
  ) {
    this.mdSeguridad = moduloSeguridad;
    this.url = this._util.getSeguridad(this.mdSeguridad.url.seguridad.rol);
    this.urlSesiones = this._util.getSeguridad(this.mdSeguridad.url.seguridad.sessionesByRol);
    this.urlSesion = this._util.getSeguridad(this.mdSeguridad.url.seguridad.sesion);
  }

  getAll() {
    // const headers = this._util.getHeaders();
    // return this.http.get<RolSeguridadModel[]>(this.url, { headers });
    return this._commom.peticionGetConHeader(this.url);
  }

  guardarRol(inRol: RolSeguridadModel) {
    // const headers = this._util.getHeaders();
    // return this.http.post<RolSeguridadModel[]>(this.url, inRol, { headers} );
    return this._commom.peticionPostConHeader(this.url, inRol);
  }

  actualizarRolId(inRol: RolSeguridadModel) {
    // const headers = this._util.getHeaders();
    // return this.http.put<RolSeguridadModel[]>(this.url, inRol, { headers} );
    return this._commom.peticionPutConHeader(this.url, inRol);
  }

  eliminarRolId(inId) {
    // const headers = this._util.getHeaders();
    // return this.http.delete(this.url + '/' + inId, { headers} );
    return this._commom.peticionDeleteConHeader(this.url + '/' + inId);
  }

  getSesionesByRol(innIdRrol) {
    // const headers = this._util.getHeaders();
    // return this.http.get<any[]>(this.urlSesiones + innIdRrol, { headers }).pipe(
    //   map(res => res['respuesta'])
    // );
    return this._commom.peticionGetConHeader(this.urlSesiones + innIdRrol);
  }//

  getAllSessiones() {
    // const headers = this._util.getHeaders();
    // return this.http.get(this.urlSesion, { headers }).pipe(
    //   map(res => res['respuesta'])
    // );
    return this._commom.peticionGetConHeader(this.urlSesion);
  }
}
