import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { moduloSeguridad } from '../diccionarios/seguridadModulo';
import { Authority } from '../models/authority.model';
import { UtilsService } from 'src/app/shared/utils/utils.service';
import { CommonService } from 'src/app/shared/utils/common.service';


@Injectable({
  providedIn: 'root'
})
export class AutorizacionService {

  mdSeguridad: any;
  url: string;
  urlAuthority: string;

  constructor(
    private http: HttpClient,
    private _util: UtilsService,
    private _commom: CommonService
  ) {
    this.mdSeguridad = moduloSeguridad;
    this.url = _util.getSeguridad(this.mdSeguridad.url.seguridad.user_authority);
    this.urlAuthority = _util.getSeguridad(this.mdSeguridad.url.seguridad.listaAuthority);
  }

  getAutorizacion(user) {
    // const headers = this._util.getHeaders();
    // return this.http.get(this.url + '/' + user, { headers });
    return this._commom.peticionGetConHeader(this.url + '/' + user);
  }

  guardarAutorizacion(inRolUsuario) {
    // const headers = this._util.getHeaders();
    // return this.http.post(this.url, inRolUsuario, { headers });
    return this._commom.peticionPostConHeader(this.url, inRolUsuario);
  }

  eliminarAutorizacion(inId, authory) {
    // const headers = this._util.getHeaders();
    // return this.http.delete(this.url + '/' + inId + '/' + authory, { headers });
    return this._commom.peticionDeleteConHeader(this.url + '/' + inId + '/' + authory);
  }

  getListaAutorizaciones() {
    // const headers = this._util.getHeaders();
    // return this.http.get(this.urlAuthority, { headers });
    return this._commom.peticionGetConHeader(this.urlAuthority);
  }

  //para el crud de authority
  guardarAutorizaciones(obj: Authority) {
    // const headers = this._util.getHeaders();
    // return this.http.post<Authority>(this.urlAuthority, obj, { headers });
    return this._commom.peticionPostConHeader(this.urlAuthority, obj);
  }

  actualizarAutorizaciones(obj: Authority) {
    // const headers = this._util.getHeaders();
    // return this.http.put<Authority>(this.urlAuthority, obj, { headers });
    return this._commom.peticionPutConHeader(this.urlAuthority, obj);
  }

  eliminarAutorizaciones(innId: string) {
    // const headers = this._util.getHeaders();
    // return this.http.delete<Authority>(this.urlAuthority + '/' + innId, { headers });
    return this._commom.peticionDeleteConHeader(this.urlAuthority + '/' + innId);
  }

}
