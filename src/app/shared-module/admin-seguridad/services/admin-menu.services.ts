import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { MantenerMenu } from '../models/mantenerMenu';
// import { url } from '../diccionarios/diccionario.js';
// import { UtilsService } from '../../../services/shared/utils.service';
import { moduloSeguridad } from '../diccionarios/seguridadModulo.js';
import { AdminMenu } from '../models/adminMenu';
import { Observable } from 'rxjs';
import { UtilsService } from 'src/app/shared/utils/utils.service';
import { CommonService } from 'src/app/shared/utils/common.service';

@Injectable({
  providedIn: 'root'
})
export class AdminMenuService {

  mdSeguridad: any;
  url: string;
  urlMenu: string;

  constructor(
    private http: HttpClient,
    private _util: UtilsService,
    private _commom: CommonService
  ) {
    this.mdSeguridad = moduloSeguridad;
    this.url = _util.getSeguridad(this.mdSeguridad.url.seguridad.menu);
    this.urlMenu = _util.getSeguridad(this.mdSeguridad.url.seguridad.admin_menu);
  }

  getAllNivel(innParamNievel, inParamenuSuperior) {
    // const headers = this._util.getHeaders();
    // return this.http.get<AdminMenu>(this.urlMenu + innParamNievel + '/' + inParamenuSuperior, { headers })
    //   .pipe(
    //     map(res => res['respuesta'])
    //   );
    return this._commom.peticionGetConHeader(this.urlMenu + innParamNievel + '/' + inParamenuSuperior);
  }

  guardarMenu(objetoMenu: MantenerMenu) {
    // const headers = this._util.getHeaders();
    // return this.http.post<MantenerMenu[]>(this.url, objetoMenu, { headers })
    //   .pipe(
    //     map(res => res['respuesta'])
    //   );
    return this._commom.peticionPostConHeader(this.url, objetoMenu);
  }

  actualizarMenu(objetoMenu: MantenerMenu) {
    // const headers = this._util.getHeaders();
    // return this.http.put<MantenerMenu>(this.url, objetoMenu, { headers })
    //   .pipe(
    //     map(res => res['respuesta'])
    //   );
    return this._commom.peticionPutConHeader(this.url, objetoMenu);
  }

  eliminarId(inId) {
    // const headers = this._util.getHeaders(); //this._util.getQuery(this.url)
    // return this.http.delete<MantenerMenu>(this.url + '/' + inId, { headers });
    return this._commom.peticionDeleteConHeader(this.url + '/' + inId);
  }

  getMenu() {
    // const headers = this._util.getHeaders();
    // return this.http.get<any[]>(this.url + '/nw/' + localStorage.getItem('userName'), { headers });
    return this._commom.peticionGetConHeader(this.url + '/nw/' + localStorage.getItem('userName'));
  }

  devolverObservableOk(innParametro) {
    return new Observable((observer) => {
      return observer.next(innParametro);
    });
  }
}
