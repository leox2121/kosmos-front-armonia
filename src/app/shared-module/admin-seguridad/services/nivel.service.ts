import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { NivelSeguridadModel } from '../models/nivel.model.js';
import { moduloSeguridad } from '../diccionarios/seguridadModulo.js';
import { UtilsService } from 'src/app/shared/utils/utils.service.js';
import { CommonService } from 'src/app/shared/utils/common.service.js';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class NivelService {

  mdSeguridad: any;
  url: string;

  constructor(
    private http: HttpClient,
    private _util: UtilsService,
    private _commom: CommonService
  ) {
    this.mdSeguridad = moduloSeguridad;
    this.url = this._util.getSeguridad(this.mdSeguridad.url.seguridad.nivel);
  }

  getAll(): Observable<NivelSeguridadModel[]> {
    // const headers = this._util.getHeaders();
    // return this.http.get<NivelSeguridadModel>(this.url, { headers });
    return this._commom.peticionGetConHeader(this.url);
  }

}
