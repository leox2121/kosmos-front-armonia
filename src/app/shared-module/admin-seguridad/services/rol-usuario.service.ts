import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { RolUsuarioModel } from '../models/rolUsuario.model';
import { moduloSeguridad } from '../diccionarios/seguridadModulo.js';
import { UtilsService } from 'src/app/shared/utils/utils.service';
import { CommonService } from 'src/app/shared/utils/common.service';


@Injectable({
  providedIn: 'root'
})
export class RolUsuarioService {

  mdSeguridad: any;
  url: string;

  constructor(
    private http: HttpClient,
    private _util: UtilsService,
    private _commom: CommonService
  ) {
    this.mdSeguridad = moduloSeguridad;
    this.url = this._util.getSeguridad(this.mdSeguridad.url.seguridad.rol_usuario);
  }

  getAll() {
    // const headers = this._util.getHeaders();
    // return this.http.get(this.url, { headers });
    return this._commom.peticionGetConHeader(this.url);
  }

  getUsuario(user) {
    // const headers = this._util.getHeaders();
    // return this.http.get(this.url + '/' + user, { headers });
    return this._commom.peticionGetConHeader(this.url + '/' + user);
  }

  guardarRolUsuario(inRolUsuario: RolUsuarioModel[]) {
    const headers = this._util.getHeaders();
    return this.http.post(this.url, inRolUsuario, { headers });
    // return this._commom.peticionPostConHeader(this.url, inRolUsuario);
  }

  actualizarRolUsuarioId(id, parametro) { }

  eliminarRolUsuarioId(rolId, usuarioId) {
    // const headers = this._util.getHeaders();
    // return this.http.delete(this.url + '/' + rolId + '/' + usuarioId, { headers });
    return this._commom.peticionDeleteConHeader(this.url + '/' + rolId + '/' + usuarioId);
  }
}
