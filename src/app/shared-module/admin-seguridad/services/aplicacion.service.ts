import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { moduloSeguridad } from '../diccionarios/seguridadModulo';
import { UtilsService } from 'src/app/shared/utils/utils.service';
import { CommonService } from 'src/app/shared/utils/common.service';

@Injectable({
  providedIn: 'root'
})
export class AplicacionService {

  mdSeguridad: any;
  url: string;
  urlAplication: string;

  constructor(
    private http: HttpClient,
    private _util: UtilsService,
    private _commom: CommonService
  ) {
    this.mdSeguridad = moduloSeguridad;
    this.urlAplication = _util.getSeguridad(this.mdSeguridad.url.seguridad.aplication);
  }

  getAplication() {
    // return this.http.get(this.urlAplication, { headers });
    return this._commom.peticionGetConHeader(this.urlAplication);
  }

  guardarAplication(inAplication: any) {
    // return this.http.post(this.urlAplication, inAplication, { headers } );
    return this._commom.peticionPostConHeader(this.urlAplication, inAplication);
  }

  eliminarAutorizacion(inId: string) {
    // return this.http.delete(this.urlAplication + '/' + inId , { headers } );
    return this._commom.peticionDeleteConHeader(this.urlAplication + '/' + inId);
  }

}
