export class Authority {
    
        name: string;
        application: string;
    
    constructor(innObj : Authority){
        this.name = innObj.name;
        this.application = innObj.application;
    }
}