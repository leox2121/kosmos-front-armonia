export class UsuarioSeguridadModel {

    id?: number;
    login: string;
    nombre: string;
    apellido: string;
    email: string;
    password: string;
    ldap: number;
    status: number;


  constructor() { }
}
