export class MantenerMenuGuardar {

    id?: number;
    descripcion: string;
    nivelId: number;
    sesionId: number;
    menuSuperior: number;

    constructor(id = null, descripcion = '', nivelId = 1, sesionId = null, menuSuperior = null) {
        this.id = id;
        this.descripcion = descripcion;
        this.nivelId = nivelId;
        this.sesionId = sesionId;
        this.menuSuperior = menuSuperior;
    }//
}

export class AdminMenu {

    id = null;
    descripcion = '';
    nivelId = null;
    nivelDescrip = '';
    sesionDescrip = '';
    menuSuperior = null;

    constructor() {

    }
    setearObjeto(innObjeto: AdminMenu): MantenerMenuGuardar {
        const newObject = new MantenerMenuGuardar();
        newObject.id = innObjeto.id === undefined ? null : innObjeto.id;
        newObject.descripcion = innObjeto.descripcion === undefined ? '' : innObjeto.descripcion;
        newObject.nivelId = innObjeto.nivelId === undefined ? null : innObjeto.nivelId;
        //newObject.nivelDescrip = innObjeto.nivelDescrip === undefined ? '' : innObjeto.nivelDescrip;
        newObject.sesionId = null; //innObjeto.sesionDescrip === undefined ? '' : innObjeto.sesionDescrip;
        newObject.menuSuperior = innObjeto.menuSuperior === undefined ? null : innObjeto.menuSuperior;
        return newObject;
    }
    devolverModeloAdminMenu() {
        const objeto: ModeloAdminMenu = {
            id: 'id',
            descripcion: 'descripcion',
            nivelId: 'nivelId',
            nivelDescrip: 'nivelDescrip',
            sesionDescrip: 'sesionDescrip',
            menuSuperior: 'menuSuperior'
        };
        return objeto;
    }
}//

export interface ModeloAdminMenu {
    id?: string;
    descripcion: string;
    nivelId: string;
    nivelDescrip: string;
    sesionDescrip: string;
    menuSuperior: string;
}

export interface ModeloAdminMenuGuardar {
    id?: string;
    descripcion: string;
    menuSuperior: string;
    nivelId: string;
    sesionId: string;
}



/*  constructor(innObjeto: AdminMenu) {
      //this.marca = innModelo.marca === undefined ? '' : innModelo.marca;
      this.id = innObjeto.id === undefined ? null : innObjeto.id;
      this.descripcion = innObjeto.descripcion === undefined ? '' : innObjeto.descripcion;
      this.nivelId = innObjeto.nivelId === undefined ? null : innObjeto.nivelId;
      this.nivelDescrip = innObjeto.nivelDescrip === undefined ? '' : innObjeto.nivelDescrip;
      this.sesionDescrip = innObjeto.sesionDescrip === undefined ? '' : innObjeto.sesionDescrip;
      this.menuSuperior = innObjeto.menuSuperior === undefined ? null : innObjeto.menuSuperior;
   } */
/*"descripcion": "Seguridad-Sistema",
            "id": 1,
            "nivelDescrip": "Nivel1",
            "nivelId": 1,
            "sesionDescrip": ""*/