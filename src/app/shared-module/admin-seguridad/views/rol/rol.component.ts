import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AgGridAngular } from 'ag-grid-angular';
import { buttons, mensajes, buttonsCss } from '../../diccionarios/diccionario';
import { RolService } from '../../services/rol.service';
import { moduloSeguridad } from '../../diccionarios/seguridadModulo';
import { SesionService } from '../../services/sesion.service';
import { RolSesionService } from '../../services/rol-sesion.service';
import { UtilsService } from 'src/app/shared/utils/utils.service';
import { CommonFormService } from 'src/app/shared/utils/common-form.service';
import { LfsaBotonEditarGridComponent } from 'src/app/shared/components/lfsa-boton-editar-grid/lfsa-boton-editar-grid.component';

@Component({
  selector: 'app-rol',
  templateUrl: './rol.component.html',
  styleUrls: ['./rol.component.css']
})
export class RolComponent implements OnInit {

  formulario: FormGroup;
  formularioAgregarRol: FormGroup;

  mostrarFormulario = false;
  editableForm = false;

  //GRID ROLES
  @ViewChild('agGrid') agGrid: AgGridAngular;
  columnDefs: any[];
  rowData: any[];
  frameworkComponents: any;
  rowSelection: any;
  gridApi: any;
  gridColumnApi: any;

  columnDefsRol: any[];
  rowDataRol: any[];
  rowSelectionRol: any;
  gridApiRol: any;
  gridColumnApiRol: any;

  textoBuscar: string;

  DAT_BTNS;
  DAT_BTNCSS;
  DAT_MSG;
  textBoton: string;

  diccionarioModulo: any;
  mostrarGridLista = true;
  cmbListaSession: any[];

  constructor(
    private fb: FormBuilder,
    private _util: UtilsService,
    private _rol: RolService,
    private sesionService: SesionService,
    private _rolSesion: RolSesionService,
    private _commonForm: CommonFormService
  ) {
    this.diccionarioModulo = moduloSeguridad;
  }

  ngOnInit() {
    this.DAT_BTNS = moduloSeguridad.button.buttonsNombre;
    this.DAT_MSG = moduloSeguridad.mensajes;
    this.DAT_BTNCSS = moduloSeguridad.button.buttonsCss;

    this.formulario = this.fb.group({
      id: [''],
      descripcion: ['', [Validators.required, Validators.maxLength(50)]]
    });

    this.formularioAgregarRol = this.fb.group({
      rolId: ['', [Validators.required]],
      sesionId: ['', [Validators.required, Validators.maxLength(50)]]
    });

    this.frameworkComponents = {
      buttonRenderer: LfsaBotonEditarGridComponent,
    };

    this.columnDefs = [
      {
        headerName: 'DESCRIPCION',
        field: 'descripcion',
        sortable: true,
        filter: true,
        // checkboxSelection: true,
        // headerCheckboxSelection: true,
        width: 400
      },
      {
        headerName: '',
        cellRenderer: 'buttonRenderer',
        width: 150,
        cellRendererParams: {
          onClick: this.editarLinea.bind(this),
          label: this.DAT_BTNS.editar,
          colorBoton: buttonsCss.info
        }
      },
      {
        headerName: '',
        cellRenderer: 'buttonRenderer',
        width: 150,
        cellRendererParams: {
          onClick: this.eliminarLinea.bind(this),
          label: this.DAT_BTNS.eliminar,
          colorBoton: buttonsCss.danger
        }
      },
      {
        headerName: '',
        cellRenderer: 'buttonRenderer',
        width: 170,
        cellRendererParams: {
          onClick: this.agregarRol.bind(this),
          label: 'Sesiones',//this.DAT_BTNS.agregarRol,
          colorBoton: buttonsCss.warning
        }
      }
    ];
    this.rowSelection = 'single';
    this.columnDefsRol = [
      {
        headerName: 'ROL',
        field: 'rolDescripcion',
        sortable: true,
        filter: true,
        width: 250
      },
      {
        headerName: 'SESION',
        field: 'sesionDescripcion',
        sortable: true,
        filter: true,
        width: 300
      },
      {
        headerName: '',
        cellRenderer: 'buttonRenderer',
        width: 150,
        cellRendererParams: {
          onClick: this.eliminarLineaRol.bind(this),
          label: this.DAT_BTNS.eliminar,
          colorBoton: buttonsCss.danger
        }
      }
    ];
    this.rowSelection = 'single';

    this.getAllRoles();
  }// End Oninit

  /** FUNCIONES */

  onGridReady(params) {
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;
  }

  onQuickFilterChanged() {
    this.gridApi.setQuickFilter(this.textoBuscar);
  }

  nuevo() {
    this.mostrarGridLista = false;
    this.textBoton = buttons.guardar;
    this.editableForm = false;
    this.formulario.reset();
  }

  editarLinea({ rowData }) {
    this.mostrarGridLista = false;
    this.textBoton = buttons.editar;
    this.editableForm = true;
    this.formulario.setValue(rowData);
  }

  acciones(event) {
    if (event === 'guardar') {
      if (this.editableForm) {
        // this._util.confirmarAcyncronico(this, { title: this.DAT_MSG.actualizar, text: ``, action: this.DAT_BTNS.editar }, () => {
        // this.mostrarGridLista = true;
        this.actualizarRolId();
        // });
      } else {
        // this._util.confirmarAcyncronico(this, { title: this.DAT_MSG.guardar, text: ``, action: this.DAT_BTNS.guardar }, () => {
        // this.mostrarGridLista = true;
        this.guardarRol();
        // });
      }
    } else {
      this.mostrarGridLista = true;
      //this._commonForm.limpiarYregrear(this);
    }
  }

  agregarRol({ rowData }) {
    this.editableForm = false;
    this.mostrarFormulario = true;
    this.formularioAgregarRol.get('rolId').setValue(rowData.id);
    this.obtenerSesiones();
    this.obtenerSesionesByRol(rowData.id);
  }

  /**SERVICIOS */
  getAllRoles() {
    this._rol.getAll().subscribe((res: any) => {
      this.rowData = res;
    });
  }

  eliminarLinea({ rowData }) {
    this._util.confirmarSincronoPromesa(
      {
        title: this.DAT_MSG.eliminar,
        text: ``,
        action: this.DAT_BTNS.eliminar,
        funcion: this._rol.eliminarRolId(rowData.id),
      }
    ).then((res: any) => {
      this.gridApi.updateRowData({ remove: this.gridApi.getSelectedRows() });
      console.log(res);
    });
  }

  guardarRol() {
    // return this._rol.guardarRol(this.formulario.value);
    this._util.confirmarSincronoPromesa(
      {
        title: this.DAT_MSG.guardar,
        text: ``,
        action: this.DAT_BTNS.guardar,
        funcion: this._rol.guardarRol(this.formulario.value),
        // loading: false,
        // context: this
      }
    ).then((res: any) => {
      this.getAllRoles();
      this.formulario.reset();
      this.mostrarGridLista = true;
    });
  }

  actualizarRolId() {
    // return this._rol.actualizarRolId(this.formulario.getRawValue());
    this._util.confirmarSincronoPromesa(
      {
        title: this.DAT_MSG.actualizar,
        text: ``,
        action: this.DAT_BTNS.editar,
        funcion: this._rol.actualizarRolId(this.formulario.getRawValue()),
        // loading: false,
        // context: this
      }
    ).then((res: any) => {
      this.getAllRoles();
      this.formulario.reset();
      this.mostrarGridLista = true;
    });
  }

  obtenerSesiones() {
    this._rol.getAllSessiones().subscribe((resp) => {
      this.cmbListaSession = resp;
    });
  }

  obtenerSesionesByRol(innIdRol) {
    this._rol.getSesionesByRol(innIdRol).subscribe((resp) => {
      this.rowDataRol = resp;
    },(err)=>{
      this.rowDataRol = [];
    });
  }

  accionesAgregarRoles(event) {
    if (event === 'guardar') {
      console.log(JSON.stringify(this.formularioAgregarRol.value));
      this._util.confirmarSincronoPromesaSinActions(
        {
          title: this.DAT_MSG.guardar,
          text: '',
          action: '',
          msn: 'ok'
        }
      ).then((resp: any) => {
        this._rolSesion.guardarRolSesion(this.formularioAgregarRol.getRawValue()).subscribe((resp) => {
          this._util.mensajeOKVisto('ok', false, 1500);
          this.obtenerSesionesByRol(this.formularioAgregarRol.get('rolId').value);
        }, (error) => {
          this._util.mensajeErrorVisto(error.error.mensaje, false, 1500);
        });
      }).catch((err) => {
      });
    } else {
      this.mostrarFormulario = false;
      this.mostrarGridLista = true;
    }
  }

  eliminarLineaRol({ rowData }) {
    this._util.confirmarSincronoPromesaSinActions(
      {
        title: 'Esta seguro que desea eliminar el siguiente registro ( ' + rowData.sesionDescripcion + ')',
        text: '',
        action: '',
        msn: 'Eliminada'
      }
    ).then((resp: any) => {
      this._rolSesion.eliminarRolSesionId(rowData.rolId, rowData.sesionId).subscribe((resp) => {
        this.obtenerSesionesByRol(rowData.rolId);
        this._util.mensajeOKVisto('sesion eliminada (' + rowData.sesionDescripcion + ')', false, 1500);
      });
    }).catch((err) => {
    });
  }
}
