import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
// import { Router } from '@angular/router';
import { AgGridAngular } from 'ag-grid-angular';
import { AutorizacionService } from '../../services/autorizacion.service';
import { RolService } from '../../services/rol.service';
import { RolSeguridadModel } from '../../models/rolSeguridad.model';
import { UsuarioSeguridadModel } from '../../models/usuarioSeguridad.model';
import { moduloSeguridad } from '../../diccionarios/seguridadModulo';
import { buttons, mensajes, buttonsCss } from '../../diccionarios/diccionario';
import { UtilsService } from 'src/app/shared/utils/utils.service';
import { CommonFormService } from 'src/app/shared/utils/common-form.service';
import { UsuarioService } from '../../services/usuario.service';
import { RolUsuarioService } from '../../services/rol-usuario.service';
import { LfsaBotonEditarGridComponent } from 'src/app/shared/components/lfsa-boton-editar-grid/lfsa-boton-editar-grid.component';

@Component({
  selector: 'app-usuario',
  templateUrl: './usuario.component.html',
  styleUrls: ['./usuario.component.css']
})
export class UsuarioComponent implements OnInit {

  formulario: FormGroup;
  formularioRol: FormGroup;
  formularioBuscar: FormGroup;
  formularioAuthority: FormGroup;

  mostrarFormulario = 1;

  mostrarRol = false;
  editableForm = false;

  cmbDisabled = false;
  hide = false;
  userRol: string;
  userId: string;
  password: boolean;
  @ViewChild('agGrid') agGrid: AgGridAngular;
  @ViewChild('openModal') openModal;
  columnDefs: any[];
  columnDefsRol: any[];
  columnDefsAuthority: any[];

  rowData: any[];
  rowDataRol: any[];
  rowDataAutorizacion: any[];

  frameworkComponents: any;
  rowSelection: any;
  isRowSelectable: any;

  gridApi: any;
  gridColumnApi: any;

  gridApiRol: any;
  gridColumnApiRol: any;

  gridApiAutorizacion: any;
  gridColumnApiAutorizacion: any;

  textoBuscar: string;
  DAT_BTNS;
  DAT_MSG;
  DAT_BTNCSS;
  textBoton: string;
  cmbListRol: RolSeguridadModel[];
  cmbListUsuario: UsuarioSeguridadModel[];
  status: any[];
  authority: any[];
  diccionarioModulo: any;
  habiliarLogin2 = true;
  ldapBoll: boolean = false;

  loading: boolean = false;

  FROM_GRID = 1;
  FORM_EDITAR = 2;
  FORM_ROLES = 3;

  constructor(private fb: FormBuilder,
    private _util: UtilsService,
    private _usuario: UsuarioService,
    private _commonForm: CommonFormService,
    private _rolUsuario: RolUsuarioService,
    private _rol: RolService,
    private autorizacionService: AutorizacionService) {
    this.diccionarioModulo = moduloSeguridad;
  }

  ngOnInit() {

    this.password = false;

    this._usuario.getAll().subscribe((res: any) => {
      this.rowData = res.respuesta;
      this.cmbListUsuario = res.respuesta;
    });

    this._rol.getAll().subscribe((res: any) => {
      // this.cmbListRol = res.respuesta;
      this.cmbListRol = res;
    });

    this.autorizacionService.getListaAutorizaciones().subscribe((res: any) => {
      // this.authority = res.respuesta;
      this.authority = res;
    });

    this.DAT_BTNS = moduloSeguridad.button.buttonsNombre;
    this.DAT_MSG = moduloSeguridad.mensajes;
    this.DAT_BTNCSS = moduloSeguridad.button.buttonsCss;

    this.status = [{ "id": 1, "valor": "Activo" }, { "id": 2, "valor": "Inactivo" }];

    this.formulario = this.fb.group({
      id: ['', [Validators.required]],
      nombre: ['', [Validators.required, Validators.maxLength(50)]],
      apellido: ['', [Validators.required, Validators.maxLength(50)]],
      login: ['', [Validators.required, Validators.maxLength(50)]],
      login2: [''],
      email: ['', [Validators.required, Validators.maxLength(100), Validators.email]],
      status: ['', [Validators.required]],
      ldap: ['', [Validators.required]]
    });

    this.formularioBuscar = this.fb.group({
      nombreUser: ['', [Validators.required]],
    });
    this.formularioRol = this.fb.group({
      usuarioId: [''],
      rolId: ['', [Validators.required]]
    });

    this.formularioAuthority = this.fb.group({
      userId: [''],
      authorityName: ['', [Validators.required]]
    });

    this.frameworkComponents = {
      buttonRenderer: LfsaBotonEditarGridComponent,
    };

    this.columnDefs = [
      {
        headerName: 'NOMBRE',
        field: 'nombre',
        sortable: true,
        width: 260,
        // checkboxSelection: true,
        // headerCheckboxSelection: true,
        filter: true
      },
      {
        headerName: 'APELLIDO',
        field: 'apellido',
        sortable: true,
        filter: true
      },
      {
        headerName: 'LOGIN',
        field: 'login',
        sortable: true,
        //width: 115,
        filter: true
      },
      {
        headerName: 'USUARIO',
        field: 'login2',
        sortable: true,
        width: 150,
        filter: true
      },
      {
        headerName: 'ESTADO',
        field: 'status',
        sortable: true,
        filter: true,
        width: 150,
        valueFormatter: this.estado
      },
      // {
      //   headerName: 'LDAP',
      //   field: 'ldap',
      //   sortable: true,
      //   filter: true,
      //   //width: 115,
      //   valueFormatter: this.ldap
      // }
      {
        headerName: 'EDITAR',
        cellRenderer: 'buttonRenderer',
        width: 120,
        cellRendererParams: {
          onClick: this.editarLinea.bind(this),
          label: this.DAT_BTNS.editar,
          colorBoton: buttonsCss.info
        }
      },
      {
        headerName: 'ROL',
        cellRenderer: 'buttonRenderer',
        width: 130,
        cellRendererParams: {
          onClick: this.agregarRolUsuario.bind(this),
          label: 'Add Rol',//this.DAT_BTNS.agregarRol,
          colorBoton: buttonsCss.warning
        }
      },
    ];

    this.columnDefsRol = [
      {
        headerName: 'ROL',
        field: 'rolDescripcion',
        sortable: true,
        filter: true,
        width: 400,
        checkboxSelection: true,
      },
      {
        headerName: '',
        cellRenderer: 'buttonRenderer',
        width: 130,
        cellRendererParams: {
          onClick: this.eliminarRol.bind(this),
          label: this.DAT_BTNS.eliminar,
          colorBoton: buttonsCss.danger
        }
      }
    ];

    this.columnDefsAuthority = [
      {
        headerName: 'AUTORIZACIÓN',
        field: 'authorityName',
        sortable: true,
        filter: true,
        checkboxSelection: true,
        width: 400,
      },
      {
        headerName: '',
        cellRenderer: 'buttonRenderer',
        width: 130,
        cellRendererParams: {
          onClick: this.eliminarAutorizacion.bind(this),
          label: this.DAT_BTNS.eliminar,
          colorBoton: buttonsCss.danger
        }
      }
    ];

    this.rowSelection = 'multiple';

    this.isRowSelectable = function (rowNode) {
      return rowNode.data ? rowNode.data.status === true : false;
    };
  }

  demo(params) {
    if (params.data.status) {
      return params.data;
    } else {
      return false;
    }
  }

  estado(params) {
    if (params.data.status === 1) {
      return 'Activo';
    } else {
      return 'Inactivo';
    }
  }

  ldap(params) {
    if (params.data.ldap === 1) {
      return 'Activo';
    } else {
      return 'Inactivo';
    }
  }

  ldapNumber(params) {
    if (params.ldap === 1) {
      this.ldapBoll = true;
    } else {
      this.ldapBoll = false;
    }
  }

  ldapPassword(params) {
    if (params === 1) {
      this.password = false;
      this.formulario.get('login2').enable();
      this.habiliarLogin2 = true;
    } else if (params === 2) {
      this.password = true;
      this.formulario.get('login2').setValue('');
      this.habiliarLogin2 = false;
    }
  }

  onQuickFilterChanged() {
    this.gridApi.setQuickFilter(this.textoBuscar);
  }

  nuevo() {
    this.formulario.get('id').disable();
    //this.formulario.get('password').enable();
    // this._commonForm.abrirFormularioParaGuardar(this);
    this.textBoton = 'Guardar';
    this.editableForm = false;
    this.mostrarFormulario = this.FORM_EDITAR;
    this.formulario.reset();
  }

  editarLinea({ rowData }) {
    if (rowData.password !== '') {
      this.password = true;
    }
    this.textBoton = this.DAT_BTNS.editar;
    this.editableForm = true;
    this.mostrarFormulario = this.FORM_EDITAR;
    delete rowData.password; //quita el atributo
    this.formulario.setValue(rowData);
    this.ldapPassword(rowData.ldap);
    this.ldapNumber(rowData);
    console.log(this.formulario);
  }

  agregarRolUsuario({ rowData }) {
    this.userRol = rowData.login;
    this.userId = rowData.id;
    // this.mostrarRol = true;
    this.mostrarFormulario = this.FORM_ROLES;
    this.formularioRol.reset();
    this.formularioAuthority.reset();
    this.formularioRol.controls.usuarioId.setValue(rowData.id);
    this.formularioAuthority.controls.userId.setValue(rowData.id);
    this.cmbDisabled = true;
    this._rolUsuario.getUsuario(rowData.login).subscribe((resp: any) => {
      // this.rowDataRol = resp.respuesta;
      this.rowDataRol = resp;
    });

    this.autorizacionService.getAutorizacion(rowData.id).subscribe((resp: any) => {
      // this.rowDataAutorizacion = resp.respuesta;
      this.rowDataAutorizacion = resp;
    });
  }

  eliminarLinea({ rowData }) {
    // this._util.confirmarAcyncronico(this, {title: this.DAT_MSG.eliminar, text: ``, action: this.DAT_BTNS.eliminar}, () => {
    //   return this.eliminarUsuarioId(rowData.id);
    // }, () => {
    //   this.rowData = this.rowData.filter(item => item.id !== rowData.id);
    // });
    this._util.confirmarSincronoPromesa(
      {
        title: this.DAT_MSG.eliminar,
        text: ``,
        action: this.DAT_BTNS.eliminar,
        funcion: this.eliminarUsuarioId(rowData.id),
        // loading: false,
        // context: this
      }
    ).then((res: any) => {
      this.rowData = this.rowData.filter(item => item.id !== rowData.id);
    });
  }

  onGridReady(params) {
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;
    this._usuario.getAll().subscribe((res: any) => {
      this.rowData = res.respuesta;
    });
  }

  guardarUsuario() {
    // return this._usuario.guardarUsuario(this.formulario.value);
    this._util.confirmarSincronoPromesa(
      {
        title: this.DAT_MSG.guardar,
        text: ``,
        action: this.DAT_BTNS.guardar,
        funcion: this._usuario.guardarUsuario(this.formulario.value),
        // loading: false,
        // context: this
      }
    ).then((res: any) => {
      console.log(res);
    });
  }

  actualizarUsuario() {
    // return this._usuario.actualizarUsuarioId(this.formulario.getRawValue());
    this._util.confirmarSincronoPromesa(
      {
        title: this.DAT_MSG.actualizar,
        text: ``,
        action: this.DAT_BTNS.editar,
        funcion: this._usuario.actualizarUsuarioId(this.formulario.getRawValue()),
        // loading: false,
        // context: this
      }
    ).then((res: any) => {
      console.log(res);
    });
  }

  eliminarUsuarioId(id) {
    return this._usuario.eliminarUsuarioId(id);
  }

  acciones(event) {
    debugger
    if (event === 'guardar') {
      if (this.editableForm) {
        // this._util.confirmarAcyncronico(this, { title: this.DAT_MSG.actualizar, text: ``, action: this.DAT_BTNS.editar }, () => {
        this.actualizarUsuario();
        // });
      } else {
        // this._util.confirmarAcyncronico(this, { title: this.DAT_MSG.guardar, text: ``, action: this.DAT_BTNS.guardar }, () => {
        this.guardarUsuario();
        // });
      }
    } else {
      // this._commonForm.limpiarYregrear(this);
      this.mostrarFormulario = this.FROM_GRID;
    }
  }

  // ROL-USUARIO

  onGridReadyRol(params) {
    this.gridApiRol = params.api;
    this.gridColumnApiRol = params.columnApi;
  }

  eliminarRol({ rowData }) {
    // this._util.confirmarAcyncronico(this, { title: this.DAT_MSG.eliminar, text: ``, action: this.DAT_BTNS.eliminar }, () => {
    //   return this._rolUsuario.eliminarRolUsuarioId(rowData.rolId, rowData.usuarioId);
    // }, () => {
    //   this.gridApiRol.updateRowData({ remove: this.gridApiRol.getSelectedRows() });
    // });
    this._util.confirmarSincronoPromesa(
      {
        title: this.DAT_MSG.eliminar,
        text: ``,
        action: this.DAT_BTNS.eliminar,
        funcion: this._rolUsuario.eliminarRolUsuarioId(rowData.rolId, rowData.usuarioId),
        // loading: false,
        // context: this
      }
    ).then((res: any) => {
      this.gridApiRol.updateRowData({ remove: this.gridApiRol.getSelectedRows() });
      console.log(res);
    });
  }

  guardarRol() {
    // this._util.confirmarAcyncronico(this, { title: this.DAT_MSG.guardar, text: ``, action: this.DAT_BTNS.guardar }, () => {
    //   return this._rolUsuario.guardarRolUsuario(this.formularioRol.getRawValue());
    // }, () => {
    //   this._rolUsuario.getUsuario(this.userRol).subscribe((resp: any) => {
    //     this.rowDataRol = resp.respuesta;
    //   });
    //   this.formularioRol.get('rolId').reset();
    // });
    this._util.confirmarSincronoPromesa(
      {
        title: this.DAT_MSG.guardar,
        text: ``,
        action: this.DAT_BTNS.guardar,
        funcion: this._rolUsuario.guardarRolUsuario(this.formularioRol.getRawValue()),
        // loading: false,
        // context: this
      }
    ).then((res: any) => {
      console.log(res);
      this._rolUsuario.getUsuario(this.userRol).subscribe((resp: any) => {
        // this.rowDataRol = resp.respuesta;
        this.rowDataRol = resp;
      });
    });
  }

  // AUTORIZACION-USUARIO

  onGridReadyAutorizacion(params) {
    this.gridApiAutorizacion = params.api;
    this.gridColumnApiAutorizacion = params.columnApi;
  }

  guardarAutorizacion() {
    // this._util.confirmarAcyncronico(this, { title: this.DAT_MSG.guardar, text: ``, action: this.DAT_BTNS.guardar }, () => {
    //   return this.autorizacionService.guardarAutorizacion(this.formularioAuthority.getRawValue());
    // }, () => {
    //   this.autorizacionService.getAutorizacion(this.userId).subscribe((resp: any) => {
    //     this.rowDataAutorizacion = resp.respuesta;
    //   });
    //   this.formularioAuthority.get('authorityName').reset();
    // });
    this._util.confirmarSincronoPromesa(
      {
        title: this.DAT_MSG.guardar,
        text: ``,
        action: this.DAT_BTNS.guardar,
        funcion: this.autorizacionService.guardarAutorizacion(this.formularioAuthority.getRawValue()),
        // loading: false,
        // context: this
      }
    ).then((res: any) => {
      console.log(res);
      this.autorizacionService.getAutorizacion(this.userId).subscribe((resp: any) => {
        this.rowDataAutorizacion = resp.respuesta;
      });
      this.formularioAuthority.get('authorityName').reset();
    });
  }

  eliminarAutorizacion({ rowData }) {
    // this._util.confirmarAcyncronico(this, { title: this.DAT_MSG.eliminar, text: ``, action: this.DAT_BTNS.eliminar }, () => {
    //   return this.autorizacionService.eliminarAutorizacion(rowData.userId, rowData.authorityName);
    // }, () => {
    //   this.gridApiAutorizacion.updateRowData({ remove: this.gridApiAutorizacion.getSelectedRows() });
    // });
    this._util.confirmarSincronoPromesa(
      {
        title: this.DAT_MSG.eliminar,
        text: ``,
        action: this.DAT_BTNS.eliminar,
        funcion: this.autorizacionService.eliminarAutorizacion(rowData.userId, rowData.authorityName),
        // loading: false,
        // context: this
      }
    ).then((res: any) => {
      console.log(res);
      this.gridApiAutorizacion.updateRowData({ remove: this.gridApiAutorizacion.getSelectedRows() });
    });
  }

  regresar() {
    this.formularioRol.reset();
    this.formularioAuthority.reset();
    // this.mostrarRol = false;
    this.mostrarFormulario = this.FROM_GRID;
  }

  buscarUsuario() {
    this._usuario.buscarUsuarioLdap(this.formularioBuscar.get('nombreUser').value).subscribe((resp: any) => {
      // this.formulario.get('status').setValue(resp.status);
      delete resp['password'];
      this.formulario.setValue(resp);
      if (resp.ldap === 1) {
        document.getElementById('radActivo').click();
      }
    }, (error) => {
      this.formulario.reset();
      this._util.mensajeErrorVisto(error.error.mensaje, false, 2000);
    });
  }
} //end class
