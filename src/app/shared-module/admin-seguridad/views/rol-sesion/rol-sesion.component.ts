import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AgGridAngular } from 'ag-grid-angular';
import { UtilsService } from '../../../../services/shared/utils.service';
import { BotonEditarGridComponent } from '../../../../shared/boton-editar-grid/boton-editar-grid.component';
import { CommonFormService } from '../../../../services/shared/common-form.service';

import { RolService } from '../../services/rol.service';
import { RolSesionService } from '../../services/rol-sesion.service.js';
import { RolSeguridadModel } from '../../models/rolSeguridad.model.js';
import { SesionSeguridadModel } from '../../models/sesionSeguridad.model.js';
import { SesionService } from '../../services/sesion.service.js';
import { moduloSeguridad } from '../../diccionarios/seguridadModulo.js';
import { buttons, mensajes, buttonsCss } from '../../diccionarios/diccionario.js';

@Component({
  selector: 'app-rol-sesion',
  templateUrl: './rol-sesion.component.html',
  styleUrls: ['./rol-sesion.component.css']
})
export class RolSesionComponent implements OnInit {

  formulario: FormGroup;
  mostrarFormulario = false;
  editableForm = false;
  @ViewChild('agGrid') agGrid: AgGridAngular;
  columnDefs: any[];
  rowData: any[];
  frameworkComponents: any;
  rowSelection: any;
  gridApi: any;
  gridColumnApi: any;
  textoBuscar: string;
  DAT_BTNS;
  DAT_BTNCSS;
  DAT_MSG;
  textBoton: string;
  cmbListRol: RolSeguridadModel[];
  cmbListSesion: SesionSeguridadModel[];
  buscarListaSession: SesionSeguridadModel[];
  cmbListAplicacion: SesionSeguridadModel[];
  cmbDisabled: boolean;
  diccionarioModulo: any;

  constructor(private fb: FormBuilder,
              private servicioUtils: UtilsService,
              private rolSesionService: RolSesionService,
              private commonFormService: CommonFormService,
              private rolService: RolService,
              private sesionServicio: SesionService ) {
                this.diccionarioModulo = moduloSeguridad;
              }

  ngOnInit() {
    this.DAT_BTNS = moduloSeguridad.button.buttonsNombre;
    this.DAT_MSG = moduloSeguridad.mensajes;
    this.DAT_BTNCSS = moduloSeguridad.button.buttonsCss;

    this.rolService.getAll().subscribe((res: any) => {
      this.cmbListRol = res.respuesta;
    });

    this.sesionServicio.getAll().subscribe((res: any) => {
      this.cmbListSesion = res.respuesta;
      this.buscarListaSession = res.respuesta;
    });

    this.getListaAplicacion();
    this.formulario = this.fb.group({
      sesionId: ['', [Validators.required]],
      rolId: ['', [Validators.required]],
      application: ['']
    });

    this.frameworkComponents = {
      buttonRenderer: BotonEditarGridComponent,
    };

    this.columnDefs = [
      {
        headerName: 'DESCRIPCION',
        field: 'sesionDescripcion',
        sortable: true, filter: true ,
        checkboxSelection: true,
        headerCheckboxSelection: true,
        width: 400
      },
      {
        headerName: 'COD. SESION',
        field: 'codSesion',
        sortable: true,
        filter: true,
        width: 250
      },
      {
        headerName: 'ROL',
        field: 'rolDescripcion',
        sortable: true,
        filter: true
      },
      {
        headerName: '',
        cellRenderer: 'buttonRenderer',
        // width: 115,
        cellRendererParams: {
          onClick: this.eliminarLinea.bind(this),
          label: this.DAT_BTNS.eliminar,
          colorBoton: this.DAT_BTNCSS.danger
        }
      }
    ];
    /*,
      {
        headerName: '',
        cellRenderer: 'buttonRenderer',
        // width: 100,
        cellRendererParams: {
          onClick: this.editarLinea.bind(this),
          label: this.DAT_BTNS.editar,
          colorBoton: this.DAT_BTNCSS.info
        }
      } */
    this.rowSelection = 'multiple';
  } //end end OnInit

  getListaAplicacion() {
    this.sesionServicio.getAplicacion().subscribe( (resp) => {
      this.cmbListAplicacion = resp.respuesta;
      //console.log(JSON.stringify(resp));
    });
  }//

  onQuickFilterChanged() {
    this.gridApi.setQuickFilter(this.textoBuscar);
  }

  nuevo() {
    this.commonFormService.abrirFormularioParaGuardar(this);
    this.cmbDisabled = false;
  }

  editarLinea({rowData}) {}

  eliminarLinea({rowData}) {
    this.servicioUtils.confirmarAcyncronico(this, {title: this.DAT_MSG.eliminar, text: ``, action: this.DAT_BTNS.eliminar}, () => {
      return this.rolSesionService.eliminarRolSesionId(rowData.rolId, rowData.sesionId);
    }, () => {
      this.rowData = this.rowData.filter(item => {
        return item.rolId !== rowData.rolId && item.sesionId !== rowData.sesionId;
      } );
    });
  }

  onGridReady(params) {
    this.commonFormService.onGridReadyUtil(this, params, this.rolSesionService);
  }

  guardarRolSesion() {
    return this.rolSesionService.guardarRolSesion(this.formulario.value);
  }

  actualizarRolSesionId(id) {}

  eliminarRolSesionId(rolId, sesionId) {
    return this.rolSesionService.eliminarRolSesionId(rolId, sesionId);
  }

  acciones(event) {
    if (event === 'guardar') {
      if (this.editableForm) {
      // actualizar
      } else {
        this.servicioUtils.confirmarAcyncronico(this, {title: this.DAT_MSG.guardar, text: ``, action: this.DAT_BTNS.guardar}, () => {
          return this.guardarRolSesion();
        });
      }
    } else {
      this.commonFormService.limpiarYregrear(this);
    }
  }

  changeAplicacion(target) {
    const posicion = target.options.selectedIndex;
    const nombre = target[posicion].text;
    this.cmbListSesion = this.buscarListaSession.filter( (item) => {
              if (item.application === nombre) {
                  return item;
              }
    });
    //this.formulario.get(this.estructuraModeloAgregarVehiculo.cedula).setValue(mostrarCedula[0].cedula);
    //alert('Dato Seleccionado---->' + nombre + 'cedula-->' + mostrarCedula[0].cedula);
  }
} //end class
