import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RolSesionComponent } from './rol-sesion.component';

describe('RolSesionComponent', () => {
  let component: RolSesionComponent;
  let fixture: ComponentFixture<RolSesionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RolSesionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RolSesionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
