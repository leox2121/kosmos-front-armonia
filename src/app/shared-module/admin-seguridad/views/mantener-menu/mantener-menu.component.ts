import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, Validators, AbstractControl } from '@angular/forms';
import { AgGridAngular } from 'ag-grid-angular';
import { UtilsService } from '../../../../services/shared/utils.service';
import { buttons, mensajes, buttonsCss } from '../../diccionarios/diccionario.js';
import { BotonEditarGridComponent } from '../../../../shared/boton-editar-grid/boton-editar-grid.component';
import { MantenerUsuarioService } from '../../services/mantener-menu.service';
import { CommonFormService } from '../../../../services/shared/common-form.service';
import { SesionService } from '../../services/sesion.service';
import { NivelService } from '../../services/nivel.service';
import { moduloSeguridad } from '../../diccionarios/seguridadModulo.js';

@Component({
  selector: 'app-mantener-menu',
  templateUrl: './mantener-menu.component.html',
  styleUrls: ['./mantener-menu.component.css']
})
export class MantenerMenuComponent implements OnInit {
  
  diccionarioModulo: any;

  constructor(private fb: FormBuilder,
              private servicioUtils: UtilsService,
              private mantenerMenu: MantenerUsuarioService,
              private commonFormService: CommonFormService,
              private sesionServicio: SesionService,
              private nivelServicio: NivelService
              ) {
                this.diccionarioModulo = moduloSeguridad;
              }

  formulario: FormGroup;
  mostrarFormulario = false;
  mostrarMenu = false;
  editableForm = false;
  cmbDisabled: boolean;
  cmbDisabled1: boolean;
  hide = false;
  @ViewChild('agGrid') agGrid: AgGridAngular;
  @ViewChild('openModal') openModal;
  columnDefs: any[];
  rowData;
  frameworkComponents: any;
  rowSelection: any;
  gridApi: any;
  gridColumnApi: any;
  textoBuscar: string;
  DAT_BTNS;
  DAT_MSG;
  DAT_BTNCSS;
  textBoton: string;
  id: boolean;
  cmbNivel: any [];
  cmbMenuSuperior: any [];
  cmbSesion: any [];



  ngOnInit() {
    this.DAT_BTNS = moduloSeguridad.button.buttonsNombre;
    this.DAT_MSG = moduloSeguridad.mensajes;
    this.DAT_BTNCSS = moduloSeguridad.button.buttonsCss;

    this.nivelServicio.getAll().subscribe((res: any) => {
          this.cmbNivel = res.respuesta;
    });

    this.sesionServicio.getAll().subscribe((res: any) => {
          this.cmbSesion = res.respuesta;
    });

    this.formulario = this.fb.group({
      id: [''],
      descripcion: ['', [Validators.required]],
      menuSuperior: [null],
      nivelId: ['', [Validators.required]],
      sesionId: [null],
    });

    this.frameworkComponents = {
      buttonRenderer: BotonEditarGridComponent,
    };

    this.columnDefs = [
      {
        headerName: 'DESCRIPCION',
        field: 'descripcion',
        sortable: true,
        resizable: true,
        filter: true
      },
      {
        headerName: 'NIVEL',
        field: 'nivel.descripcion',
        sortable: true,
        // width: 112,
        resizable: true,
        filter: true
      },
      {
        headerName: 'MENU SUPERIOR',
        field: 'menuSuperior.descripcion',
        sortable: true,
        resizable: true,
        filter: true
      },
      {
        headerName: 'APLICACION',
        field: 'sesion.application',
        sortable: true,
        // width: 160,
        resizable: true,
        filter: true
      },
      {
        headerName: 'SESION',
        field: 'sesion.descripcion',
        sortable: true,
        // width: 160,
        resizable: true,
        filter: true
      },
      {
        headerName: '',
        cellRenderer: 'buttonRenderer',
        // width: 150,
        cellRendererParams: {
          onClick: this.editarLinea.bind(this),
          label: this.DAT_BTNS.editar,
          colorBoton: buttonsCss.info
        }
      },
      {
        headerName: '',
        cellRenderer: 'buttonRenderer',
        cellRendererParams: {
          onClick: this.eliminarLinea.bind(this),
          label: this.DAT_BTNS.eliminar,
          colorBoton: buttonsCss.danger
        }
      }
    ];
    this.rowSelection = 'multiple';
  /*{
        headerName: 'ID',
        field: 'id',
        sortable: true,
        // width: 112,
        checkboxSelection: true,
        headerCheckboxSelection: true,
        lockPosition: true,
        filter: true
      }, */
  }
  onQuickFilterChanged() {
    this.gridApi.setQuickFilter(this.textoBuscar);
  }

  nuevo() {
    this.formulario.get('id').enable();
    this.formulario.get('descripcion').enable();
    this.commonFormService.abrirFormularioParaGuardar(this);
  }
  // eliminarId(id) {
  //   return this.mantenerMenu.eliminarId(id);
  // }

  editarLinea({rowData}) {
    if (rowData.id !== '' ) {
      this.formulario.get('id').enable();
    }
    this.textBoton = this.DAT_BTNS.editar;
    this.editableForm = true;
    this.mostrarFormulario = true;
    this.formulario.get('id').setValue(rowData.id);
    this.formulario.get('descripcion').setValue(rowData.descripcion);
    if (rowData.menuSuperior === undefined || rowData.menuSuperior === null) {
          console.log('entro');
          this.formulario.get('menuSuperior').setValue(null);
          this.cmbDisabled = true;
    } else {
          this.formulario.get('menuSuperior').setValue(rowData.menuSuperior.id);
          this.cmbDisabled = false;
    }
    this.formulario.get('nivelId').setValue(rowData.nivel.id);
    if (rowData.sesion === undefined || rowData.sesion === null) {
          console.log('entro');
          this.formulario.get('sesionId').setValue(null);
          this.cmbDisabled1 = true;
    } else {
          this.formulario.get('sesionId').setValue(rowData.sesion.id);
          this.cmbDisabled1 = false;
    }
    console.log(rowData);
   }


 demo() {
  console.log(this.formulario.get('nivelId').value);
  if (this.formulario.get('nivelId').value === 1) {
        this.cmbDisabled = true;
        this.formulario.get('menuSuperior').setValue(null);
        this.cmbDisabled1 = true;
        this.formulario.get('sesionId').setValue(null);
  } else if (this.formulario.get('nivelId').value === 2) {
        this.cmbDisabled = false;
        this.formulario.get('menuSuperior').setValue(null);
        this.cmbDisabled1 = true;
        this.formulario.get('sesionId').setValue(null);
  } else if (this.formulario.get('nivelId').value === 3) {
        this.cmbDisabled = false;
        this.formulario.get('menuSuperior').setValue(null);
        this.cmbDisabled1 = false;
        this.formulario.get('sesionId').setValue(null);
  }
}

  onGridReady(params) {
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;
    this.getAllMenu();
    }
  
    getAllMenu() {
      this.mantenerMenu.getAll().subscribe(res => {
        console.log(res);
        this.rowData = res;
        this.cmbMenuSuperior = res;
        }, error  => {
            console.log(error);
      });
    }//

  guardarCampo() {
    if (this.formulario.get('nivelId').value === 1) {
      return this.mantenerMenu.guardarMenu(this.formulario.value);
    } else {
      if (this.validarCampos(this.formulario.get('nivelId').value,
      this.formulario.get('menuSuperior').value, this.formulario.get('sesionId').value)) {
        return this.mantenerMenu.guardarMenu(this.formulario.value);
      } else {
        this.servicioUtils.mensajeErrorVisto(this.DAT_MSG.completeInformacion);
      }
    }
    console.log(this.formulario.value);
  }

  validarCampos( innNivel, innMenuSuperior, innSesion) {
    let bandera = false;
    if (innNivel === 2 && innMenuSuperior !== null && innSesion === null ) {
       bandera = true;
    }
    if (innNivel === 3 && innMenuSuperior !== null && innSesion !== null) {
       bandera = true;
    }
    console.log(bandera);
    return bandera;
  }
  actualizarCampo() {
    if (this.formulario.get('nivelId').value === 1 &&
        this.formulario.get('menuSuperior').value == null &&
        this.formulario.get('sesionId').value == null) {
        console.log(this.formulario.value);
        console.log(this.formulario.value.descripcion);
        return this.mantenerMenu.actualizarMenu(this.formulario.value);
    } else {
      if (this.validarCampos(this.formulario.get('nivelId').value,
      this.formulario.get('menuSuperior').value, this.formulario.get('sesionId').value)) {
        return this.mantenerMenu.actualizarMenu(this.formulario.value);
        // console.log(this.formulario.value);
      } else {
        this.servicioUtils.mensajeErrorVisto(this.DAT_MSG.completeInformacion);
      }
    }
  }

  acciones(event) {
    if (event === 'guardar') {
      if (this.editableForm) {
        this.servicioUtils.confirmarAcyncronico(this, {title: this.DAT_MSG.actualizar, text: ``, action: this.DAT_BTNS.editar}, () => {
          return this.actualizarCampo();
        });
      } else {
          this.servicioUtils.confirmarAcyncronico(this, {title: this.DAT_MSG.guardar, text: ``, action: this.DAT_BTNS.guardar}, () => {
          return this.guardarCampo();
        });
      }
    } else {
      this.commonFormService.limpiarYregrear(this);
    }
  }
  regresar() {
    this.formulario.reset();
  }//

  eliminarLinea( {rowData}) {
      console.log(JSON.stringify(rowData));
      this.servicioUtils.confirmarSincronoPromesaSinActions(
        {
          title: '¿Esta seguro que desea Eliminar este Registro(' + rowData.descripcion + ')?',
          text: '',
          action: '',
          msn: 'ok'
        }
      ).then((resp: any) => {
        this.mantenerMenu.eliminarId(rowData.id).subscribe( () => {
          this.getAllMenu();
          this.servicioUtils.mensajeOKVisto('ok', false, 1000);
        });
      }).catch((err) => {
      });
  }//

}

