import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MantenerMenuComponent } from './mantener-menu.component';

describe('MantenerMenuComponent', () => {
  let component: MantenerMenuComponent;
  let fixture: ComponentFixture<MantenerMenuComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MantenerMenuComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MantenerMenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
