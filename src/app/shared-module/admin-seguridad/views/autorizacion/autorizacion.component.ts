import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AgGridAngular } from 'ag-grid-angular';
import { moduloSeguridad } from '../../diccionarios/seguridadModulo';
import { buttons, mensajes, buttonsCss } from '../../diccionarios/diccionario';
import { AutorizacionService } from '../../services/autorizacion.service';
import { Authority } from '../../models/authority.model';
import { AplicacionService } from '../../services/aplicacion.service';
import { UtilsService } from 'src/app/shared/utils/utils.service';
import { CommonFormService } from 'src/app/shared/utils/common-form.service';
import { LfsaBotonEditarGridComponent } from 'src/app/shared/components/lfsa-boton-editar-grid/lfsa-boton-editar-grid.component';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';

@Component({
  selector: 'app-autorizacion',
  templateUrl: './autorizacion.component.html',
  styleUrls: ['./autorizacion.component.css']
})
export class AutorizacionComponent implements OnInit {

  formulario: FormGroup;
  mostrarFormulario = false;
  editableForm = false;
  @ViewChild('agGrid') agGrid: AgGridAngular;
  columnDefs: any[];
  rowData: any[];
  frameworkComponents: any;
  rowSelection: any;
  gridApi: any;
  gridColumnApi: any;
  textoBuscar: string;
  DAT_BTNS;
  DAT_BTNCSS;
  DAT_MSG;
  textBoton: string;
  diccionarioModulo: any;
  cmbAplication: any = [];

  constructor(
    private fb: FormBuilder,
    private _util: UtilsService,
    private autorizacionSer: AutorizacionService,
    private aplicationSer: AplicacionService,
    private _commonForm: CommonFormService
  ) {
    this.diccionarioModulo = moduloSeguridad;
    this.getAutorizaciones();
    this.getAplicaciones();
  }

  ngOnInit() {
    this.DAT_BTNS = moduloSeguridad.button.buttonsNombre;
    this.DAT_MSG = moduloSeguridad.mensajes;
    this.DAT_BTNCSS = moduloSeguridad.button.buttonsCss;

    this.formulario = this.fb.group({
      id: [''],
      name: ['', [Validators.required]],
      application: ['', [Validators.required]]
    });

    this.frameworkComponents = {
      buttonRenderer: LfsaBotonEditarGridComponent,
    };

    this.columnDefs = [
      {
        headerName: 'DESCRIPCION',
        field: 'name',
        sortable: true,
        filter: true,
        checkboxSelection: true,
        headerCheckboxSelection: true,
        width: 400
      },
      {
        headerName: 'APLICACIÓN',
        field: 'application',
        sortable: true,
        filter: true,
        width: 400
      },
      {
        headerName: '',
        cellRenderer: 'buttonRenderer',
        width: 150,
        cellRendererParams: {
          onClick: this.editarLinea.bind(this),
          label: this.DAT_BTNS.editar,
          colorBoton: buttonsCss.info
        }
      },
      {
        headerName: '',
        cellRenderer: 'buttonRenderer',
        width: 150,
        cellRendererParams: {
          onClick: this.eliminarLinea.bind(this),
          label: this.DAT_BTNS.eliminar,
          colorBoton: buttonsCss.danger
        }
      }
    ];
    this.rowSelection = 'single';
  }

  onGridReady(params) {
    this.gridApi = params.api;
    // this.getAutorizaciones();
  }

  /**FUNCIONES */
  onQuickFilterChanged() {
    this.gridApi.setQuickFilter(this.textoBuscar);
  }

  nuevo() {
    this.mostrarFormulario = true;
    this.textBoton = buttons.guardar;
    this.editableForm = false;
    this.formulario.reset();
  }

  editarLinea({ rowData }) {
    rowData['id'] = rowData.name;
    this.mostrarFormulario = true;
    this.textBoton = buttons.editar;
    this.editableForm = true;
    this.formulario.setValue(rowData);
  }

  devolverObjeto(data: any): Authority {
    const enviarObjeto = new Authority(data);
    return enviarObjeto;
  }

  acciones(event) {
    if (event === 'guardar') {
      if (this.editableForm) {
        // this._util.confirmarAcyncronico(this, { title: this.DAT_MSG.actualizar, text: ``, action: this.DAT_BTNS.editar }, () => {
        this.actualizarAutorizacionId();
        // });
      } else {
        // this._util.confirmarAcyncronico(this, { title: this.DAT_MSG.guardar, text: ``, action: this.DAT_BTNS.guardar }, () => {
        this.guardarAutorizacion();
        // });
      }
    } else {
      this._commonForm.limpiarYregrear(this);
    }
  }

  /**SERVICIOS */
  eliminarLinea({ rowData }) {
    // this._util.confirmarAcyncronico(this, { title: this.DAT_MSG.eliminar, text: ``, action: this.DAT_BTNS.eliminar }, () => {
    //   return this.eliminarAutorizacionId(rowData.id);
    // }, () => {
    //   this.gridApi.updateRowData({ remove: this.gridApi.getSelectedRows() });
    // });
    console.log(rowData);
    this._util.confirmarSincronoPromesa(
      {
        title: this.DAT_MSG.eliminar,
        text: ``,
        action: this.DAT_BTNS.eliminar,
        funcion: this.autorizacionSer.eliminarAutorizaciones(rowData.name),
        // loading: false,
        // context: this
      }
    ).then((res: any) => {
      this.gridApi.updateRowData({ remove: this.gridApi.getSelectedRows() });
    });
  }

  getAutorizaciones() {
    this.autorizacionSer.getListaAutorizaciones().subscribe((res: any) => {
      // this.rowData = res.respuesta;
      this.rowData = res;
    })
  }

  getAplicaciones() {
    this.aplicationSer.getAplication().subscribe((res: any) => {
      // this.cmbAplication = res.respuesta;
      this.cmbAplication = res;
    })
  }

  guardarAutorizacion() {
    // return this.autorizacionSer.guardarAutorizaciones(this.devolverObjeto(this.formulario.value));
    this._util.confirmarSincronoPromesa(
      {
        title: this.DAT_MSG.guardar,
        text: ``,
        action: this.DAT_BTNS.guardar,
        funcion: this.autorizacionSer.guardarAutorizaciones(this.devolverObjeto(this.formulario.value)),
        // loading: false,
        // context: this
      }
    ).then((res: any) => {
     this.getAutorizaciones();
     this._commonForm.limpiarYregrear(this);
    });
  }

  actualizarAutorizacionId() {
    // return this.autorizacionSer.actualizarAutorizaciones(this.devolverObjeto(this.formulario.value));
    this._util.confirmarSincronoPromesa(
      {
        title: this.DAT_MSG.actualizar,
        text: ``,
        action: this.DAT_BTNS.editar,
        funcion: this.autorizacionSer.actualizarAutorizaciones(this.devolverObjeto(this.formulario.value)),
        // loading: false,
        // context: this
      }
    ).then((res: any) => {
     this.getAutorizaciones();
     this._commonForm.limpiarYregrear(this);
    });
  }

  // eliminarAutorizacionId(id) {
  //   // return this.autorizacionSer.eliminarAutorizaciones(id);
  //   this._util.confirmarSincronoPromesa(
  //     {
  //       title: this.DAT_MSG.eliminar,
  //       text: ``,
  //       action: this.DAT_BTNS.eliminar,
  //       funcion: this.autorizacionSer.eliminarAutorizaciones(id),
  //       // loading: false,
  //       // context: this
  //     }
  //   ).then((res: any) => {
  //     this.gridApi.updateRowData({ remove: this.gridApi.getSelectedRows() });
  //   });
  // }

}
