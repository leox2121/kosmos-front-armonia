import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AgGridAngular } from 'ag-grid-angular';

import { CommonFormService } from '../../../../services/shared/common-form.service';
import { BotonEditarGridComponent } from '../../../../shared/boton-editar-grid/boton-editar-grid.component';
import { UtilsService } from '../../../../services/shared/utils.service';

import { RolUsuarioService } from '../../services/rol-usuario.service.js';
import { RolService } from '../../services/rol.service.js';
import { UsuarioService } from '../../services/usuario.service.js';
import { RolSeguridadModel } from '../../models/rolSeguridad.model.js';
import { UsuarioSeguridadModel } from '../../models/usuarioSeguridad.model.js';

import { moduloSeguridad } from '../../diccionarios/seguridadModulo.js';
import { buttons, mensajes, buttonsCss } from '../../diccionarios/diccionario.js';

@Component({
  selector: 'app-rol-usuario',
  templateUrl: './rol-usuario.component.html',
  styleUrls: ['./rol-usuario.component.css']
})
export class RolUsuarioComponent implements OnInit {

  formulario: FormGroup;
  mostrarFormulario = false;
  editableForm = false;
  @ViewChild('agGrid') agGrid: AgGridAngular;
  columnDefs: any[];
  rowData: any[];
  frameworkComponents: any;
  rowSelection: any;
  gridApi: any;
  gridColumnApi: any;
  textoBuscar: string;
  DAT_BTNS;
  DAT_MSG;
  DAT_BTNCSS:any;
  textBoton: string;
  cmbListRol: RolSeguridadModel[];
  cmbListUsuario: UsuarioSeguridadModel[];
  cmbDisabled: boolean;
  diccionarioModulo: any;
  

  constructor(private fb: FormBuilder,
              private servicioUtils: UtilsService,
              private rolUsuarioService: RolUsuarioService,
              private commonFormService: CommonFormService,
              private rolService: RolService,
              private usuarioService: UsuarioService ) {
                this.diccionarioModulo = moduloSeguridad;
              }

  ngOnInit() {
    // this.DAT_BTNS = buttons;
    // this.DAT_MSG = mensajes;

    this.DAT_BTNS = moduloSeguridad.button.buttonsNombre;
    this.DAT_MSG = moduloSeguridad.mensajes;
    this.DAT_BTNCSS = moduloSeguridad.button.buttonsCss;

    this.rolService.getAll().subscribe((res: any) => {
      this.cmbListRol = res.respuesta;
    });

    this.usuarioService.getAll().subscribe((res: any) => {
      this.cmbListUsuario = res.respuesta;
    });

    this.formulario = this.fb.group({
      usuarioId: ['', [Validators.required]],
      rolId: ['', [Validators.required]]
    });

    this.frameworkComponents = {
      buttonRenderer: BotonEditarGridComponent,
    };

    this.columnDefs = [
      {
        headerName: 'USUARIO',
        field: 'userLogin',
        sortable: true,
        filter: true ,
        checkboxSelection: true,
        headerCheckboxSelection: true
      },
      {
        headerName: 'NOMBRE',
        field: 'userNombre',
        sortable: true,
        filter: true
      },
      {
        headerName: 'ESTADO',
        field: 'userStatus',
        sortable: true,
        filter: true,
        valueFormatter: this.estado
      },
      {
        headerName: 'ROL',
        field: 'rolDescripcion',
        sortable: true,
        filter: true,
        width: 300
      },
      {
        headerName: '',
        cellRenderer: 'buttonRenderer',
        // width: 115,
        cellRendererParams: {
          onClick: this.eliminarLinea.bind(this),
          label: this.DAT_BTNS.eliminar,
          colorBoton: this.DAT_BTNCSS.danger
        }
      }
    ];
    this.rowSelection = 'multiple';
  }

  estado(parems) {
    if ( parems.data.userStatus) {
      return 'Activo';
    } else {
      return 'Inactivo';
    }
  }

  onQuickFilterChanged() {
    this.gridApi.setQuickFilter(this.textoBuscar);
  }

  nuevo() {
    this.commonFormService.abrirFormularioParaGuardar(this);
    this.cmbDisabled = false;
  }

  editarLinea({rowData}) {
    /*,
      {
        headerName: '',
        cellRenderer: 'buttonRenderer',
        cellRendererParams: {
          onClick: this.editarLinea.bind(this),
          label: this.DAT_BTNS.editar,
          colorBoton: this.DAT_BTNCSS.info
        }
      } */
  }

  eliminarLinea({rowData}) {
    this.servicioUtils.confirmarAcyncronico(this, {title: this.DAT_MSG.eliminar, text: ``, action: this.DAT_BTNS.eliminar}, () => {
      return this.rolUsuarioService.eliminarRolUsuarioId(rowData.rolId, rowData.usuarioId);
    }, () => {
        this.gridApi.updateRowData({ remove: rowData });
    });
  }

  onGridReady(params) {
    this.commonFormService.onGridReadyUtil(this, params, this.rolUsuarioService);
  }

  guardarRolUsuario() {
    console.log(this.formulario.value);
    return this.rolUsuarioService.guardarRolUsuario(this.formulario.value);
  }

  actualizarRolUsuarioId(id) {}

  eliminarRolUsuarioId(rolId, usuarioId) {
    return this.rolUsuarioService.eliminarRolUsuarioId(rolId, usuarioId);
  }

  acciones(event) {
    if (event === 'guardar') {
      if (this.editableForm) {
        // ACTUALIZAR
      } else {
        this.servicioUtils.confirmarAcyncronico(this, {title: this.DAT_MSG.guardar, text: ``, action: this.DAT_BTNS.guardar}, () => {
          return this.guardarRolUsuario();
        });
      }
    } else {
      this.commonFormService.limpiarYregrear(this);
    }
  }

  /* printResult(res) {

    if (res.remove) {
      res.remove.forEach((rowNode) => {
        console.log('Removed Row Node', rowNode);
      });
    }
  } */

}
