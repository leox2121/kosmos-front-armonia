import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { moduloSeguridad } from '../../diccionarios/seguridadModulo';
import { AgGridAngular } from 'ag-grid-angular';
import { buttons, mensajes, buttonsCss } from '../../diccionarios/diccionario';
import { AplicacionService } from '../../services/aplicacion.service';
import { UtilsService } from 'src/app/shared/utils/utils.service';
import { CommonFormService } from 'src/app/shared/utils/common-form.service';
import { LfsaBotonEditarGridComponent } from 'src/app/shared/components/lfsa-boton-editar-grid/lfsa-boton-editar-grid.component';

@Component({
  selector: 'app-aplicacion',
  templateUrl: './aplicacion.component.html',
  styleUrls: ['./aplicacion.component.css']
})
export class AplicacionComponent implements OnInit {

  formulario: FormGroup;
  
  mostrarFormulario = false;
  editableForm = false;

  @ViewChild('agGrid') agGrid: AgGridAngular;
  columnDefs: any[];
  rowData: any[];
  frameworkComponents: any;
  rowSelection: any;
  gridApi: any;
  gridColumnApi: any;
  
  textoBuscar: string;
  DAT_BTNS;
  DAT_BTNCSS;
  DAT_MSG;
  
  textBoton: string;
  diccionarioModulo: any;
  // mostrarGridLista = true;

  constructor(private fb: FormBuilder,
    private _util: UtilsService,
    private _aplication: AplicacionService,
    private _commonForm: CommonFormService) {
    this.diccionarioModulo = moduloSeguridad;
    this.getAplicaciones();
  }

  ngOnInit() {
    this.DAT_BTNS = moduloSeguridad.button.buttonsNombre;
    this.DAT_MSG = moduloSeguridad.mensajes;
    this.DAT_BTNCSS = moduloSeguridad.button.buttonsCss;

    this.formulario = this.fb.group({
      application: ['', [Validators.required]]
    });

    this.frameworkComponents = {
      buttonRenderer: LfsaBotonEditarGridComponent,
    };

    this.columnDefs = [
      {
        headerName: 'APLICACIÓN',
        field: 'application',
        sortable: true,
        filter: true,
        // checkboxSelection: true,
        // headerCheckboxSelection: true,
        width: 400
      },
      {
        headerName: '',
        cellRenderer: 'buttonRenderer',
        width: 150,
        cellRendererParams: {
          onClick: this.eliminarLinea.bind(this),
          label: this.DAT_BTNS.eliminar,
          colorBoton: buttonsCss.danger
        }
      }
    ];
    this.rowSelection = 'single';
  }

  onQuickFilterChanged() {
    this.gridApi.setQuickFilter(this.textoBuscar);
  }

  nuevo() {
    //this.mostrarGridLista = false;
    this.mostrarFormulario = true;
    this.textBoton = buttons.guardar;
    this.editableForm = false;
    this.formulario.reset();
  }

  eliminarLinea({ rowData }) {
    this._util.confirmarSincronoPromesa(
      {
        title: this.DAT_MSG.eliminar,
        text: ``,
        action: this.DAT_BTNS.eliminar,
        funcion: this._aplication.eliminarAutorizacion(rowData.application),
      }
    ).then((res: any) => {
      this.gridApi.updateRowData({ remove: this.gridApi.getSelectedRows() });
      console.log(res);
    });
    // this._util.confirmarAcyncronico(this, { title: this.DAT_MSG.eliminar, text: ``, action: this.DAT_BTNS.eliminar }, () => {
    //   return this.eliminarAutorizacionId(rowData.application);
    // }, () => {
    //   this.gridApi.updateRowData({ remove: this.gridApi.getSelectedRows() });
    // });
  }

  onGridReady(params) {
    this.gridApi = params.api;
    this.getAplicaciones();
  }

  getAplicaciones() {
    this._aplication.getAplication().subscribe((res: any) => {
      // this.rowData = res.respuesta;
      this.rowData = res;
    })
  }

  guardarAutorizacion() {
    // return this._aplication.guardarAplication(this.formulario.value);
    this._util.confirmarSincronoPromesa(
      {
        title: this.DAT_MSG.guardar,
        text: ``,
        action: this.DAT_BTNS.guardar,
        funcion: this._aplication.guardarAplication(this.formulario.value),
        // loading: false,
        // context: this
      }
    ).then((res: any) => {
      this.getAplicaciones();
      this._commonForm.limpiarYregrear(this);
      // this.formulario.reset();
      // this.mostrarGridLista = true;
    });
  }

  acciones(event) {
    if (event === 'guardar') {
        // this._util.confirmarAcyncronico(this, { title: this.DAT_MSG.guardar, text: ``, action: this.DAT_BTNS.guardar }, () => {
          this.guardarAutorizacion();
        // });
    } else {
      this._commonForm.limpiarYregrear(this);
    }
  }

}
