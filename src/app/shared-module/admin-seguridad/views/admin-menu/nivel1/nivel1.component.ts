import { Component, OnInit } from '@angular/core';
import { AdminMenuService } from '../../../services/admin-menu.services';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { UtilsService } from '../../../../../services/shared/utils.service';
import { moduloSeguridad } from '../../../diccionarios/seguridadModulo.js';
import { BotonEditarGridComponent } from '../../../../../shared/boton-editar-grid/boton-editar-grid.component';
import { AdminMenu, ModeloAdminMenu, MantenerMenuGuardar } from '../../../models/adminMenu';
import { Router } from '@angular/router';

@Component({
  selector: 'app-nivel1',
  templateUrl: './nivel1.component.html',
  styleUrls: ['./nivel1.component.css']
})
export class Nivel1Component implements OnInit {

  formulario: FormGroup;
  columnDefs: any[];
  rowData;
  frameworkComponents: any;
  rowSelection: any;
  gridApi: any;
  gridColumnApi: any;
  diccionarioModulo: any;
  modelo: ModeloAdminMenu;
  constructor(private servicioWeb: AdminMenuService,
              private fb: FormBuilder,
              private servicioUtils: UtilsService,
              private router: Router) { }

  ngOnInit() {
    this.diccionarioModulo = moduloSeguridad;
    const innclase = new AdminMenu();
    this.modelo = innclase.devolverModeloAdminMenu();
    this.getAllNivel1();
    this.componentGrid();
    this.formulario = this.fb.group({
      menuSuperior: [ '', [Validators.required]]
    });
  }// end Oninit()

  getAllNivel1() {
    this.servicioWeb.getAllNivel(1, 0).subscribe( (resp) => {
        //console.log(JSON.stringify(resp));
        this.rowData = resp;
    });
  }

  componentGrid() {
    this.frameworkComponents = {
      buttonRenderer: BotonEditarGridComponent,
    };

    this.columnDefs = [
      {
        headerName: 'NIVEL',
        field: this.modelo.nivelDescrip,
        sortable: true,
        resizable: true,
        filter: true
      },
      {
        headerName: 'DESCRIPCION',
        field: this.modelo.descripcion,
        sortable: true,
        resizable: true,
        filter: true
      },
      {
        headerName: '',
        cellRenderer: 'buttonRenderer',
        // width: 150,
        cellRendererParams: {
          onClick: this.goNivel2.bind(this),
          label: this.diccionarioModulo.button.buttonsNombre.agregarN2,
          colorBoton: this.diccionarioModulo.button.buttonsCss.primary
        }
      },
      {
        headerName: '',
        cellRenderer: 'buttonRenderer',
        // width: 150,
        cellRendererParams: {
          onClick: this.editarLinea.bind(this),
          label: this.diccionarioModulo.button.buttonsNombre.editar,
          colorBoton: this.diccionarioModulo.button.buttonsCss.success
        }
      },
      {
        headerName: '',
        cellRenderer: 'buttonRenderer',
        cellRendererParams: {
          onClick: this.eliminarLinea.bind(this),
          label: this.diccionarioModulo.button.buttonsNombre.eliminar,
          colorBoton: this.diccionarioModulo.button.buttonsCss.danger
        }
      }
    ];
    this.rowSelection = 'multiple';
  }//end componentGri()

  onGridReady(params) {
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;
    }

  editarLinea({rowData}) {

    this.servicioUtils.confirmarSincronoPromesaDetalle(
      {
        title: '¿Esta seguro que desea modificar el nombre del Menu (' + rowData.descripcion + ')?',
        text: 'Ingrese Nuevo Nombre',
        action: '',
        funcion: this.servicioWeb.devolverObservableOk(rowData.descripcion)
      }
    ).then((nuevaDescripcion: any) => {
      //console.log(JSON.stringify(nuevaDescripcion));
      const llenarObjeto = new MantenerMenuGuardar(rowData.id, nuevaDescripcion);
      //console.log(JSON.stringify(llenarObjeto));
      this.actualizarMenuNivel1(llenarObjeto);
    }).catch((err) => {
    });

  }

  guardarMenuNivel1(innObjeto) {
    this.servicioWeb.guardarMenu(innObjeto).subscribe( (resp) => {
      this.servicioUtils.mensajeOKVisto(resp.descripcion , true, 3000);
      this.formulario.get('menuSuperior').reset();
      this.getAllNivel1();
    }, (error) => {
      this.servicioUtils.mensajeErrorVisto(error.error.mensaje, true, 3000);
    });
  }//

  actualizarMenuNivel1(innObjeto) {
    this.servicioWeb.actualizarMenu(innObjeto).subscribe( (resp) => {
      this.getAllNivel1();
    }, (error) => {
      this.servicioUtils.mensajeErrorVisto(error.error.mensaje, true, 3000);
    });
  }//

  eliminarMenuNivel1(innId) {
    this.servicioWeb.eliminarId(innId).subscribe( (resp) => {
      this.getAllNivel1();
    }, (error) => {
      this.servicioUtils.mensajeErrorVisto(error.error.mensaje, true, 3000);
    });
  }//

  eliminarLinea({rowData}) {
    this.servicioUtils.confirmarSincronoPromesaNormal(
      {
        title: '¿Esta seguro que desea Eliminar un nuevo Menu Nivel 1 (' + rowData.descripcion + ')?',
        text: '',
        action: '',
        funcion: this.servicioWeb.devolverObservableOk(rowData.descripcion)
      }
    ).then((nuevaDescripcion: any) => {
      this.eliminarMenuNivel1(rowData.id);
    }).catch((err) => {
    });

  }
  NuevoMenuNivel1() {
    const descripcion = this.formulario.get('menuSuperior').value;
    this.servicioUtils.confirmarSincronoPromesaNormal(
      {
        title: '¿Esta seguro que desea guardar un nuevo Menu Nivel 1 (' + descripcion + ')?',
        text: '',
        action: '',
        funcion: this.servicioWeb.devolverObservableOk(descripcion)
      }
    ).then((nuevaDescripcion: any) => {
      const llenarObjeto = new MantenerMenuGuardar(null, descripcion);
      this.guardarMenuNivel1(llenarObjeto);
    }).catch((err) => {
    });
  }

  goNivel2({rowData}) {
    localStorage.setItem('descripcionMenuSuperior', rowData.descripcion);
    localStorage.setItem('enviarMenuSuperior', rowData.id);
    this.router.navigate(['admin-seguridad/mantener-menu/2']);
  }//
}//end class
