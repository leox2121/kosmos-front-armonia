import { Component, OnInit } from '@angular/core';
import { AdminMenuService } from '../../../services/admin-menu.services';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { UtilsService } from '../../../../../services/shared/utils.service';
import { moduloSeguridad } from '../../../diccionarios/seguridadModulo.js';
import { BotonEditarGridComponent } from '../../../../../shared/boton-editar-grid/boton-editar-grid.component';
import { AdminMenu, ModeloAdminMenu, MantenerMenuGuardar } from '../../../models/adminMenu';
import { Router } from '@angular/router';
import { SesionService } from '../../../services/sesion.service';
import { SesionSeguridadModel } from '../../../models/sesionSeguridad.model';

@Component({
  selector: 'app-nivel3',
  templateUrl: './nivel3.component.html',
  styleUrls: ['./nivel3.component.css']
})
export class Nivel3Component implements OnInit {

  formulario: FormGroup;
  columnDefs: any[];
  rowData;
  frameworkComponents: any;
  rowSelection: any;
  gridApi: any;
  gridColumnApi: any;
  diccionarioModulo: any;
  modelo: ModeloAdminMenu;
  buscarListaSession: SesionSeguridadModel[];
  nivel3 = 3;
  enviarMenuSuperior = 3;
  descripcionMenuSuperior = '';
  descripcionMenuSuperiorNivel2 = '';
  cmbSesion: any [];
  abrir: any ;
  enviarIdRegistro = 0;
  modoEdicion = false;
  mostrarFormulario = false;
  textBoton = '';
  cmbListAplicacion: any [];
  constructor(private servicioWeb: AdminMenuService,
              private fb: FormBuilder,
              private servicioUtils: UtilsService,
              private sesionServicio: SesionService,
              private router: Router) { }

  ngOnInit() {
    this.enviarMenuSuperior = Number(localStorage.getItem('enviarMenuSuperiorNivel2'));
    this.descripcionMenuSuperior = localStorage.getItem('descripcionMenuSuperior');
    this.descripcionMenuSuperiorNivel2 = localStorage.getItem('descripcionMenuSuperiorNivel2');
    this.diccionarioModulo = moduloSeguridad;
    const innclase = new AdminMenu();
    this.modelo = innclase.devolverModeloAdminMenu();
    this.getListaAplicacion();
    this.getAllNivel3();
    this.componentGrid();
    this.getAllSesiones();
    this.formulario = this.fb.group({
      sesionId: [ '', [Validators.required]],
      descripcion: ['', [Validators.required]],
      application: ['']
    });
  }//end onInit

  getListaAplicacion() {
    this.sesionServicio.getAplicacion().subscribe( (resp) => {
      this.cmbListAplicacion = resp.respuesta;
      //console.log(JSON.stringify(resp));
    });
  }//

  getAllSesiones() {
    this.sesionServicio.getAll().subscribe((res: any) => {
      this.cmbSesion = res.respuesta;
      this.buscarListaSession = res.respuesta;
    });
  }
  getAllNivel3() {
    this.servicioWeb.getAllNivel(this.nivel3, this.enviarMenuSuperior).subscribe( (resp) => {
        this.rowData = resp;
    });
  }
  componentGrid() {
    this.frameworkComponents = {
      buttonRenderer: BotonEditarGridComponent,
    };
    
    this.columnDefs = [
      {
        headerName: 'NIVEL',
        field: this.modelo.nivelDescrip,
        sortable: true,
        resizable: true,
        filter: true
      },
      {
        headerName: 'DESCRIPCION',
        field: this.modelo.descripcion,
        sortable: true,
        resizable: true,
        filter: true
      },
      {
        headerName: 'DESCRIPCION SESSION',
        field: this.modelo.sesionDescrip,
        sortable: true,
        resizable: true,
        filter: true,
        with: 700
      },
      {
        headerName: '',
        cellRenderer: 'buttonRenderer',
        // width: 150,
        cellRendererParams: {
          onClick: this.editarLinea.bind(this),
          label: this.diccionarioModulo.button.buttonsNombre.editar,
          colorBoton: this.diccionarioModulo.button.buttonsCss.success
        }
      },
      {
        headerName: '',
        cellRenderer: 'buttonRenderer',
        cellRendererParams: {
          onClick: this.eliminarLinea.bind(this),
          label: this.diccionarioModulo.button.buttonsNombre.eliminar,
          colorBoton: this.diccionarioModulo.button.buttonsCss.danger
        }
      }
    ];
    this.rowSelection = 'multiple';
  }//end componentGri()

  onGridReady(params) {
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;
  }

  editarLinea({rowData}) {
    this.formulario.get('descripcion').setValue(rowData.descripcion);
    this.formulario.get('sesionId').setValue(rowData.sesionId);
    this.enviarIdRegistro = rowData.id;
    this.mostrarFormulario = true;
    this.modoEdicion = true;
    this.textBoton = this.diccionarioModulo.button.buttonsNombre.editar;
    //this.abrirModal();
  }

  guardarMenuNivel3(innObjeto) {
    this.servicioWeb.guardarMenu(innObjeto).subscribe( (resp) => {
      this.servicioUtils.mensajeOKVisto(resp.descripcion , true, 3000);
      //this.formulario.get('descripcion').reset();
      this.mostrarGrid();
      this.getAllNivel3();
    }, (error) => {
      this.servicioUtils.mensajeErrorVisto(error.error.mensaje, true, 3000);
    });
  }//

  mostrarGrid() {
    this.mostrarFormulario = false;
    this.modoEdicion = false;
  }
  actualizarMenuNivel3(innObjeto) {
    this.servicioWeb.actualizarMenu(innObjeto).subscribe( (resp) => {
      this.mostrarGrid();
      this.getAllNivel3();
    }, (error) => {
      this.servicioUtils.mensajeErrorVisto(error.error.mensaje, true, 3000);
    });
  }//

  eliminarMenuNivel3(innId) {
    this.servicioWeb.eliminarId(innId).subscribe( (resp) => {
      this.getAllNivel3();
    }, (error) => {
      this.servicioUtils.mensajeErrorVisto(error.error.mensaje, true, 3000);
    });
  }//

  eliminarLinea({rowData}) {
    this.servicioUtils.confirmarSincronoPromesaNormal(
      {
        title: '¿Esta seguro que desea Eliminar un nuevo Menu Nivel 3 \n(' + rowData.descripcion + ')?',
        text: '',
        action: '',
        funcion: this.servicioWeb.devolverObservableOk(rowData.descripcion)
      }
    ).then((nuevaDescripcion: any) => {
      this.eliminarMenuNivel3(rowData.id);
    }).catch((err) => {
    });

  }
  NuevoMenuNivel3() {
    const descripcion = this.formulario.get('descripcion').value;
    const sesionId = this.formulario.get('sesionId').value;  //(' + descripcion + ')
    this.servicioUtils.confirmarSincronoPromesaNormal(
      {
        title: '¿Esta seguro que desea guardar un nuevo Menu Nivel 3 \n(' + descripcion + ')?',
        text: '',
        action: '',
        funcion: this.servicioWeb.devolverObservableOk(descripcion)
      }
    ).then((nuevaDescripcion: any) => {
      const llenarObjeto = new MantenerMenuGuardar(null, descripcion, this.nivel3, sesionId, this.enviarMenuSuperior);
      //console.log(JSON.stringify(llenarObjeto));
      this.guardarMenuNivel3(llenarObjeto);
    }).catch((err) => {
    });
  }

  goNivel2() {
    //localStorage.setItem('enviarDescripcionNivel3', rowData.id);
    this.router.navigate(['admin-seguridad/mantener-menu/2']);
  }//
  nuevoMostrarModal() {
    this.formulario.reset();
    this.mostrarFormulario = true;
    this.modoEdicion = false;
    this.textBoton = this.diccionarioModulo.button.buttonsNombre.guardar;
    /*this.formulario.get('descripcion').setValue('');
    this.formulario.get('sesionId').setValue('');*/
  }//

  acciones(event) {
    if (event === 'guardar') {
      if (this.modoEdicion) {
          this.actualizarDatosServicios();
      } else {
        this.NuevoMenuNivel3();
      }
    } else {
      this.mostrarGrid();
    }
  }//

  actualizarDatosServicios() {
    const descripcion  = this.formulario.get('descripcion').value;
    const sesionId  = this.formulario.get('sesionId').value;
    this.servicioUtils.confirmarSincronoPromesaNormal(
      {
        title: '¿Esta seguro que desea modificar el siguiente registro \n(' + descripcion + ')?',
        text: '',
        action: '',
        funcion: this.servicioWeb.devolverObservableOk(descripcion)
      }
    ).then((nuevaDescripcion: any) => {
      //console.log(JSON.stringify(nuevaDescripcion));
      const llenarObjeto = new MantenerMenuGuardar(this.enviarIdRegistro, descripcion, this.nivel3, sesionId, this.enviarMenuSuperior);
      //console.log('Editar Nivel 3-->' + JSON.stringify(llenarObjeto));
      this.actualizarMenuNivel3(llenarObjeto);
    }).catch((err) => {
    });
  }//


  changeAplicacion(target) {
    const posicion = target.options.selectedIndex;
    const nombre = target[posicion].text;
    this.cmbSesion = this.buscarListaSession.filter( (item) => {
              if (item.application === nombre) {
                  return item;
              }
    });
    //this.formulario.get(this.estructuraModeloAgregarVehiculo.cedula).setValue(mostrarCedula[0].cedula);
    //alert('Dato Seleccionado---->' + nombre + 'cedula-->' + mostrarCedula[0].cedula);
  } 



}// end class
