import { Component, OnInit } from '@angular/core';
import { AdminMenuService } from '../../../services/admin-menu.services';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { UtilsService } from '../../../../../services/shared/utils.service';
import { moduloSeguridad } from '../../../diccionarios/seguridadModulo.js';
import { BotonEditarGridComponent } from '../../../../../shared/boton-editar-grid/boton-editar-grid.component';
import { AdminMenu, ModeloAdminMenu, MantenerMenuGuardar } from '../../../models/adminMenu';
import { Router } from '@angular/router';

@Component({
  selector: 'app-nivel2',
  templateUrl: './nivel2.component.html',
  styleUrls: ['./nivel2.component.css']
})
export class Nivel2Component implements OnInit {

  formulario: FormGroup;
  columnDefs: any[];
  rowData;
  frameworkComponents: any;
  rowSelection: any;
  gridApi: any;
  gridColumnApi: any;
  diccionarioModulo: any;
  modelo: ModeloAdminMenu;
  nivel2 = 2;
  enviarMenuSuperior = 2;
  descripcionMenuSuperior = '';
  constructor(private servicioWeb: AdminMenuService,
              private fb: FormBuilder,
              private servicioUtils: UtilsService,
              private router: Router) { }

  ngOnInit() {
    this.enviarMenuSuperior = Number(localStorage.getItem('enviarMenuSuperior'));
    this.descripcionMenuSuperior = localStorage.getItem('descripcionMenuSuperior');
    this.diccionarioModulo = moduloSeguridad;
    const innclase = new AdminMenu();
    this.modelo = innclase.devolverModeloAdminMenu();
    this.getAllNivel2();
    this.componentGrid();
    this.formulario = this.fb.group({
      descripcion: [ '', [Validators.required]]
    });
  }//ends Oninit

  getAllNivel2() {
    this.servicioWeb.getAllNivel(this.nivel2, this.enviarMenuSuperior).subscribe( (resp) => {
        this.rowData = resp;
    });
  }
  componentGrid() {
    this.frameworkComponents = {
      buttonRenderer: BotonEditarGridComponent,
    };

    this.columnDefs = [
      {
        headerName: 'NIVEL',
        field: this.modelo.nivelDescrip,
        sortable: true,
        resizable: true,
        filter: true
      },
      {
        headerName: 'DESCRIPCION',
        field: this.modelo.descripcion,
        sortable: true,
        resizable: true,
        filter: true
      },
      {
        headerName: '',
        cellRenderer: 'buttonRenderer',
        // width: 150,
        cellRendererParams: {
          onClick: this.goNivel3.bind(this),
          label: this.diccionarioModulo.button.buttonsNombre.agregarN3,
          colorBoton: this.diccionarioModulo.button.buttonsCss.primary
        }
      },
      {
        headerName: '',
        cellRenderer: 'buttonRenderer',
        // width: 150,
        cellRendererParams: {
          onClick: this.editarLinea.bind(this),
          label: this.diccionarioModulo.button.buttonsNombre.editar,
          colorBoton: this.diccionarioModulo.button.buttonsCss.success
        }
      },
      {
        headerName: '',
        cellRenderer: 'buttonRenderer',
        cellRendererParams: {
          onClick: this.eliminarLinea.bind(this),
          label: this.diccionarioModulo.button.buttonsNombre.eliminar,
          colorBoton: this.diccionarioModulo.button.buttonsCss.danger
        }
      }
    ];
    this.rowSelection = 'multiple';
  }//end componentGri()

  onGridReady(params) {
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;
    }

    editarLinea({rowData}) {

      this.servicioUtils.confirmarSincronoPromesaDetalle(
        {
          title: '¿Esta seguro que desea modificar el nombre del Menu (' + rowData.descripcion + ')?',
          text: 'Ingrese Nuevo Nombre',
          action: '',
          funcion: this.servicioWeb.devolverObservableOk(rowData.descripcion)
        }
      ).then((nuevaDescripcion: any) => {
        //console.log(JSON.stringify(nuevaDescripcion));
        //const llenarObjeto = new MantenerMenuGuardar(rowData.id, nuevaDescripcion);
        const llenarObjeto = new MantenerMenuGuardar(rowData.id, nuevaDescripcion, this.nivel2, null, this.enviarMenuSuperior);
        //console.log('Editar Nivel 2-->' + JSON.stringify(llenarObjeto));
        this.actualizarMenuNivel2(llenarObjeto);
      }).catch((err) => {
      });
  
    }

    guardarMenuNivel2(innObjeto) {
      this.servicioWeb.guardarMenu(innObjeto).subscribe( (resp) => {
        this.servicioUtils.mensajeOKVisto(resp.descripcion , true, 3000);
        this.formulario.get('descripcion').reset();
        this.getAllNivel2();
      }, (error) => {
        this.servicioUtils.mensajeErrorVisto(error.error.mensaje, true, 3000);
      });
    }//

    actualizarMenuNivel2(innObjeto) {
      this.servicioWeb.actualizarMenu(innObjeto).subscribe( (resp) => {
        this.getAllNivel2();
      }, (error) => {
        this.servicioUtils.mensajeErrorVisto(error.error.mensaje, true, 3000);
      });
    }//

    eliminarMenuNivel1(innId) {
      this.servicioWeb.eliminarId(innId).subscribe( (resp) => {
        this.getAllNivel2();
      }, (error) => {
        this.servicioUtils.mensajeErrorVisto(error.error.mensaje, true, 3000);
      });
    }//

    eliminarLinea({rowData}) {
      this.servicioUtils.confirmarSincronoPromesaNormal(
        {
          title: '¿Esta seguro que desea Eliminar un nuevo Menu Nivel 1 (' + rowData.descripcion + ')?',
          text: '',
          action: '',
          funcion: this.servicioWeb.devolverObservableOk(rowData.descripcion)
        }
      ).then((nuevaDescripcion: any) => {
        this.eliminarMenuNivel1(rowData.id);
      }).catch((err) => {
      });
  
    }
    NuevoMenuNivel2() {
      const descripcion = this.formulario.get('descripcion').value;
      this.servicioUtils.confirmarSincronoPromesaNormal(
        {
          title: '¿Esta seguro que desea guardar un nuevo Menu Nivel 2 (' + descripcion + ')?',
          text: '',
          action: '',
          funcion: this.servicioWeb.devolverObservableOk(descripcion)
        }
      ).then((nuevaDescripcion: any) => {
        const llenarObjeto = new MantenerMenuGuardar(null, descripcion, this.nivel2, null, this.enviarMenuSuperior);
        //console.log(JSON.stringify(llenarObjeto));
        this.guardarMenuNivel2(llenarObjeto);
      }).catch((err) => {
      });
    }
  
    goNivel3({rowData}) {
      localStorage.setItem('descripcionMenuSuperiorNivel2', rowData.descripcion);
      localStorage.setItem('enviarMenuSuperiorNivel2', rowData.id);
      //localStorage.setItem('enviarDescripcionNivel3', rowData.id);
      this.router.navigate(['admin-seguridad/mantener-menu/3']);
    }//

    goNivel1() {
      this.router.navigate(['admin-seguridad/mantener-menu/']);
    }

}//end class
