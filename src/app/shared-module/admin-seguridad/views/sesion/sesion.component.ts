import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AgGridAngular } from 'ag-grid-angular';

import { SesionService } from '../../services/sesion.service';
import { RolService } from '../../services/rol.service';
import { RolSesionService } from '../../services/rol-sesion.service';

import { RolSeguridadModel } from '../../models/rolSeguridad.model';

import { moduloSeguridad } from '../../diccionarios/seguridadModulo';
import { buttons, mensajes, buttonsCss } from '../../diccionarios/diccionario';
import { UtilsService } from 'src/app/shared/utils/utils.service';
import { CommonFormService } from 'src/app/shared/utils/common-form.service';
import { LfsaBotonEditarGridComponent } from 'src/app/shared/components/lfsa-boton-editar-grid/lfsa-boton-editar-grid.component';

@Component({
  selector: 'app-sesion',
  templateUrl: './sesion.component.html',
  styleUrls: ['./sesion.component.css']
})
export class SesionComponent implements OnInit {

  @ViewChild('agGrid') agGrid: AgGridAngular;

  formulario: FormGroup;
  formularioSesion: FormGroup;

  mostrarFormulario = false;
  editableForm = false;
  mostrarSesion = false;

  columnDefs: any[];
  columnDefsRol: any[];

  rowData: any[];
  rowDataRol: any[];

  frameworkComponents: any;
  rowSelection: any;
  gridApi: any;
  gridColumnApi: any;
  textoBuscar: string;
  DAT_BTNS;
  DAT_BTNCSS;
  DAT_MSG;
  textBoton: string;
  cmbListRol: RolSeguridadModel[];
  cmbAplicacion: any[];
  sesionRol: string;
  diccionarioModulo: any;

  constructor(private fb: FormBuilder,
              private _util: UtilsService,
              private sesionService: SesionService,
              private commonFormService: CommonFormService,
              private rolService: RolService,
              private rolSesionService: RolSesionService ) {
                this.diccionarioModulo = moduloSeguridad;
              }

  ngOnInit() {
    this.DAT_BTNS = moduloSeguridad.button.buttonsNombre;
    this.DAT_MSG = moduloSeguridad.mensajes;
    this.DAT_BTNCSS = moduloSeguridad.button.buttonsCss;
    this.getListaAplicacion();
    this.rolService.getAll().subscribe((res: any) => {
      this.cmbListRol = res.respuesta;
    });

    this.formulario = this.fb.group({
      id: [''],
      descripcion: ['', [Validators.required, Validators.maxLength(50)]],
      codSesion: ['', [Validators.required, Validators.maxLength(70)]],
      application: ['', [Validators.required]]
    });
    this.formularioSesion = this.fb.group({
      sesionId: [''],
      rolId: ['', [Validators.required]]
    });

    this.frameworkComponents = {
      buttonRenderer: LfsaBotonEditarGridComponent,
    };

    this.columnDefs = [
      {
        headerName: 'APLICACION',
        field: 'application',
        sortable: true,
        filter: true,
        width: 200
      },
      {
        headerName: 'DESCRIPCION',
        field: 'descripcion',
        sortable: true,
        filter: true,
        width: 300
      },
      {
        headerName: 'COD. SESION',
        field: 'codSesion',
        sortable: true,
        filter: true,
        width: 400
      },
      {
        headerName: '',
        cellRenderer: 'buttonRenderer',
        width: 120,
        cellRendererParams: {
          onClick: this.editarLinea.bind(this),
          label: this.DAT_BTNS.editar,
          colorBoton: buttonsCss.info
        }
      },
      {
        headerName: '',
        cellRenderer: 'buttonRenderer',
        width: 120,
        cellRendererParams: {
          onClick: this.eliminarLinea.bind(this),
          label: this.DAT_BTNS.eliminar,
          colorBoton: buttonsCss.danger
        }
      },
      {
        headerName: '',
        cellRenderer: 'buttonRenderer',
        width: 170,
        cellRendererParams: {
          onClick: this.agregarRolSesion.bind(this),
          label: this.DAT_BTNS.agregarRol,
          colorBoton: buttonsCss.warning
        }
      }
    ];

    this.columnDefsRol = [
      {
        headerName: 'ROL',
        field: 'rolDescripcion',
        sortable: true,
        filter: true,
        checkboxSelection: true,
        width: 300
      },
      {
        headerName: '',
        cellRenderer: 'buttonRenderer',
        //width: 200,
        cellRendererParams: {
          onClick: this.eliminarRol.bind(this),
          label: this.DAT_BTNS.eliminar,
          colorBoton: buttonsCss.danger
        }
      }
    ];

    this.rowSelection = 'multiple';

    this.getAllSesiones();
  } //end Oninit

  onQuickFilterChanged() {
    this.gridApi.setQuickFilter(this.textoBuscar);
  }

  getListaAplicacion() {
    this.sesionService.getAplicacion().subscribe( (resp) => {
      this.cmbAplicacion = resp.respuesta;
      console.log(JSON.stringify(resp));
    });
  }//

  nuevo() {
    this.commonFormService.abrirFormularioParaGuardar(this);
  }

  editarLinea({rowData}) {
    this.commonFormService.abrirFormularioParaEditar(this, rowData);
  }

  eliminarLinea({rowData}) {
    // this._util.confirmarAcyncronico(this, {title: this.DAT_MSG.eliminar, text: ``, action: this.DAT_BTNS.eliminar}, () => {
    //   return this.eliminarSesionId(rowData.id);
    // }, () => {
    //   this.gridApi.updateRowData({ remove: this.gridApi.getSelectedRows() });
    // });
  }

  onGridReady(params) {
    // this.commonFormService.onGridReadyUtil(this, params, this.sesionService);
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;
  }

  getAllSesiones() {
    this.sesionService.getAll().subscribe((res: any)=>{
      this.rowData = res;
    });
  }

  guardarSesion() {
    //this.formulario.get('application').setValue('GLOBAL');
    // return this.sesionService.guardarSesion(this.formulario.value);
    this._util.confirmarSincronoPromesa(
      {
        title: this.DAT_MSG.guardar,
        text: ``,
        action: this.DAT_BTNS.guardar,
        funcion: this.sesionService.guardarSesion(this.formulario.value),
      }
    ).then((res: any) => {
      this.getAllSesiones();
      this.commonFormService.limpiarYregrear(this);
    });
  }

  actualizarSesion() {
    // return this.sesionService.actualizarSesionId(this.formulario.getRawValue());
    this._util.confirmarSincronoPromesa(
      {
        title: this.DAT_MSG.actualizar,
        text: ``,
        action: this.DAT_BTNS.editar,
        funcion: this.sesionService.actualizarSesionId(this.formulario.getRawValue()),
      }
    ).then((res: any) => {
      this.getAllSesiones();
      this.commonFormService.limpiarYregrear(this);
    });
  }

  eliminarSesionId(id) {
    return this.sesionService.eliminarSesionId(id);
  }

  acciones(event) {
    if (event === 'guardar') {
      if (this.editableForm) {
        // this._util.confirmarAcyncronico(this, {title: this.DAT_MSG.actualizar, text: ``, action: this.DAT_BTNS.editar}, () => {
          this.actualizarSesion();
        // });
      } else {
        // this._util.confirmarAcyncronico(this, {title: this.DAT_MSG.guardar, text: ``, action: this.DAT_BTNS.guardar}, () => {
          this.guardarSesion();
        // });
      }
    } else {
      this.commonFormService.limpiarYregrear(this);
    }
  }

  agregarRolSesion({rowData}) {
    console.log(rowData);
    this.mostrarSesion = true;
    this.sesionRol = rowData.id;
    this.formularioSesion.reset();
    this.formularioSesion.controls.sesionId.setValue(rowData.id);
    this.rolSesionService.getSesion(rowData.id).subscribe((resp: any) => {
      this.rowDataRol = resp.respuesta;
    });
  }

  onGridReadyRol(params) {
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;
  }

  eliminarRol({rowData}) {
    // this._util.confirmarAcyncronico(this, {title: this.DAT_MSG.eliminar, text: ``, action: this.DAT_BTNS.eliminar}, () => {
    //   return this.rolSesionService.eliminarRolSesionId(rowData.rolId, rowData.sesionId);
    // }, () => {
    //   this.gridApi.updateRowData({ remove: this.gridApi.getSelectedRows() });
    // });
  }

  accionesRol(event) {
    if (event === 'guardar') {
      // this._util.confirmarAcyncronico(this, {title: this.DAT_MSG.guardar, text: ``, action: this.DAT_BTNS.guardar}, () => {
      //   return this.rolSesionService.guardarRolSesion(this.formularioSesion.getRawValue());
      // }, () => {
      //   this.rolSesionService.getSesion(this.sesionRol).subscribe((resp: any) => {
      //     this.rowDataRol = resp.respuesta;
      //   });
      //   this.formularioSesion.get('rolId').reset();
      // });
    } else {
      this.formularioSesion.reset();
      this.mostrarSesion = false;
    }
  }

}
