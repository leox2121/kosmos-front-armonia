import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
// import { RolComponent } from './views/rol/rol.component';
// import { RolSesionComponent } from './views/rol-sesion/rol-sesion.component';
// import { RolUsuarioComponent } from './views/rol-usuario/rol-usuario.component';
// import { SesionComponent } from './views/sesion/sesion.component';
import { UsuarioComponent } from './views/usuario/usuario.component';
// import { Nivel1Component } from './views/admin-menu/nivel1/nivel1.component';
// import { Nivel2Component } from './views/admin-menu/nivel2/nivel2.component';
// import { Nivel3Component } from './views/admin-menu/nivel3/nivel3.component';
// import { AutorizacionComponent } from './views/autorizacion/autorizacion.component';
// import { AplicacionComponent } from './views/aplicacion/aplicacion.component';
import { AuthGuard } from '../seguridad/services/auth.guard';
import { RolComponent } from './views/rol/rol.component';
import { AutorizacionComponent } from './views/autorizacion/autorizacion.component';
import { AplicacionComponent } from './views/aplicacion/aplicacion.component';
import { SesionComponent } from './views/sesion/sesion.component';


const adminSeguridadRoutes: Routes = [
    // {
    //   path: 'admin-seguridad/mantener-menu',
    //   component: Nivel1Component,
    //   canActivate: [AuthGuard]
    // },
    // {
    //   path: 'admin-seguridad/mantener-menu/2',
    //   component: Nivel2Component,
    //   canActivate: [AuthGuard]
    // },
    // {
    //   path: 'admin-seguridad/mantener-menu/3',
    //   component: Nivel3Component,
    //   canActivate: [AuthGuard]
    // },
    {
      path: 'admin-seguridad/rol',
      component: RolComponent,
      // canActivate: [AuthGuard]
    },
    {
      path: 'admin-seguridad/sesion',
      component: SesionComponent,
      // canActivate: [AuthGuard]
    },
    // {
    //   path: 'admin-seguridad/rol-sesion',
    //   component: RolSesionComponent,
    //   canActivate: [AuthGuard]
    // },
    // {
    //   path: 'admin-seguridad/rol-usuario',
    //   component: RolUsuarioComponent,
    //   canActivate: [AuthGuard]
    // },
    {
      path: 'admin-seguridad/usuario',
      component: UsuarioComponent,
      // canActivate: [AuthGuard]
    },
    {
      path: 'admin-seguridad/autorizacion',
      component: AutorizacionComponent,
      // canActivate: [AuthGuard]
    },
    {
      path: 'admin-seguridad/aplicacion',
      component: AplicacionComponent,
      // canActivate: [AuthGuard]
    }
];

@NgModule({
  imports: [RouterModule.forChild(adminSeguridadRoutes)],
  exports: [RouterModule]
})
export class AdminiSeguridadRoutingModule {}
