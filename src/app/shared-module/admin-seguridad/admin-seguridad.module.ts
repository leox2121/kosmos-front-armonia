import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AgGridModule } from 'ag-grid-angular';
// import { RolComponent } from './views/rol/rol.component';
// import { RolSesionComponent } from './views/rol-sesion/rol-sesion.component';
// import { RolUsuarioComponent } from './views/rol-usuario/rol-usuario.component';
// import { SesionComponent } from './views/sesion/sesion.component';
// import { UsuarioComponent } from './views/usuario/usuario.component';
// import { MantenerMenuComponent } from './views/mantener-menu/mantener-menu.component';
// import { AdminiSeguridadRoutingModule } from './admin-seguridad-routing.module';
// import { SharedModule } from '../../shared/shared.module';
// import { Nivel1Component } from './views/admin-menu/nivel1/nivel1.component';
// import { Nivel2Component } from './views/admin-menu/nivel2/nivel2.component';
// import { Nivel3Component } from './views/admin-menu/nivel3/nivel3.component';
// import { AutorizacionComponent } from './views/autorizacion/autorizacion.component';
// import { AplicacionComponent } from './views/aplicacion/aplicacion.component';
import { LfsaBotonEditarGridComponent } from 'src/app/shared/components/lfsa-boton-editar-grid/lfsa-boton-editar-grid.component';
import { AdminiSeguridadRoutingModule } from './admin-seguridad-routing.module';
import { UsuarioComponent } from './views/usuario/usuario.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { RolComponent } from './views/rol/rol.component';
import { AutorizacionComponent } from './views/autorizacion/autorizacion.component';
import { AplicacionComponent } from './views/aplicacion/aplicacion.component';
import { SesionComponent } from './views/sesion/sesion.component';


@NgModule({
    declarations: [
        RolComponent,
        // RolSesionComponent,
        // RolUsuarioComponent,
        SesionComponent,
        UsuarioComponent,
        // MantenerMenuComponent,
        // Nivel1Component,
        // Nivel2Component,
        // Nivel3Component,
        AutorizacionComponent,
        AplicacionComponent
    ],
    exports: [
        RolComponent,
        AutorizacionComponent,
        // RolSesionComponent,
        // RolUsuarioComponent,
        // SesionComponent,
        // UsuarioComponent,
        // MantenerMenuComponent,
        // Nivel1Component,
        // Nivel2Component,
        // Nivel3Component
    ],
    imports: [
      CommonModule,
      AdminiSeguridadRoutingModule,
      RouterModule,
      FormsModule,
      ReactiveFormsModule,
      SharedModule,
      AgGridModule.withComponents([LfsaBotonEditarGridComponent]),
    ]
  })
export class AdminSeguridadModule { }