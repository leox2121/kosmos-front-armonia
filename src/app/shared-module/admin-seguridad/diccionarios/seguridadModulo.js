const moduloSeguridad = {
    agGrid: {
        paginacion: 15,
        id: 'myGrid',
        // class: 'ag-theme-material mt-3',
        class: 'ag-theme-alpine',
        style: {
            width: '100%',
            height: '400px'
        }
    },
    url: {
        seguridad: {
            usuario: 'usuario',
            sesion: 'sesion',
            rol: 'rol',
            rol_sesion: 'rol-sesion',
            rol_usuario: 'rol-usuario',
            nivel: 'nivel',
            menu: 'menu',
            user_authority: 'user-authority',
            aplication: 'application',
            listaAuthority: 'authority',
            // sessionesByRol: 'rol-sesion/rolId/',
            sessionesByRol: 'rol-sesion/rol/',
            buscarLdap: 'usuario/account/ldap/',
            admin_menu: 'menu/nivel/' //-->nivel(1)/menuSuperior(0)
        }
    },
    button: {
        buttonsCss: {
            primary: 'btn btn-primary btn-block',
            secondary: 'btn btn-secondary btn-block',
            success: 'btn btn-success btn-block',
            danger: 'btn btn-danger btn-block',
            warning: 'btn btn-warning btn-block',
            info: 'btn btn-info btn-block',
            light: 'btn btn-light btn-block',
            dark: 'btn btn-dark btn-block',

            primaryOutline: 'btn btn-outline-primary btn-block',
            secondaryOutline: 'btn btn-outline-secondary btn-block',
            successOutline: 'btn btn-outline-success btn-block',
            dangerOutline: 'btn btn-outline-danger btn-block',
            warningOutline: 'btn btn-outline-warning btn-block',
            infoOutline: 'btn btn-outline-info btn-block',
            lightOutline: 'btn btn-outline-light btn-block',
            darkOutline: 'btn btn-outline-dark btn-block'
        },
        buttonsNombre: {
            editar: 'Editar',
            eliminar: 'Eliminar',
            nuevo: 'Nuevo',
            agregar: 'Agregar',
            agregarN2: 'Agregar Nivel 2',
            agregarN3: 'Agregar Nivel 3',
            agregarRol: 'Agregar Rol',
            agregarEstado: 'Agregar Estado',
            agregarSesion: 'Agregar Sesion',
            agregarUsuario: 'Agregar Usuario',
            agregarMaterial: 'Agg Material',
            agregarPortafolio: 'Agregar Portafolio',
            agregarAutorizacion: 'Agregar Autorización',
            agregarAplicacion: 'Agregar Aplicación',
            agregarCampo: 'Agregar',
            guardar: 'Guardar',
            mostrar: 'Mostrar seleccion',
            regresar: 'Regresar',
            continuar: 'Continuar',
            buscar: 'Buscar',
            procesar: 'Procesar',
        }
    },
    mensajes: {
        eliminar: '¿Esta seguro que desea eliminar el siguiente elemento?',
        guardar: '¿Esta seguro que desea guardar el siguiente elemento?',
        actualizar: '¿Esta seguro que desea actualizar el siguiente elemento?',
        datoGuardado: 'Se guardo corectamente..!!',
        completeInformacion: 'Complete la informacion requerida',
        mensajeAdvertencia: 'La Órden contiene un solo elemento en el detalle desea eliminar la órden completa?',
        guardarDatorForm: 'Desea guardar los datos del formulario',
        mjsProcesar: 'Desea procesar los datos del formulario',
        mjsCampo: 'Al menos debe contener otro Campo "Articulo Alternativo"',
        mjsCampo1: 'Al menos debe contener otro Campo "CodigoBarra"',
        mjsCampo2: 'Al menos debe contener otro Campo',
        mjsCampoAdv: 'Obligatorio Campo Cantidad',
        mjsFilasCorrectas: 'Filas Correctas',
        mjsFilasIncorrectas: 'Filas Incorrectas',
        procesar: '¿Esta seguro que desea procesar el siguiente formulario?'
    }
}
export { moduloSeguridad }