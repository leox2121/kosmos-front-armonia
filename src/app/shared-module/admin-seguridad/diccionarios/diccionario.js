const buttons = {
    editar: 'Editar',
    eliminar: 'Eliminar',
    agregar: 'Agregar elemento',
    agregarRol: 'Agregar Rol',
    agregarEstado: 'Agregar Estado',
    agregarSesion: 'Agregar Sesion',
    agregarUsuario: 'Agregar Usuario',
    agregarMaterial: 'Agg Material',
    agregarPortafolio: 'Agregar Portafolio',
    agregarAutorizacion: 'Agregar Autorizacion',
    agregarAplicacion: 'Agregar Aplicacion',
    agregarCampo: 'Agregar',
    guardar: 'Guardar',
    mostrar: 'Mostrar seleccion',
    regresar: 'Regresar',
    continuar: 'Continuar',
    buscar: 'Buscar',
    procesar: 'Procesar',
}

const estructuraRespuesta = {
    respuesta: 'respuesta'
}
const mensajes = {
    eliminar: '¿Esta seguro que desea eliminar el siguiente elemento?',
    guardar: '¿Esta seguro que desea guardar el siguiente elemento?',
    actualizar: '¿Esta seguro que desea actualizar el siguiente elemento?',
    datoGuardado: 'Se guardo corectamente..!!',
    completeInformacion: 'Complete la informacion requerida',
    mensajeAdvertencia: 'La Órden contiene un solo elemento en el detalle desea eliminar la órden completa?',
    guardarDatorForm: 'Desea guardar los datos del formulario',
    mjsProcesar: 'Desea procesar los datos del formulario',
    mjsCampo: 'Al menos debe contener otro Campo "Articulo Alternativo"',
    mjsCampo1: 'Al menos debe contener otro Campo "CodigoBarra"',
    mjsCampo2: 'Al menos debe contener otro Campo',
    mjsCampoAdv: 'Obligatorio Campo Cantidad',
    mjsFilasCorrectas: 'Filas Correctas',
    mjsFilasIncorrectas: 'Filas Incorrectas',
    procesar: '¿Esta seguro que desea procesar el siguiente formulario?'
}

const url = {
    seguridad: 'http://172.25.4.19:8080/app/v1/api/services/',
    //seguridad: 'http://172.25.4.19:8080/app/ms/datosmaestros/v1/api/services/',
    //seguridadPrueba: 'http://172.25.4.19:8080/app/ms/datosmaestros/v1/api/services/',
    seguridadPrueba: 'http://172.25.4.19:8080/app/v1/api/services/',
    ventas: 'param-auto-servicios/',
    pedidoDetalle: 'pedido-detalle/',
    pedidoCabecera: 'pedido-cabecera/',
    ordeVenta: 'orderVenta/',
    ordenVenta: 'orderVenta/pedidoSelector',
    ordenAnaliza: 'orderVenta/analice',
    pedido: 'paramDefColumByIdPedido',
    clienteOrganizacion: 'organizacionByCliente',
    clienteOrganizacionCanal: 'canalByClienteOrganizacion',
    clienteOrganizacionCanalSector: 'sectorByClienteOrganizacionCanal',
    empresa: 'empresa',
    banco: 'banco/findParam/',
    organizacion: 'organizacion-venta',
    canal: 'canal-venta',
    sector: 'sector-venta',
    oficina: 'oficina-venta',
    cliente: 'clienteAmpliado',
    documento: 'clase-documento',
    paramAutoServicios: 'param-auto-servicios',
    clienteAmpliado: 'clienteAmpliado',
    pedidoId: 'allPedidoByIdPedido?idPedidoCabecera=',
    pedidoDetalleId: 'editSingle?idPedidoDetalle=',
    idOrganizacion: '&idOrganizacion=',
    idCanal: '&idCanal=',
    idSector: '&idSector=',
    idCliente: '?idCliente=',
    idEmpresa: '&idEmpresa=',
    idBanco: '&idBanco=',
    idempresa: '?idEmpresa=',
    file: 'uploadFile',
    pedidoid1: 'orderVenta/allPedidoByIdPedidoParamDetail',
    ped1: '?idPedidoCabecera=',
    ped2: '&paramPedidoDetalle=',
    // param-conciliacionBancaria-servicios param-conciliacionBancaria-servicios
    ConciBanca: 'param-conci-banca',//'param-conciliacionBancaria-servicios',
    DataBanca: '/paramConciBanca',
    obtenerConci: '/paramAutoServiciosPK',
    //detalle venta 
    orderVenta: 'orderVenta/processSAP',
    idPedido: '?idPedido=',//4500554984 | 15-03-2020
    idCliente_: '&idCliente=',//100009
    idEmpresa_: '&idEmpresa=',//1001
    
    //balanza
        // balanza: 'balanza',
        // articulo: 'articulo',
        // linea: 'linea',
        // densidad: 'densidad',
        // relacion: 'relacion',
    //Variable: '',
    bancoCuenta: 'bancoCuenta/findParam/',
    findFormato: '/findFormato',
    DataBancaServ:'conci-banca-servicios',
    uploadDocSAP:'/uploadFileForSAP',
    uploadDoc: '/uploadFileForCuerpo',
    //vendedor
    vendedor: 'vendedor',
    oficina: 'oficina-venta',
    porCabecera: 'portafolio/cabecera',
    vendedorPorta:'vendedorPortafolio',
    organizacion: 'organizacion-venta/grupoOrganizacion',
    zonaVenta: 'zona-ventas',
    //portafolios
    portafolios: 'portafolio',
    portafoliosAmpliado: 'portafolio/ampliado',
    grupoArticulo: 'grupo-articulo/portafolio',
    idPortafolio: '?idPortafolio=',
    //almacen
    almacen: 'almacen',
    //input_search
    clienteFind:'cliente/findParam?cliDesde=',
    cobrosAutoserv:'cobros-autoserv',
    conciBanc:'conci-banc/'
}

const buttonsCss = {
    primary: 'btn btn-primary btn-block',
    secondary: 'btn btn-secondary btn-block',
    success: 'btn btn-success btn-md-block',
    danger: 'btn btn-danger btn-block',
    warning: 'btn btn-warning btn-block',
    info: 'btn btn-info btn-block',
    light: 'btn btn-light btn-block',
    dark: 'btn btn-dark btn-block',

    primaryOutline: 'btn btn-outline-primary btn-block',
    secondaryOutline: 'btn btn-outline-secondary btn-block',
    successOutline: 'btn btn-outline-success btn-block',
    dangerOutline: 'btn btn-outline-danger btn-block',
    warningOutline: 'btn btn-outline-warning btn-block',
    infoOutline: 'btn btn-outline-info btn-block',
    lightOutline: 'btn btn-outline-light btn-block',
    darkOutline: 'btn btn-outline-dark btn-block'
}


export { buttons, mensajes, url, buttonsCss, estructuraRespuesta }