import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { UtilsService } from 'src/app/shared/utils/utils.service';
import { AuthService } from '../../services/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  formulario: FormGroup;
  public isError: boolean = false;
  loading: boolean = false;

  constructor(
    private fb: FormBuilder,
    private router: Router,
    private _auth: AuthService,
    private _utils: UtilsService
  ) { }

  ngOnInit() {
    this.formulario = this.fb.group({
      username: ['', Validators.required],
      password: ['', Validators.required]
    });
    if (this._auth.getCurrentUser() !== null) {
      // console.log('this.authService.getCurrentUser()' + this.authService.getCurrentUser());
      this.router.navigate(['home']);
    }
  }

  onLogin() {
    this.loading = true;
    this._auth.login(this.formulario.value).subscribe((data: any) => { }, (error) => {
      let msj = '';
      if (error.error.key === '406' || error.status === 400 || error.error.key === '400' || error.error.key === '401') {
        msj = error.error.message;
      }
      this._utils.mensajeErrorVisto(msj, true, 2000);
      setTimeout(() => {
        this.formulario.reset();
        this.loading = false;
      }, 2000);
    }
    );
  }

}
