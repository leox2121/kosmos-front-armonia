import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
// import { AgGridModule } from 'ag-grid-angular';
// import { SharedModule } from '../../shared/shared.module';
// import { RecuperacionComponent } from './views/recuperacion/recuperacion.component';
// import { ChangePasswordComponent } from './views/change-password/change-password.component';
// import { LoginComponent } from './views/login/login.component';
import { SeguridadRoutingModule } from './seguridad-routing.module';
import { LoginComponent } from './views/login/login.component';

@NgModule({
    declarations: [
      // LoginComponent,
      // RecuperacionComponent,
      // ChangePasswordComponent
    
    LoginComponent
  ],
    exports: [
      // LoginComponent,
      // RecuperacionComponent,
      // ChangePasswordComponent
    ],
    imports: [
      CommonModule,
      SeguridadRoutingModule,
      RouterModule,
      FormsModule,
      ReactiveFormsModule,
      // SharedModule,
      // AgGridModule.withComponents([]),
    ]
  })
export class SeguridadModule {}