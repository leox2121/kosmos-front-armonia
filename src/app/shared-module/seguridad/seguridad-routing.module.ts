import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './views/login/login.component';
// import { RecuperacionComponent } from './views/recuperacion/recuperacion.component';
// import { ChangePasswordComponent } from './views/change-password/change-password.component';


const adminSeguridadRoutes: Routes = [
  { path: 'login', component: LoginComponent},
  // { path: 'seguridad/recuperacion', component: RecuperacionComponent},
  // { path: 'login/RegistroClienteServlet/:hash/:categoria', component: ChangePasswordComponent},
];

@NgModule({
  imports: [RouterModule.forChild(adminSeguridadRoutes)],
  exports: [RouterModule]
})
export class SeguridadRoutingModule {}
