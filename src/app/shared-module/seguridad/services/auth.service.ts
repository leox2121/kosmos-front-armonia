import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { User } from './user';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { UtilsService } from 'src/app/shared/utils/utils.service';
import { MantenerUsuarioService } from '../../admin-seguridad/services/mantener-menu.service';
// import { MantenerUsuarioService } from '../shared-module/admin-seguridad/services/mantener-menu.service';

@Injectable()
export class AuthService {

  headers: HttpHeaders;

  private loggedIn: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  URL = '';

  get isLoggedIn() {
    return this.loggedIn.asObservable();
  }

  constructor(
    private http: HttpClient,
    private _util: UtilsService,
    private _menu: MantenerUsuarioService
  ) {
    this.URL = this._util.getAuthenticate();
  }

  login(user: User): Observable<any> {
    return this.http.post(this.URL, user, { responseType: 'json', observe: 'response' })
      .pipe(map((data: any) => {
        debugger
        if (data.headers.get('Authorization') !== '' && data.headers.get('Authorization') !== null) {
          this.loggedIn.next(true);
          this.setToken(data.headers.get('Authorization'));
          localStorage.setItem('datosUsuarioLogin', JSON.stringify(data.body.respuesta));
          localStorage.setItem('mostrarMenu', 'true');
          localStorage.setItem('userName', user.username);
          let date = new Date();
          localStorage.setItem('timeLogin', date.toString());
          this._menu.getMenu().subscribe((result: any) => {
            localStorage.setItem('menu', JSON.stringify(result));
            window.location.href = window.location.origin;
          });
        }
      }));
  }

  setToken(token: string): void {
    localStorage.setItem('accessToken', token);
  }

  getToken() {
    return localStorage.getItem('accessToken');
  }

  getCurrentUser(url?: string): any {
    const accessToken = this.getToken();
    if (!this.isNullOrUndefined(accessToken)) {
      this.loggedIn.next(true);
      return url;
    } else {
      this.loggedIn.next(false);
      return null;
    }
  }

  logout() {
    this.loggedIn.next(false);
    localStorage.clear();
    window.location.href = window.location.origin;
  }

  isNullOrUndefined(value: any) {
    return value === null || value === undefined;
  }

}
