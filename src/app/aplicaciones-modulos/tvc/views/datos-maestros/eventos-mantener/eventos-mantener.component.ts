import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { LfsaBotonEditarGridComponent } from 'src/app/shared/components/lfsa-boton-editar-grid/lfsa-boton-editar-grid.component';
import { CommonFormService } from 'src/app/shared/utils/common-form.service';
import { UtilsService } from 'src/app/shared/utils/utils.service';
import { CatalogosTvcService } from '../../../catalogos/catalogosTvc.service';
// import { BotonEditarGridComponent } from 'src/app/shared/boton-editar-grid/boton-editar-grid.component';
import { moduloTvc } from '../../../diccionarios/tvcModulo';
import { EventoModel } from '../../../models/eventoModel';
import { EventosService } from '../../../services/eventos.service';

@Component({
  selector: 'app-eventos-mantener',
  templateUrl: './eventos-mantener.component.html',
  styleUrls: ['./eventos-mantener.component.css']
})
export class EventosMantenerComponent implements OnInit {

  formulario: FormGroup;
  columnDefs: any[];
  rowData: any[] = [];
  frameworkComponents: any;
  gridApi: any;
  gridColumnApi: any;
  rowSelection: any;

  textBoton: string;
  textoBuscar: string;
  mostrarFormulario = false;
  tvcModulo: any;
  editableForm = false;
  cmbListaPlanta: any[] = [];

  loading: boolean = false;
  listaVacia: boolean = true;

  cmbClasificacion: any[] = [];
  cmbTipoClasificacion: any[] = [];
  cmbTasa: any[] = [];
  constructor(
    private fb: FormBuilder,
    private _util: UtilsService,
    private _eventos: EventosService,
    private _catalogo: CatalogosTvcService,
    private commonFormService: CommonFormService
  ) {
  }

  ngOnInit() {
    this.tvcModulo = moduloTvc;
    this.textBoton = this.tvcModulo.button.buttonsNombre.guardar;
    this.formulario = this.fb.group({
      idEvento: [{ value: '', disabled: true }, [Validators.required, Validators.min(1), Validators.maxLength(5)]],
      dsca: ['', [Validators.required, Validators.maxLength(100)]],
      clasificacion: ['', [Validators.required, Validators.maxLength(25)]],
      tipoClasificacion: ['', [Validators.required, Validators.maxLength(25)]],
      tasaGravedadFallo: ['', [Validators.required]]
    });
    this.cargarGrig();
    this.getAll();
    this.cmbClasificacion = this._catalogo.catalgoEventoClasificacion();
    this.cmbTipoClasificacion = this._catalogo.catalgoTipoClasificacion();
    this.cmbTasa = this._catalogo.getCmbTasa();
  }

  getAll() {
    this.loading = true;
    this.listaVacia = true;
    this._eventos.getAll().subscribe((res: any) => {
      this.loading = false;
      this.rowData = res;
      if (this.rowData.length > 0) {
        this.listaVacia = false;
      } else {
        this.listaVacia = true;
      }
    }, (err) => {
      this.listaVacia = true;
      this.loading = false;
      console.log(err);
    });
  }

  cargarGrig() {
    this.columnDefs = [
      {
        headerName: 'CODIGO',
        field: 'idEvento',
        sortable: true,
        filter: true,
        width: 140
      },
      {
        headerName: 'DESCRIPCION',
        field: 'dsca',
        sortable: true,
        filter: true,
        width: 320
      },
      {
        headerName: 'CLASIFICACION',
        field: 'clasificacion',
        sortable: true,
        filter: true,
        width: 200
      },
      {
        headerName: 'TIPO',
        field: 'tipoClasificacion',
        sortable: true,
        filter: true,
        width: 200
      },
      {
        headerName: 'TGF',
        field: 'tasaGravedadFallo',
        sortable: true,
        filter: true,
        width: 150,
        valueGetter: function (p) {
          return p.data.tasaGravedadFallo ? 'SI' : 'NO'
        }
      },
      {
        headerName: '',
        cellRenderer: 'buttonRenderer',
        width: 100,
        cellRendererParams: {
          onClick: this.editarLineaGrid.bind(this),
          label: this.tvcModulo.button.buttonsNombre.editar,
          colorBoton: this.tvcModulo.button.buttonsCss.info
        }
      },
      {
        headerName: '',
        cellRenderer: 'buttonRenderer',
        width: 100,
        cellRendererParams: {
          onClick: this.eliminarLineaGrid.bind(this),
          label: this.tvcModulo.button.buttonsNombre.eliminar,
          colorBoton: this.tvcModulo.button.buttonsCss.danger
        }
      }
    ];

    this.frameworkComponents = {
      buttonRenderer: LfsaBotonEditarGridComponent,
    };

    this.rowSelection = 'multiple';
  }

  onQuickFilterChanged() {
    this.gridApi.setQuickFilter(this.textoBuscar);
  }

  onGridReady(params) {
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;
  }

  nuevo() {
    this.formulario.get('idEvento').enable();
    this.commonFormService.abrirFormularioParaGuardar(this);
  }

  editarLineaGrid({ rowData }) {
    delete rowData['idevento'];
    this.formulario.get('idEvento').disable();
    this.commonFormService.abrirFormularioParaEditar(this, rowData);
  }

  eliminarLineaGrid({ rowData }) {
    this._util.confirmarSincronoPromesa({
      title: this.tvcModulo.mensajes.eliminar, 
      text: `${rowData.dsca}`, 
      action: this.tvcModulo.button.buttonsNombre.eliminar,
      funcion: this.eliminar(rowData.idEvento),
    }).then((res: any) => {
      this.rowData = this.rowData.filter(item => item.idEvento !== rowData.idEvento);
    });
  }

  acciones(event) {
    if (event === 'guardar') {
      if (this.editableForm) {
        this.actualizar();
        
      } else {
        this.guardar();
      }
    } else {
      this.commonFormService.limpiarYregrear(this);
    }
  }

  guardar() {
    const obj = new EventoModel();
    obj.idEvento = this.formulario.get('idEvento').value;
    obj.dsca = this.formulario.get('dsca').value;
    obj.clasificacion = this.formulario.get('clasificacion').value;
    obj.tipoClasificacion = this.formulario.get('tipoClasificacion').value;
    obj.tasaGravedadFallo = this.formulario.get('tasaGravedadFallo').value;
    this._util.confirmarSincronoPromesa({
      title: this.tvcModulo.mensajes.guardar, 
      text: `${this.formulario.get('idEvento').value}`, 
      action: this.tvcModulo.button.buttonsNombre.guardar,
      funcion: this._eventos.save(obj),
    }).then((res: any) => {
      this.getAll();
      this.commonFormService.limpiarYregrear(this);
    });
    // return this._eventos.save(obj);
  }

  eliminar(innId) {
    return this._eventos.delete(innId);
  }

  actualizar() {
    const obj = new EventoModel();
    obj.idEvento = this.formulario.get('idEvento').value;
    obj.dsca = this.formulario.get('dsca').value;
    obj.clasificacion = this.formulario.get('clasificacion').value;
    obj.tipoClasificacion = this.formulario.get('tipoClasificacion').value;
    obj.tasaGravedadFallo = this.formulario.get('tasaGravedadFallo').value;
    this._util.confirmarSincronoPromesa({
      title: this.tvcModulo.mensajes.actualizar, 
      text: `${this.formulario.get('idEvento').value}`, 
      action: this.tvcModulo.button.buttonsNombre.actualizar,
      funcion: this._eventos.edit(obj),
    }).then((res: any) => {
      this.getAll();
      this.commonFormService.limpiarYregrear(this);
    });
    // return this._eventos.edit(obj);
  }

}
