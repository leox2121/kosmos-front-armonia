import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EventosMantenerComponent } from './eventos-mantener.component';

describe('EventosMantenerComponent', () => {
  let component: EventosMantenerComponent;
  let fixture: ComponentFixture<EventosMantenerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EventosMantenerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EventosMantenerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
