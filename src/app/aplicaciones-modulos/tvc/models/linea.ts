export class Linea {
    id: number;
    codigoLinea: number;
    descripcion: string;
    nombre: string;
    idPlanta: number;
    descripcionElastic: string;
    planta: {};
    idEmpresa?: number
    constructor() { }
}