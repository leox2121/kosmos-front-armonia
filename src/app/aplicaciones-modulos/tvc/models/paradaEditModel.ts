import { Linea } from "./linea";
import { Producto } from "./producto";

export class ParadaEditModel {

    codigoEvento: number;
    codProducto: string;
    descripcionEvento: string;
    diferenciaHora: number;
    fechaRegistro: string;
    fechaActualizacion: string;
    finContador: number;
    finEvento: string;
    id: number;
    idLinea: number;
    idProducto: number;
    inicioContador: number;
    inicioEvento: string;
    noLote: number;
    ordenProduccion: string;
    rechazoFin: number;
    rechazoInicio: number;
    turno: number;
    velocidadEstandar: number;
    difSegundoEvento?: number;

    constructor(){}
}