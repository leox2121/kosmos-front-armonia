export class Parada {
    codigoLinea: number;
    codigoProducto: string;
    noLote: number;
    turno: number;
    status: string;
    maquina: string;
    fecha: string;
    inicioEvento: string;
    clasificacion: string;
    inicioContador: number;
    codigoEvento: number;
    descripcionEvento: string;
    finalContador: number;
    finEvento: string;
    origen: string;
    codigoEventoAnterior: number;
    respuesta: string;
    rechazoInicio:number;
    rechazoFin:number;
    velocidadEstandar:number;
    ordenProduccion:string;
    usuarioLog:string;
    constructor() { }
}

/*
{
    id: number;
    "codigoLinea":600,
    "codigoProducto": "800",
    "noLote": 85529,
    "turno": 2,
    "status": "pendiente",
    "maquina": "",
    "fecha": "2019-11-17T15:15:00.00Z",
    "inicioEvento": "2019-11-17T18:50:00.00Z",
    "clasificacion": "PROGRAMADA",
    "inicioContador": 100,
    "codigoEvento": 1500,
    "descripcionEvento" : "",
    "finalContador": 100,
    "finEvento": "2019-11-17T20:15:00.00Z",
    "origen": "PLC"
}
*/