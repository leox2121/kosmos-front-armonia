export class Producto {
    
    id?: number;
    codigoProducto: string;
    descripcion: string;
    numeroLote = '';
    nombre: number;
    linea: number;

    velocidadEstandar: number;
    unidadCaja: number;
    unidadMolde: number;
    
    constructor() { }
}