export class ProdProducto {

    idProducto: number;//341572,
    nombreProducto: string;//"CJ*27.5KG ESPECIAL C",
    unidadesProducidas: number;//0.0,
    unidadesRechazadas: number;//0.0
    unidadesLaboratorio: number;
    
    constructor() { }
}