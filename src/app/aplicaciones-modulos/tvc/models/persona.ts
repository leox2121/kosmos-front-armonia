export class Persona {
    id: number;
    nombre: string;
    apellido: string;
    direccion: string;
    mail: string;
    telefono: string;
    tipo: string;
    idUsuario: string;
    constructor() { }
}
