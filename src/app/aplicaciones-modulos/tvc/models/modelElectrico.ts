export class ModelElectrico {
    clasificacionEvento: string;
    codigoEvento: number;
    codigoLinea: number;
    idLinea: number;
    idProducto: number;
    incioEvento: string;
    nombreEvento: string;
    nombreLinea: string;
    nombreProducto: string;
    tgf: boolean;
    turno: number;
    unidadesProducidas: number;
    unidadesRechazadas: number;
    constructor() { }
}