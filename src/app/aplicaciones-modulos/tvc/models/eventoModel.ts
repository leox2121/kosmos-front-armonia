export class EventoModel {

    idEvento: number;
    dsca: string;
    clasificacion: string;
    tipoClasificacion: string;
    tasaGravedadFallo: boolean;
    constructor() { }
}