import { GrupoSeccionesModel } from "./grupoSeccionesModel";

export class SeccionesModel {

    codigo: number;
    descripcion: string;
    grupo?: GrupoSeccionesModel;
    codigoGrupo?: number;

    constructor(){}
}