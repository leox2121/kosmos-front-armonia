export class ReporteParada {
    anio: number;
    clasificacion: string;
    codigoEvento: number;
    codigoLinea: number;
    codigoProducto: string;
    descripcionEvento: string;
    descripcionLinea: string;
    dia: number;
    diferenciaFechaMinutos: number;
    diferenciaFechaSegundos: number;
    diferenciaFechaTotal: string;
    fecha: string; //": "2019-09-18 10:21:52.42",
    finContador: number;
    finEvento: string; //": "2019-09-18 10:22:00.451",
    inicioEvento: string; //": "2019-09-18 10:22:00.451",
    horaFin: string;
    horaInicio: string;
    idParada: number;
    inicioContador: number;
    // tslint:disable-next-line: variable-name
    linea_id: number;
    mes: number;
    nolote: number;
    nombreProducto: string;
    numeroSemana: string;
    // tslint:disable-next-line: variable-name
    producto_id: number;
    status: string;
    turno: number;
    unidadesProducidas: number;
    velocidadEstandar: number;
    //
    rechazoInicio: number; 
    rechazoFin: number;
    unidadesRechazadas: number;
    tasaGF: boolean;

    constructor() { }

    devolverModeloGridReporteParada() {
        const objeto: ModeloGridReporteParada = {
            anio: 'anio',
            clasificacion: 'clasificacion',
            codigoEvento: 'codigoEvento',
            codigoLinea: 'codigoLinea',
            codigoProducto: 'codigoProducto',
            descripcionEvento: 'descripcionEvento',
            descripcionLinea: 'descripcionLinea',
            dia: 'dia',
            diferenciaFechaMinutos: 'diferenciaFechaMinutos',
            diferenciaFechaSegundos: 'diferenciaFechaSegundos',
            diferenciaFechaTotal: 'diferenciaFechaTotal',
            fecha: 'fecha',
            finContador: 'finContador',
            finEvento: 'finEvento',
            horaFin: 'horaFin',
            horaInicio: 'horaInicio',
            idParada: 'idParada',
            inicioContador: 'inicioContador',
            linea_id: 'linea_id',
            mes: 'mes',
            nolote: 'nolote',
            nombreProducto: 'nombreProducto',
            numeroSemana: 'numeroSemana',
            producto_id: 'producto_id',
            status: 'status',
            turno: 'turno',
            unidadesProducidas: 'unidadesProducidas',
            inicioEvento: 'inicioEvento',
            velocidadEstandar: 'velocidadEstandar',
            //
            rechazoInicio: 'rechazoInicio', 
            rechazoFin: 'rechazoFin',
            unidadesRechazadas: 'unidadesRechazadas',
            tasaGF: 'tasaGF'
        };
        return objeto;
    }
}//

export interface ModeloGridReporteParada {
    anio: string;
    clasificacion: string;
    codigoEvento: string;
    codigoLinea: string;
    codigoProducto: string;
    descripcionEvento: string;
    descripcionLinea: string;
    dia: string;
    diferenciaFechaMinutos: string;
    diferenciaFechaSegundos: string;
    diferenciaFechaTotal: string;
    fecha: string; //": "2019-09-18 10:21:52.42",
    finContador: string;
    finEvento: string; //": "2019-09-18 10:22:00.451",
    horaFin: string;
    horaInicio: string;
    idParada: string;
    inicioContador: string;
    // tslint:disable-next-line: variable-name
    linea_id: string;
    mes: string;
    nolote: string;
    nombreProducto: string;
    numeroSemana: string;
    // tslint:disable-next-line: variable-name
    producto_id: string;
    status: string;
    turno: string;
    unidadesProducidas: string; 
    inicioEvento: string;
    velocidadEstandar: string;
    //
    rechazoInicio: string;
    rechazoFin: string;
    unidadesRechazadas: string;
    tasaGF:string;
}

export interface EstructuraListaTablero {
    descripcionFila: string;
    mes1: number;
    mes2: number;
    mes3: number;
    semana1: number;
    semana2: number;
    semana3: number;
    semana4: number;
    banderaPorcentaje: boolean;
    banderaResaltar?: boolean;
  }