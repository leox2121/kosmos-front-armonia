import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../../shared/shared.module';
import { TvcRoutingModule } from './tvc-routing.module';
import { AgGridModule } from 'ag-grid-angular';
import { EventosMantenerComponent } from './views/datos-maestros/eventos-mantener/eventos-mantener.component';
import { LfsaBotonEditarGridComponent } from 'src/app/shared/components/lfsa-boton-editar-grid/lfsa-boton-editar-grid.component';
// import { BotonEditarGridComponent } from '../../shared/boton-editar-grid/boton-editar-grid.component';

@NgModule({
    declarations: [
     EventosMantenerComponent
    ],
    exports: [
       EventosMantenerComponent
    ],
    imports: [
      CommonModule,
      TvcRoutingModule,
      RouterModule,
      FormsModule,
      ReactiveFormsModule,
      SharedModule,
      AgGridModule.withComponents([LfsaBotonEditarGridComponent]),
    ]
  })
export class TvcModule { }