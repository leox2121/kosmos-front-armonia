import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CatalogosTvcService {

  path: string = '../../../assets/image/vdi/IMG/';
  //lafabril-logo-medium.svg',
  constructor() { }

  catalogoTodos() {
    return [
      { clave: 0, valor: 'Todos', color: 'white', img: this.path + "Button-Synchronize-white-24.png" },
      { clave: 1, valor: 'Orden', color: '#98FB98', img: this.path + "Button-Synchronize-green-24.png" },
      { clave: 2, valor: 'Enviado', color: '#99FFFF', img: this.path + "Button-Synchronize-blue-24.png" },
      { clave: 3, valor: 'Por enviar', color: '#FFC0CB', img: this.path + "Button-Synchronize-red-24.png" }
    ];
  }

  catalogoPedidoDoc() {
    return [
      { clave: 0, valor: 'Todos', color: 'white', img: this.path + "Button-Synchronize-white-24.png" },
      { clave: 1, valor: 'Autorizado', color: '#98FB98', img: this.path + "Button-Synchronize-green-24.png" },
      { clave: 2, valor: 'No Autorizado', color: '#FFC0CB', img: this.path + "Button-Synchronize-red-24.png" },
      { clave: 3, valor: 'Espera', color: 'white', img: this.path + "Button-Synchronize-blue-24.png" },
    ];
  }

  catalogoRecaudo() {
    return [
      { clave: 0, valor: 'Todos', color: 'white', img: this.path + "Button-Synchronize-white-24.png" },
      { clave: 1, valor: 'Asiento Fin', color: '#FFFF99', img: this.path + "Button-Synchronize-green-24.png" },
      { clave: 2, valor: 'Asiento', color: '#99FF99', img: this.path + "Button-Synchronize-red-24.png" },
      { clave: 3, valor: 'Enviado', color: '	#99FFFF', img: this.path + "Button-Synchronize-blue-24.png" },
      { clave: 4, valor: 'Por enviar', color: '#FFC0CB', img: this.path + "Button-Synchronize-blue-24.png" },
    ];
  }

  catalogoTrasnportista() {
    return [
      { clave: '', valor: 'Todos' },
      { clave: false, valor: 'Vendedor' },
      { clave: true, valor: 'Transportistas' },
    ];
  }

  imagenIcon() {
    return {
      white: this.path + 'Button-Synchronize-white-24.png',
      green: this.path + 'Button-Synchronize-green-24.png',
      blue: this.path + 'Button-Synchronize-blue-24.png',
      red: this.path + 'Button-Synchronize-red-24.png',
      yellow: this.path + 'Button-Synchronize-yellow-32.png',
      user: this.path + 'user.png',
      filePathSyncro: this.path + "download.mobile.png",
      filePathNoSyncro: this.path + "database.png",
      fileConfirmSyncro: this.path + "upload.mobile.png",
      smartphoneDownRed: this.path + 'smartphone-down-red.png',
      smartphoneDownGreen: this.path + 'smartphone-down-green.png',
      trasInv: this.path + 'trasInv.png',
      warning: this.path + 'Warning.png'
    };
  }

  getAllEpresas() {
    return [
      { clave: 1, valor: 'Todos' },
      { clave: 2, valor: 'La Fabril' }
    ];
  }

  catalogoAsesorIcon() {
    return {
      vendedor: this.path + 'aser.jpg',
      vendeorApoyo: this.path + 'user.png',
      cliente: this.path + 'client.png',
      fechaCalendario: this.path + 'calendario-semanal.png',
      refineria: this.path + 'iconRefineria.png',
      planta: this.path + 'iconPlanta.png',
      calendario: this.path + 'calendario.png',
      paradaTvc: this.path + 'paradaTvc.png',
      csv: this.path + 'csv.svg',
    };
  }

  catalgoEventoClasificacion() {
    return [
      { clave: 'Paro Programado', valor: 'Paro Programado' },
      { clave: 'Paro No Programado', valor: 'Paro No Programado' },
      { clave: 'Produccion', valor: 'Produccion' },
      { clave: 'SIN ASIGNACIÓN', valor: 'SIN ASIGNACIÓN' },
      { clave: 'Flta. Ord. Produccion', valor: 'Flta. Ord. Produccion' },
      { clave: 'Feriado y Fin de Semana', valor: 'Feriado y Fin de Semana' }
    ];
  }

  catalgoTipoClasificacion() {
    return [
      { clave: 'Paro Programado', valor: 'Paro Programado' },
      { clave: 'Paro No Programado', valor: 'Paro No Programado' },
      { clave: 'Produccion', valor: 'Produccion' },
      { clave: 'SIN ASIGNACIÓN', valor: 'SIN ASIGNACIÓN' },
      { clave: 'Flta. Ord. Produccion', valor: 'Flta. Ord. Produccion' },
      { clave: 'Feriado y Fin de Semana', valor: 'Feriado y Fin de Semana' }
    ];
  }

  getCmbTasa() {
    return [
      {clave: true, valor: 'SI'},
      {clave: false, valor: 'NO'}
    ];
  }

}
