const moduloTvc = {
    url: {
        datosMaestros: {
            planta: 'planta',
            linea: 'linea',
            persona: 'persona',
            lineaPersona: 'linea-persona',
            producto: 'producto',
            plantaPersona: 'planta-persona',
            eventos: 'evento',
            plantaLineaPersona: 'linea/planta-persona',
            empresa: 'empresa'
        },
        parada: {
            getLineasPersonas: 'getLineasPersonas',
            getProductosPorLineas: 'getProductos-por-lineas',
            parada: 'parada',
            paradaWeb: 'parada/web',
            lineaInfo: 'last-parada',
            eventos: 'eventos',
            unidadesProducidas: 'unidades-producidas',
            unidadesProducidasLineas: 'unidades-producidas-linea',
            reporteParada: 'report',
            reporteIndicadoresTVC: 'indicador-tvc-linea',
            reporteIndicadorTVCTablero: 'indicador-tvc-linea/linea',
            indicadorTvcLineaParam:'indicador-tvc-linea/param',
            actualizarDescripcion: 'actualizar-descripcion',
            electricista: 'reporte/electrisista',
            elementoLaboratorio: 'reporte/parada/elemento',
            laboratorio: 'laboratorio',
            rechazoRetiro: 'rechazo-retiro',
            reporteProdpro: 'reporte/produccion/producto/',
            reportMotivo: 'report/motivo/',
            eventoContador: 'evento/contador/',
            eventoContadorSeccion: 'evento/contador-seccion',
            eventoContadorTablero: 'evento/contador-tablero-control',
            reporteProdproTablero: 'reporte/produccion/producto-tablero-control',
            reportMotivoTablero: 'report/motivo-tablero-control'
        },
        coordinador: {
            indice: 'indice',
            indiceLinea: 'indice-linea',
            param: 'param'
        },
        contingencia: {
            paradaDefecto: 'contingencia/parada-defecto/',
            secuenciaParada: 'contingencia/ajustar-secuencia-parada',
            statusSecuencial: 'procesos/ajuste-parada'
        },
        kibana:{
            getTipoDasboard: 'dashboard-kibana/allTipo',
            saveTipoDasboard:'dashboard-kibana/insertTipo',
            deleteTipoDasboard:'dashboard-kibana/eliminarDashTipo',
            getAllDasboard: 'dashboard-kibana/all',
            getAllDasboardTVC: 'dashboard-kibana/dashboard/tvc',
            saveDashboard: 'dashboard-kibana/insert',
            deleteDashboard: 'dashboard-kibana/eliminarDash',
            dashboardPlanta: 'dashboard-kibana/planta',
            linea: 'linea/lineasByIdPlanta',
            //dasboardLineas
            saveDashLineas: 'linea-dashboard-kibana/insert',
            lineasIdDashboard: 'linea-dashboard-kibana/lineas/dashboard',
            deleteDashLineas: 'linea-dashboard-kibana/delete',
            dashboardLinea: 'linea-dashboard-kibana/linea'
        },
        secciones: {
            secciones: 'seccion',
            grupoSecciones: 'grupo-seccion'
        },
        tablero:{
            tableroControl: 'reporte/tablero-control'
        },
        insumo: {
            bitacora: 'insumo/bitacora/',
        },
        pareto: 'pareto',
        seguridadPrueba: ''
    },
    button: {
        buttonsCss: {
            primary: 'btn btn-primary btn-block',
            secondary: 'btn btn-secondary btn-block',
            success: 'btn btn-success btn-block',
            danger: 'btn btn-danger btn-block',
            warning: 'btn btn-warning btn-block',
            info: 'btn btn-info btn-block',
            light: 'btn btn-light btn-block',
            dark: 'btn btn-dark btn-block',
            primaryOutline: 'btn btn-outline-primary',
            secondaryOutline: 'btn btn-outline-secondary',
            successOutline: 'btn btn-outline-success',
            dangerOutline: 'btn btn-outline-danger',
            warningOutline: 'btn btn-outline-warning',
            infoOutline: 'btn btn-outline-info',
            lightOutline: 'btn btn-outline-light',
            darkOutline: 'btn btn-outline-dark'
        },
        buttonsNombre: {
            editar: 'Editar',
            eliminar: 'Eliminar',
            agregar: 'Agregar ',
            guardar: 'Guardar',
            actualizar: 'Actualizar',
            mostrar: 'Mostrar seleccion',
            regresar: 'Regresar',
            continuar: 'Continuar',
            seleccionar: 'Seleccionar'
        },
        buttonsClass: {
            btnNuevoStyle: {
                'float-right': true,
                'mx-3': true
            }
        }
    },
    mensajes: {
        errorServicio: 'error..!!',
        desabilitada: 'Opción desabilitada',
        eliminar: '¿Esta seguro que desea eliminar el siguiente elemento?',
        guardar: '¿Esta seguro que desea guardar el siguiente elemento?',
        actualizar: '¿Esta seguro que desea actualizar el siguiente elemento?',
        datoGuardado: 'Se guardo corectamsente..!!',
        guardarParada: 'Esta seguro que desea registrar este evento..??',
        secuencialParada: '¿Esta seguro que desea actualizar los campos de F.Inicio, F.Fin, Contadores y Rechazo de los siguientes elementos?',
    },
    agGrid: {
        paginacion: 10,
        paginacionPareto: 100,
        id: 'myGrid',
        class: 'ag-theme-material mt-3',
        style: {
            width: '100%',
            height: '600px'
        }
    },
    div: {
        resposiveFull: 'col-12 col-sm-6 col-md-6 col-lg-4 ',//verificar el tipo de pantalla
    }

}

export { moduloTvc }