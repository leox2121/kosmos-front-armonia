import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EventosMantenerComponent } from './views/datos-maestros/eventos-mantener/eventos-mantener.component';
import { AuthGuard } from 'src/app/shared-module/seguridad/services/auth.guard';

const tvcRoutes: Routes = [
  {
    path: 'tvc/mantenedor/eventos',
    component: EventosMantenerComponent,
    // canActivate: [AuthGuard]
  }
];

@NgModule({
  imports: [RouterModule.forChild(tvcRoutes)],
  exports: [RouterModule]
})
export class TvcRoutingModule { }
