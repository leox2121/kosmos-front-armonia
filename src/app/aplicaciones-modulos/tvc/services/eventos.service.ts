import { Injectable } from '@angular/core';
import { CommonService } from 'src/app/shared/utils/common.service';
import { UtilsService } from 'src/app/shared/utils/utils.service';
import { moduloTvc } from '../diccionarios/tvcModulo';
import { EventoModel } from '../models/eventoModel';

@Injectable({
  providedIn: 'root'
})
export class EventosService {

  tvcModulo: any;
  url: string;

  constructor(private utilService: UtilsService,private commom: CommonService) { 
    this.tvcModulo = moduloTvc;
    this.url = this.utilService.getQueryTvc( this.tvcModulo.url.datosMaestros.eventos);
    //this.url = 'http://localhost:8080/tvc/v1/api/fe/evento';
  }

  getAll() {
    return this.commom.peticionGetConHeader(this.url);
  }

  save(objEvento: EventoModel) {
    return this.commom.peticionPostConHeader(this.url, objEvento);
  }

  edit(objEvento: EventoModel) {
    return this.commom.peticionPutConHeader(this.url, objEvento);
  }

  delete(innIdEvento: string) {
    return this.commom.peticionDeleteConHeader(this.url +'/'+innIdEvento);
  }

}
